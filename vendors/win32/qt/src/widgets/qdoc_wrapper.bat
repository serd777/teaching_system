@echo off
SetLocal EnableDelayedExpansion
(set QT_VERSION=5.7.0)
(set QT_VER=5.7)
(set QT_VERSION_TAG=570)
(set QT_INSTALL_DOCS=C:/Qt5.7/qtbase/doc)
C:\Qt5.7\qtbase\bin\qdoc.exe %*
EndLocal
