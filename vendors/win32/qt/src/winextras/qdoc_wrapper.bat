@echo off
SetLocal EnableDelayedExpansion
(set QT_VERSION=5.7.0)
(set QT_VER=5.7)
(set QT_VERSION_TAG=570)
(set QT_INSTALL_DOCS=C:/Qt5.7/qtbase/doc)
(set PATH=C:\Qt5.7\qtbase\lib;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=C:\Qt5.7\qtbase\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=C:\Qt5.7\qtbase\plugins
)
C:\Qt5.7\qtbase\bin\qdoc.exe %*
EndLocal
