@echo off
SetLocal EnableDelayedExpansion
(set PATH=C:\Qt5.7\qtbase\lib;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=C:\Qt5.7\qtbase\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=C:\Qt5.7\qtbase\plugins
)
C:\Qt5.7\qtbase\bin\qdbusxml2cpp.exe %*
EndLocal
