#include "debugger.h"

#include <managers/managers.h>
#include <observables/observables.h>
#include <compiler/compiler.h>
#include <messages/error_wnd/error_wnd.h>

using namespace client::managers;

DebugManager::DebugManager(QObject* parent)                                         //����������� ������
             :QObject(parent)                                                       //�������������� ������������ �����
{
    initSlots();                                                                    //���������� ������� � ������
}

void DebugManager::initSlots()                                                      //�����, ������������ � �������� ������������ �����
{
    auto menu = &Observables::Instance().menu();                                    //����������� ����

    connect(menu, SIGNAL(debugStarted()),                                           //� ������� �������
            this, SLOT(onDebugStarted()));                                          //���������� ���� �������
}

//
//  ����� ���������
//

void DebugManager::onDebugStarted()                                                 //���� ��� ������� �������
{
    auto& manager = Managers::Instance().projectManager();                          //�������� �������
    auto& compiler = data::Compiler::Instance();                                    //������ � �����������

    compiler.compile(manager.project());                                            //�������� ���������� �������

    if (compiler.isSuccess())                                                       //���� ���������� ������ �������
        manager.project().launchExe();                                              //��������� ����������� ����
    else gui::messages::ErrorWnd msg(compiler.error());                             //������ ����������
}