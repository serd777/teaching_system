#pragma once

//
//  ����� ����������� ���������
//

#include <QtCore/QObject>

namespace client
{
    namespace managers
    {
        class DebugManager : public QObject                                         //����� ����������� ���������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit DebugManager(                                                  //����������� ������
                QObject* parent = nullptr                                           //������������ �����
            );
            ~DebugManager() {}                                                      //���������� ������

        private slots:                                                              //�������� �����

            void onDebugStarted();                                                  //���� ��� ������� �������

        private:                                                                    //�������� ������

            void initSlots();                                                       //�����, ������������ � �������� ������������ �����
        };
    }
}
