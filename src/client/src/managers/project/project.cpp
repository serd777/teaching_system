#include "project.h"

#include <observables/observables.h>

using namespace client::managers;

ProjectManager::ProjectManager(QObject* parent)                                     //����������� ������
               :QObject(parent)                                                     //�������������� ������������ �����
{
    initSlots();                                                                    //��������� �����
}

//
//  ������ �������������
//

void ProjectManager::initSlots()                                                    //�����, ������������ ����� � ������������
{
    auto menu = &Observables::Instance().menu();                                    //����������� ����
    auto project = &Observables::Instance().project();                              //����������� �������

    connect(menu, SIGNAL(projectOpen()),                                            //��� ��������� ������� �� �������� �������
            this, SLOT(onProjectOpened()));                                         //���������� ����
    connect(menu, SIGNAL(projectSave()),                                            //��� ��������� ������� �� ���������� �������
            this, SLOT(onProjectSaved()));                                          //���������� ����
    connect(menu, SIGNAL(projectClose()),                                           //��� ��������� ������� �� �������� �������
            this, SLOT(onProjectClosed()));                                         //���������� ����

    connect(this,    SIGNAL(projectLoaded()),                                       //��������� ������� �� ���������
            project, SIGNAL(projectLoaded()));                                      //� �����������
}

//
//  ����� ���������
//

void ProjectManager::onProjectOpened()                                              //���� ��� �������� �������
{

}
void ProjectManager::onProjectSaved()                                               //���� ��� ���������� �������
{

}
void ProjectManager::onProjectClosed()                                              //���� ��� �������� �������
{

}

//
//  ������ ������ � ��������
//

void ProjectManager::loadProject(const QString& path)                               //�����, ����������� ������ �� ���������� ����
{
    project_ = data::Project(path);                                                 //�������������� ������

    emit projectLoaded();                                                           //�������� ������ ������������ �������
}