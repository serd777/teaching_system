#pragma once

//
//  ����� ���������� ���������
//

#include <QtCore/QObject>
#include <project/project.h>

namespace client
{
    namespace managers
    {
        class ProjectManager : public QObject                                       //����� ���������� ���������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit ProjectManager(                                                //����������� ������
                QObject* parent = nullptr                                           //Qt ��������
            );
            ~ProjectManager() {}                                                    //���������� ������

            auto& project() { return project_; }                                    //����� ������� � �������� �������

            void loadProject(const QString& path);                                  //�����, ����������� ������ �� ���������� ����

        signals:                                                                    //�������

            void projectLoaded();                                                   //������ �������� �������� �������

        private:                                                                    //�������� ������

            data::Project project_;                                                 //������� ������

            void initSlots();                                                       //�����, ������������ ����� � ������������

        private slots:                                                              //�������� �����

            void onProjectOpened();                                                 //���� ��� �������� �������
            void onProjectSaved();                                                  //���� ��� ���������� �������
            void onProjectClosed();                                                 //���� ��� �������� �������
        };
    }
}