#pragma once

//
//  ����� ������� � ���������� ����������
//

#include "./project/project.h"
#include "./debugger/debugger.h"
#include "./hotkeys/hotkeys.h"

namespace client
{
    class Managers                                                                  //����� ������� � ���������� ����������
    {
    public:                                                                         //�������� ������

        static Managers& Instance()                                                 //����� ������� � ������
        {
            static Managers managers;                                               //������� ����� ���� ��� �� ��� ������
            return managers;                                                        //���������� ���
        }

        auto& projectManager() { return project_manager_; }                         //����� ������� � ���������� ���������
        auto& debugManager()   { return debug_manager_;   }                         //����� ������� � ��������� �������
        auto& hotkeysManager() { return hotkeys_manager_; }                         //����� ������� � ��������� ������� ������

    private:                                                                        //�������� ������

        managers::ProjectManager project_manager_;                                  //��������� ��������
        managers::DebugManager   debug_manager_;                                    //�������� �������
        managers::HotkeysManager hotkeys_manager_;                                  //�������� ������� ������

        Managers()  {}                                                              //����������� ������
        ~Managers() {}                                                              //���������� ������

        Managers(Managers const&) = delete;                                         //��������� ����������� �����������
        Managers& operator= (Managers const&) = delete;                             //� �������� ������������
    };
}