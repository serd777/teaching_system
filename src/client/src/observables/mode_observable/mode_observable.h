#pragma once

//
//  ����������� ����� �������
//

#include <QtCore/QObject>

namespace client
{
    namespace observables
    {
        class ModeObservable : public QObject                                       //����������� ����� �������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit ModeObservable(                                                //����������� ������
                QObject* parent = nullptr                                           //Qt ��������
            ) : QObject(parent) {}                                                  //�������������� ������������ �����

            ~ModeObservable() {};                                                   //���������� ������

        signals:                                                                    //�������

            void modeTrain();                                                       //����� ����������
            void modeControl();                                                     //����� ��������
        };
    }
}