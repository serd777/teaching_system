#pragma once

//
//  ����������� ��������� ����������
//

#include <QtCore/QObject>
#include <QtCore/QPoint>

namespace client
{
    namespace observables
    {
        class AppObservable : public QObject                                        //����������� ��������� ����������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit AppObservable(                                                 //����������� ������
                QObject* parent = nullptr                                           //Qt ��������
            ) : QObject(parent) {};                                                 //�������������� ������������ �����

            ~AppObservable() {};                                                    //���������� ������

        signals:                                                                    //�������

            void moveMain(QPoint);                                                  //������ ������������ ����

            void maximizeMain();                                                    //������ �������������� ��������� ����
            void minimizeMain();                                                    //������ ������������� ��������� ����

            void closeApp();                                                        //������ ���������� ������
        };
    }
}