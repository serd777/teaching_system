#pragma once

//
//  ��������� ����� ������� � ������������ ����������
//

#include "./app_observable/app_observable.h"
#include "./menu_observable/menu_observable.h"
#include "./mode_observable/mode_observable.h"
#include "./project_observable/project_observable.h"

namespace client
{
    class Observables                                                               //��������� ����� ������� � ������������ ����������
    {
    public:                                                                         //�������� ������

        static Observables& Instance()                                              //����� �������
        {
            static Observables observables;                                         //������� ����� ���� �� �� ��� ��� ������
            return observables;                                                     //���������� ������
        }

        auto& app()     { return app_observable_;  }                                //������ � ����������� ����������
        auto& menu()    { return menu_observable_; }                                //������ � ����������� ����
        auto& mode()    { return mode_observable_; }                                //������ � ����������� �������
        auto& project() { return project_observable_; }                             //������ � ����������� �������

    private:                                                                        //�������� ������

        observables::AppObservable  app_observable_;                                //����������� ����������
        observables::MenuObservable menu_observable_;                               //����������� ����
        observables::ModeObservable mode_observable_;                               //����������� �������
        observables::ProjectObservable project_observable_;                         //����������� �������

        Observables()  {}                                                           //����������� ������
        ~Observables() {}                                                           //���������� ������

        Observables(Observables const&)             = delete;                       //��������� ����������� �����������
        Observables& operator= (Observables const&) = delete;                       //� �������� ������������
    };
}