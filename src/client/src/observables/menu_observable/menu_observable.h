#pragma once

//
//  ����������� ��������� �������� ���� ����������
//

#include <QtCore/QObject>

namespace client
{
    namespace observables
    {
        class MenuObservable : public QObject                                       //����������� ��������� �������� ���� ����������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit MenuObservable(                                                //����������� ������
                QObject* parent = nullptr                                           //Qt ��������
            ) : QObject(parent) {}                                                  //������������� ������������� ������

            ~MenuObservable() {}                                                    //���������� ������

        signals:                                                                    //�������

            void projectOpen();                                                     //������ �������� �������
            void projectSave();                                                     //������ ���������� �������
            void projectClose();                                                    //������ �������� �������

            void debugStarted();                                                    //������ ������� �������
        };
    }
}