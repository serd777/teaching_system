#pragma once

//
//  ����������� ���������� ������
//

#include <QtCore/QObject>

namespace client
{
    namespace observables
    {
        class ProjectObservable : public QObject                                    //����������� ���������� ������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit ProjectObservable(                                             //����������� ������
                QObject* parent = nullptr                                           //Qt ��������
            ) : QObject(parent) {}                                                  //�������������� ������������ �����

            ~ProjectObservable() {}                                                 //���������� ������

        signals:                                                                    //�������

            void projectLoaded();                                                   //������ ������������ �������
        };
    }
}