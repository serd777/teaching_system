#include "application.h"

#include <fonts/fonts.h>
#include <messages/error_wnd/error_wnd.h>
#include <syntax/accessor/accessor.h>
#include <settings/settings.h>
#include <code_helper/code_helper.h>

#include <managers/managers.h>
#include <directory/directory.h>

//
//  ����������� � ����������
//

Application::Application(int argc, char** argv)                                     //����������� ������
            :QObject()                                                              //�������������� ������������ �����
{
    q_app_ = new QApplication(argc, argv);                                          //�������������� Qt ����������
    start_timer_ = new QTimer;                                                      //�������������� ��������� ������
}
Application::~Application()                                                         //���������� ������
{
    delete start_timer_;                                                            //����������� ������
    delete q_app_;                                                                  //����������� ������
}

//
//  ������ ������
//

bool Application::init()                                                            //�����, ���������������� ����������
{
    if (!initResources())                                                           //�������������� �������
        return false;                                                               //������������� �����������

    semantic::SyntaxAccessor::init(new semantic::Syntax("pascal"));                 //�������������� ��������� ����� ����������������

    initWindows();                                                                  //�������������� ����
    initProject();                                                                  //�������������� ������

    return true;                                                                    //������������� ������ �������
}
bool Application::initResources()                                                   //�����, ���������������� ��������� �������
{
    QString err_msg;                                                                //��������� ������ �������������

    if (!resources::Styles::init())                                                 //���� ����� �� ����������������
        err_msg = "Styles init failed!";                                            //��������� ������

    if (!resources::Loc::init())                                                    //���� ����������� �� ������������������
        err_msg += "\nLocalization init failed!";                                   //��������� ������

    if (!resources::Fonts::init())                                                  //���� ������ �� ����������������
        err_msg += "\nFonts init failed";                                           //��������� ������

    if (!resources::CodeHelper::Instance().init())                                  //���� �� ����������������� Helper ��� ����
        err_msg += "\nCode Helper init failed";                                     //��������� ������

    resources::Settings::Instance().init();                                         //�������������� ���������

    if (!err_msg.isEmpty())                                                         //���� �������� ������ ��� �������
    {
        gui::messages::ErrorWnd wnd(err_msg);                                       //������� ��������� � �������
        return false;                                                               //��������� ������
    }

    return true;                                                                    //������� ������������������
}
void Application::initWindows()                                                     //�����, ���������������� ���� ����������
{
    main_window_    = new client::windows::MainWindow;                              //������� ���� ����������
    startup_window_ = new client::windows::StartupWindow;                           //������� ��������� ����

    main_window_->init();                                                           //�������������� ����
    startup_window_->init();                                                        //�������������� ��������� ����
}
void Application::initProject()                                                     //�����, ���������������� ��������� ������
{
    filesystem::Directory dir("tmp/project");                                       //���������� ���������� �������

    if (dir.isExists())                                                             //���� ���������� ����������
        dir.clear();                                                                //�� ������� �
    else
        dir.create();                                                               //����� �������

    auto& manager = client::Managers::Instance().projectManager();                  //������ � ���������� ���������
    manager.loadProject(dir.path());                                                //��������� ��������� ������
}

void Application::showStartWindow()                                                 //�����, ������������ ��������� ����
{
    startup_window_->show();                                                        //������� ��������� ����

    connect(start_timer_, &QTimer::timeout,                                         //��������� ������ ����������
            this,         [this]()                                                  //� �������
    {
        startup_window_->hide();                                                    //�������� ��������� ����
        main_window_->show();                                                       //���������� ��������
        start_timer_->stop();                                                       //������������� ������ �������
    });
    start_timer_->setInterval(3000);                                                //������ �������� ���������� 3 �������
    start_timer_->start();                                                          //��������� ������
}

int Application::exec()                                                             //�����, ����������� ���������� ����������
{
    if (!init())                                                                    //���� ������������� �����������
        return -1;                                                                  //�� ��������� ������ ����������

#ifdef _DEBUG
    main_window_->show();                                                           //��� ������� ���������� ����� ��������
#else
    showStartWindow();                                                              //���������� ��������� ����
#endif
    
    return q_app_->exec();                                                          //��������� ����� ����������
}