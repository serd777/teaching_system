#pragma once

//
//  �������� ����� ����������
//

#include <QtWidgets/QApplication>
#include <QtCore/QTimer>

#include <windows/startup_window/startup_window.h>
#include <windows/main_window/main_window.h>

class Application : public QObject                                                  //�������� ����� ����������
{
public:                                                                             //�������� ������

    explicit Application(                                                           //����������� ������
        int argc,                                                                   //���-�� ����������
        char** argv                                                                 //��������� �������
    );

    ~Application();                                                                 //���������� ������

    int exec();                                                                     //�����, ����������� ���������� ����������

private:                                                                            //�������� ������

    QApplication* q_app_;                                                           //Qt ����������
    QTimer*       start_timer_;                                                     //��������� ������

    client::windows::StartupWindow* startup_window_;                                //��������� ���� ����������
    client::windows::MainWindow* main_window_;                                      //�������� ���� ����������

    bool init();                                                                    //�����, ���������������� ����������

    bool initResources();                                                           //�����, ���������������� ��������� �������
    void initWindows();                                                             //�����, ���������������� ���� ����������
    void initProject();                                                             //�����, ���������������� ��������� ������

    void showStartWindow();                                                         //�����, ������������ ��������� ����
};