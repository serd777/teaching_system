#pragma once

//
//  ����� ��������� ���� ����������
//

#include <widgets/gmainwindow.h>
#include "./ui/ui_main_window.h"

namespace client
{
    namespace windows
    {
        class MainWindow : public gui::widgets::GMainWindow                         //����� ��������� ���� ����������
        {
        public:                                                                     //�������� ������

            explicit MainWindow(                                                    //����������� ������
                QWidget* parent = nullptr                                           //Qt ��������
            );

            ~MainWindow() {}                                                        //���������� ������

            void init() override;                                                   //�����, ���������������� ����

        protected:                                                                  //����������� ������

            void keyPressEvent(QKeyEvent* e) override;                              //���������� ������� ������

        private:                                                                    //�������� ������

            UIMainWindow* ui_;                                                      //����������� �������� ����

            void initSlots();                                                       //����� ������������� ������
        };
    }
}