#include "maxim_button.h"
#include <observables/observables.h>

using namespace client::windows::ui_mainwindow;

MaximButton::MaximButton(QWidget* parent)                                           //����������� ������
            :GPushButton(parent)                                                    //�������������� ������������ �����
{
    setFixedSize(25, 25);                                                           //������ ������������� ������

    connect(this,                           SIGNAL(clicked()),                      //������ ��������� ������� �������
            &Observables::Instance().app(), SIGNAL(maximizeMain()));                //� �����������
}

void MaximButton::updateStyle()                                                     //����� ���������� �����
{
    setIcon(QIcon(                                                                  //������ ������ ������
        ":/" +
        resources::Styles::accessor()->name() +
        "/window_max"
    ));

    GPushButton::updateStyle();                                                     //�������� ������������ �����
}
