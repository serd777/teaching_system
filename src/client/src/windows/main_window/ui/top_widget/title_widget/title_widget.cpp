#include "title_widget.h"
#include <observables/observables.h>

using namespace client::windows::ui_mainwindow;

TitleWidget::TitleWidget(QWidget* parent)                                           //����������� ������
            :GWidget(parent)                                                        //������������� ������������� ������
{
    setObjectName("client-main_wnd-top_wdj-title_wdj");                             //������ ��� �������
    setFixedHeight(height_);                                                        //������ ������������� ������

    title_label_ = new TitleLabel(this);                                            //�������������� ������������ ������
    system_btn_widget_ = new SystemButtonWidget(this);                              //�������������� ������ � ���������� ��������
    
    layout_ = new QHBoxLayout(this);                                                //�������������� ����
    layout_->setContentsMargins(100, 0, 0, 0);                                      //������ ������� ����
    layout_->addWidget(title_label_);                                               //��������� ������ �� ����
    layout_->addWidget(system_btn_widget_);                                         //��������� ������ �� ����

    connect(this,                           SIGNAL(moveTriggered(QPoint)),          //������ ��������� ������� ��������
            &Observables::Instance().app(), SIGNAL(moveMain(QPoint)));              //� �����������
}
void TitleWidget::init()                                                            //����� �������������
{
    title_label_->init();                                                           //�������������� ������������ ������
    system_btn_widget_->init();                                                     //�������������� ������ � ���������� ��������

    GWidget::init();                                                                //�������� ������������ �����
}

//
//  ����������� ������� ����
//

void TitleWidget::mousePressEvent(QMouseEvent* event)                               //����� ��������� ������� �� ������
{
    if (event->button() == Qt::LeftButton)                                          //���� ������ ���
    {
        point_click_ = event->pos();                                                //���������� ����� �������
        mouse_pressed_ = true;                                                      //��������� ���� ������� ���
    }
}
void TitleWidget::mouseReleaseEvent(QMouseEvent* event)                             //����� ��������� ���������� ������ ����
{
    if (event->button() == Qt::LeftButton)                                          //���� �������� ����� ������ ����
        mouse_pressed_ = false;                                                     //��������� ������������ ������������ ����
}
void TitleWidget::mouseMoveEvent(QMouseEvent* event)                                //����� ��������� �������� ����
{
    if (mouse_pressed_)                                                             //���� ������ ���
        emit moveTriggered(event->globalPos() - point_click_);                      //�������� ������
}
