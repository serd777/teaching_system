#pragma once

//
//  ������������ ������ � ������ ����
//

#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>
#include <QtGui/QMouseEvent>

#include "title_label/title_label.h"
#include "system_btn_widget/system_btn_widget.h"

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class TitleWidget : public gui::widgets::GWidget                        //������������ ������ � ������ ����
            {
                Q_OBJECT                                                            //MOC ����

            public:                                                                 //�������� ������

                explicit TitleWidget(                                               //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~TitleWidget() {}                                                   //���������� ������

                void init() override;                                               //����� �������������

            protected:                                                              //����������� ������

                void mousePressEvent(QMouseEvent* event)   override;                //����� ��������� ������� �� ������
                void mouseReleaseEvent(QMouseEvent* event) override;                //����� ��������� ���������� ������ ����
                void mouseMoveEvent(QMouseEvent* event)    override;                //����� ��������� �������� ����

            signals: void moveTriggered(QPoint);                                    //������ ��������
            private:                                                                //�������� ������

                const int    height_ = 40;                                          //������ �������
                QHBoxLayout* layout_;                                               //���� �������
                QPoint       point_click_;                                          //����� ���������� ������� �� ������
                bool         mouse_pressed_ = false;                                //���� ������� ���
                
                TitleLabel* title_label_;                                           //������ � ������ ����
                SystemButtonWidget* system_btn_widget_;                             //������ � ���������� ��������
            };
        }
    }
}