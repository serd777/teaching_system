#include "minim_button.h"
#include <observables/observables.h>

using namespace client::windows::ui_mainwindow;

MinimButton::MinimButton(QWidget* parent)                                           //����������� ������
            :GPushButton(parent)                                                    //�������������� ������������ �����
{
    setFixedSize(25, 25);                                                           //������ ������������� ������

    connect(this,                           SIGNAL(clicked()),                      //������ ��������� ������� �������
            &Observables::Instance().app(), SIGNAL(minimizeMain()));                //� �����������
}

void MinimButton::updateStyle()                                                     //����� ���������� �����
{
    setIcon(QIcon(                                                                  //������ ������ ������
        ":/" +
        resources::Styles::accessor()->name() +
        "/window_min"
    ));

    GPushButton::updateStyle();                                                     //�������� ������������ �����
}
