#pragma once

//
//  ������ �������� ����
//

#include <widgets/gpushbutton.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class CloseButton : public gui::widgets::GPushButton                    //������ �������� ����
            {
            public:                                                                 //�������� ������

                explicit CloseButton(                                               //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~CloseButton() {}                                                   //���������� ������

            protected: void updateStyle() override;                                 //����� ���������� �����
            };
        }
    }
}