#include "close_button.h"
#include <observables/observables.h>

using namespace client::windows::ui_mainwindow;

CloseButton::CloseButton(QWidget* parent)                                           //����������� ������
            :GPushButton(parent)                                                    //�������������� ������������ �����
{
    setFixedSize(25, 25);                                                           //������������� ������ ������

    connect(this,                           SIGNAL(clicked()),                      //������ ��������� ������� �������
            &Observables::Instance().app(), SIGNAL(closeApp()));                    //� �����������
}

void CloseButton::updateStyle()                                                     //����� ���������� �����
{
    setIcon(QIcon(                                                                  //������ ������ ������
        ":/" +
        resources::Styles::accessor()->name() +
        "/window_close"
    ));

    GPushButton::updateStyle();                                                     //�������� ������������ �����
}