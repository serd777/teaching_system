#pragma once

//
//  ������ �������������� ����
//

#include <widgets/gpushbutton.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class MaximButton : public gui::widgets::GPushButton                    //������ ��������������
            {
            public:                                                                 //�������� ������

                explicit MaximButton(                                               //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~MaximButton() {}                                                   //���������� ������

            protected: void updateStyle() override;                                 //����� ���������� �����
            };
        }
    }
}