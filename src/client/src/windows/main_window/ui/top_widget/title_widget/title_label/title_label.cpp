#include "title_label.h"

using namespace client::windows::ui_mainwindow;

TitleLabel::TitleLabel(QWidget* parent)                                             //����������� ������
           :GLabel(parent)                                                          //������������� ������������� ������
{
    setObjectName("client-main_wnd-top_wdj-title_label");                           //������ ��� �������
    setAlignment(Qt::AlignCenter);                                                  //����� �� ������
}

void TitleLabel::updateLocalization()                                               //����� ���������� �����������
{
    setText((*resources::Loc::accessor())["client-main_window"]);                   //������ �����
}
