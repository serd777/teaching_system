#pragma once

//
//  ������ �������������
//

#include <widgets/gpushbutton.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class MinimButton : public gui::widgets::GPushButton                    //������ �������������
            {
            public:                                                                 //�������� ������

                explicit MinimButton(                                               //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~MinimButton() {}                                                   //���������� ������

            protected: void updateStyle() override;                                 //����� ���������� �����
            };
        }
    }
}