#pragma once

//
//  ������, ���������� ��������� ������ ����
//

#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>

#include "./minim_button/minim_button.h"
#include "./maxim_button/maxim_button.h"
#include "./close_button/close_button.h"

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class SystemButtonWidget : public gui::widgets::GWidget                 //������, ���������� ��������� ������ ����
            {
            public:                                                                 //�������� ������

                explicit SystemButtonWidget(                                        //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~SystemButtonWidget() {}                                            //���������� ������

                void init() override;                                               //����� �������������

            private:                                                                //�������� ������

                const int width_ = 100;                                             //������ �������

                QHBoxLayout* layout_;                                               //���� �������
                
                MinimButton* minim_button_;                                         //������ �������������
                MaximButton* maxim_button_;                                         //������ ��������������
                CloseButton* close_button_;                                         //������ ��������
            };
        }
    }
}