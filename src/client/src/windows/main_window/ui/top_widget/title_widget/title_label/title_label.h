#pragma once

//
//  ������ � ������ ����
//

#include <widgets/glabel.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class TitleLabel : public gui::widgets::GLabel                          //������ � ������ ����
            {
            public:                                                                 //�������� ������

                explicit TitleLabel(                                                //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~TitleLabel() {}                                                    //���������� ������

            protected: void updateLocalization() override;                          //����� ���������� �����������
            };
        }
    }
}