#include "system_btn_widget.h"

using namespace client::windows::ui_mainwindow;

SystemButtonWidget::SystemButtonWidget(QWidget* parent)                             //����������� ������
                   :GWidget(parent)                                                 //������������� ������������� ������
{
    setFixedWidth(width_);                                                          //������ ������������� ������

    minim_button_ = new MinimButton(this);                                          //�������������� ������ �������������
    maxim_button_ = new MaximButton(this);                                          //�������������� ������ ��������������
    close_button_ = new CloseButton(this);                                          //�������������� ������ ��������
    layout_ = new QHBoxLayout(this);                                                //�������������� ���� �������

    layout_->setSpacing(5);                                                         //������ ���������� ����� ���������
    layout_->setContentsMargins(0, 0, 0, 0);                                        //������ ������� �����������

    layout_->addWidget(minim_button_);                                              //��������� ������ �� ����
    layout_->addWidget(maxim_button_);                                              //��������� ������ �� ����
    layout_->addWidget(close_button_);                                              //��������� ������ �� ����
}

void SystemButtonWidget::init()
{
    minim_button_->init();                                                          //�������������� ������ �������������
    maxim_button_->init();                                                          //�������������� ������ ��������������
    close_button_->init();                                                          //�������������� ������ ��������

    GWidget::init();                                                                //�������� ������������ �����
}