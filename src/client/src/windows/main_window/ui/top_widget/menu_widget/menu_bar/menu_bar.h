#pragma once

//
//  ������ ��������� ���� ���������
//

#include <widgets/gmenubar.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class MenuBar : public gui::widgets::GMenuBar                           //������ ��������� ���� ���������
            {
                struct ProjectActions_t                                             //�������� ��������� �������� ���� "������"
                {
                    QAction* open;                                                  //������� ������
                    QAction* save;                                                  //��������� ������
                    QAction* close;                                                 //������� ������
                };

                struct DebugActions_t                                               //�������� ��������� �������� ���� "�������"
                {
                    QAction* start;                                                 //������ �������
                };

            public:                                                                 //�������� ������

                explicit MenuBar(                                                   //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~MenuBar() {};                                                      //���������� ������

            protected: void updateLocalization() override;                          //����� ���������� �����������

            private:                                                                //�������� ������

                QMenu* menu_project_;                                               //���� "������"
                QMenu* menu_debug_;                                                 //���� "�������"

                ProjectActions_t project_actions_;                                  //��������� � ���������� ���� "������"
                DebugActions_t debug_actions_;                                      //��������� � ���������� ���� "�������"

                void initMenus();                                                   //����� ������������� �������
                void initActions();                                                 //����� ������������� �������� ���� �������
                void initTriggers();                                                //����� ������������� ��������� ���������
            };
        }
    }
}