#include "menu_widget.h"

using namespace client::windows::ui_mainwindow;

MenuWidget::MenuWidget(QWidget* parent)                                             //����������� ������
           :GWidget(parent)                                                         //�������������� ������������ �����
{
    setObjectName("client-main_wnd-top_wdj-menu");                                  //������ ��� �������
    setFixedHeight(height_);                                                        //������ ������������� ������

    menu_bar_ = new MenuBar(this);                                                  //�������������� �������������� ����
    layout_ = new QHBoxLayout(this);                                                //�������������� ���� �������

    layout_->setContentsMargins(0, 5, 0, 0);                                        //������ ������ ������� ����
    layout_->addWidget(menu_bar_);                                                  //��������� ������ �� ����
}

void MenuWidget::init()                                                             //����� �������������
{
    menu_bar_->init();                                                              //�������������� �������������� ����

    GWidget::init();                                                                //�������� ������������ �����
}
