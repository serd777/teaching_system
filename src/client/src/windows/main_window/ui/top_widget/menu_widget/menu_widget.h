#pragma once

//
//  ������ ��������� ����
//

#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>

#include "./menu_bar/menu_bar.h"

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class MenuWidget : public gui::widgets::GWidget                         //������ ��������� ����
            {
            public:                                                                 //�������� ������

                explicit MenuWidget(                                                //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~MenuWidget() {}                                                    //���������� ������

                void init() override;                                               //����� �������������

            private:                                                                //�������� ������

                const int height_ = 40;                                             //������ �������

                QHBoxLayout* layout_;                                               //���� �������
                MenuBar*     menu_bar_;                                             //�������������� ����
            };
        }
    }
}