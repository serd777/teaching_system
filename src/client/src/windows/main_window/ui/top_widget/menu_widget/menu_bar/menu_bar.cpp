#include "menu_bar.h"

#include <observables/observables.h>

using namespace client::windows::ui_mainwindow;

MenuBar::MenuBar(QWidget* parent)                                                   //����������� ������
        :GMenuBar(parent)                                                           //�������������� ������������ �����
{
    initMenus();                                                                    //�������������� ������ ����
    initActions();                                                                  //�������������� �������� ���� ����
    initTriggers();                                                                 //�������������� �������� ���������
}

//
//  ������ �������������
//

void MenuBar::initMenus()                                                           //����� ������������� �������
{
    menu_project_ = addMenu("Project");                                             //���� ������
    menu_debug_ = addMenu("Debug");                                                 //���� �������
}
void MenuBar::initActions()                                                         //����� ������������� �������� ���� �������
{
    project_actions_.open = menu_project_->addAction("Open");                       //������� ������
    project_actions_.close = menu_project_->addAction("Close");                     //������� ������
    project_actions_.save = menu_project_->addAction("Save");                       //��������� ������

    debug_actions_.start = menu_debug_->addAction("Start");                         //��������� �������
}
void MenuBar::initTriggers()                                                        //����� ������������� ��������� ���������
{
    auto menu_obsrv = &Observables::Instance().menu();                              //����������� ����

    connect(project_actions_.open,  SIGNAL(triggered()),                            //��� ������� �� "������� ������"
            menu_obsrv,             SIGNAL(projectOpen()));                         //������ ������ �����������
    connect(project_actions_.close, SIGNAL(triggered()),                            //��� ������� �� "������� ������"
            menu_obsrv,             SIGNAL(projectClose()));                        //������ ������ �����������
    connect(project_actions_.save,  SIGNAL(triggered()),                            //��� ������� �� "��������� ������"
            menu_obsrv,             SIGNAL(projectSave()));                         //������ ������ �����������

    connect(debug_actions_.start, SIGNAL(triggered()),                              //��� ������� �� "��������� �������"
            menu_obsrv,           SIGNAL(debugStarted()));                          //������ ������ �����������
}

//
//  ������������� ������
//

void MenuBar::updateLocalization()                                                  //����� ���������� �����������
{
    auto loc = resources::Loc::accessor();                                          //������ � ������������

    menu_project_->setTitle((*loc)["menu-project"]);                                //������ ����� ���� "������"
    menu_debug_->setTitle((*loc)["menu-debug"]);                                    //������ ����� ���� "�������"

    project_actions_.open->setText((*loc)["menu-project-open"]);                    //������ ����� ��� ���� "������� ������"
    project_actions_.close->setText((*loc)["menu-project-close"]);                  //������ ����� ��� ���� "������� ������"
    project_actions_.save->setText((*loc)["menu-project-save"]);                    //������ ����� ��� ���� "��������� ������"

    debug_actions_.start->setText((*loc)["menu-debug-start"]);                      //������ ����� ��� ���� "��������� �������"
}