#pragma once

//
//  ������ ��������� ����
//

#include <widgets/gwidget.h>
#include <QtWidgets/QVBoxLayout>

#include "./title_widget/title_widget.h"
#include "./menu_widget/menu_widget.h"
#include "./menu_button_widget/menu_button_widget.h"

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class TopWidget : public gui::widgets::GWidget                          //������ ��������� ����
            {
            public:                                                                 //�������� ������

                explicit TopWidget(                                                 //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~TopWidget() {}                                                     //���������� ������

                void init() override;                                               //����� �������������

            private:                                                                //�������� ������

                const int height_ = 120;                                            //������ �������
                QVBoxLayout* layout_;                                               //���� �������

                TitleWidget* title_widget_;                                         //������ � ������ ����
                MenuWidget* menu_widget_;                                           //������ ����
                MenuButtonWidget* menu_button_widget_;                              //������ ���������� ����
            };
        }
    }
}