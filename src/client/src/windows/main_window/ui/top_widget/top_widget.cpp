#include "top_widget.h"

using namespace client::windows::ui_mainwindow;

TopWidget::TopWidget(QWidget* parent)                                               //����������� ������
          :GWidget(parent)                                                          //������������� ������������� ������
{
    setObjectName("client-main_wnd-top_wdj");                                       //������ ��� �������
    setFixedHeight(height_);                                                        //��������� ������ �������

    title_widget_ = new TitleWidget(this);                                          //�������������� ������ � ������ ����
    menu_widget_ = new MenuWidget(this);                                            //�������������� ������ ����
    menu_button_widget_ = new MenuButtonWidget(this);                               //�������������� ������ ���������� ����

    layout_ = new QVBoxLayout(this);                                                //�������������� ���� �������
    layout_->setSpacing(0);                                                         //���������� ����� ���������

    layout_->addWidget(title_widget_);                                              //��������� ������
    layout_->addWidget(menu_widget_);                                               //��������� ������
    layout_->addWidget(menu_button_widget_);                                        //��������� ������
}

void TopWidget::init()                                                              //����� �������������
{
    title_widget_->init();                                                          //�������������� ������ � ������ ����
    menu_widget_->init();                                                           //�������������� ������ ����
    menu_button_widget_->init();                                                    //�������������� ������ ���������� ����

    GWidget::init();                                                                //�������� ������������ �����
}
