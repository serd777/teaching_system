#include "debug_button.h"

#include <observables/observables.h>

using namespace client::windows::ui_mainwindow::menu_buttons;

DebugButton::DebugButton(QWidget* parent)                                           //����������� ������
            :GPushButton(parent)                                                    //�������������� ��������
{
    setFixedSize(30, 30);                                                           //������ ������������� ������

    initSlots();
}

void DebugButton::initSlots()                                                       //��������� � ��������
{
    auto observable = &Observables::Instance().menu();                              //����������� �������� ����

    connect(this,       SIGNAL(clicked()),                                          //��� ������� �� ������
            observable, SIGNAL(debugStarted()));                                    //������ ���������� �����������
}

//
//  ������������� ������
//

void DebugButton::updateStyle()                                                     //�����, ����������� ����� ������
{
    setIcon(QIcon(":/icons/start_debug"));                                          //������ ������

    GPushButton::updateStyle();                                                     //�������� ������������ �����
}
void DebugButton::updateLocalization()                                              //�����, ����������� �����������
{
    setToolTip((*resources::Loc::accessor())["menu-debug-start"]);                  //������ ����������� ���������

    GPushButton::updateLocalization();                                              //�������� ������������ �����
}
