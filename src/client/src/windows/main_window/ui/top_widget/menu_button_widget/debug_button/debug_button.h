#pragma once

//
//  ������ ������� �������
//

#include <widgets/gpushbutton.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            namespace menu_buttons
            {
                class DebugButton : public gui::widgets::GPushButton                //������ ������� �������
                {
                public:                                                             //�������� ������

                    explicit DebugButton(                                           //����������� ������
                        QWidget* parent = nullptr                                   //Qt ��������
                    );

                    ~DebugButton() {}                                               //���������� ������

                protected:
                    
                    void updateStyle() override;                                    //�����, ����������� ����� ������
                    void updateLocalization() override;                             //�����, ����������� �����������

                private: void initSlots();                                          //�����, ������������ �������
                };
            }
        }
    }
}