#include "menu_button_widget.h"

using namespace client::windows::ui_mainwindow;

MenuButtonWidget::MenuButtonWidget(QWidget* parent)                                 //����������� ������
                 :GWidget(parent)                                                   //�������������� ������������ �����
{
    //setObjectName("");
    setFixedHeight(height_);                                                        //������ ������������� ������

    layout_ = new QHBoxLayout(this);                                                //�������������� ���� ������
    layout_->setContentsMargins(10, 0, 0, 0);                                       //������ ������� ����
    layout_->setSpacing(10);                                                        //���������� ����� ���������
    layout_->setAlignment(Qt::AlignLeft);                                           //������������ ��������

    debug_button_ = new menu_buttons::DebugButton(this);                            //�������������� ������ ������� �������
    settings_button_ = new menu_buttons::SettingsButton(this);                      //�������������� ������ ��������

    layout_->addWidget(debug_button_);                                              //��������� ������ �� ����
    layout_->addWidget(settings_button_);                                           //��������� ������ �� ����
}

void MenuButtonWidget::init()                                                       //����� �������������
{
    debug_button_->init();                                                          //�������������� ������ ������� �������
    settings_button_->init();                                                       //�������������� ������ ��������

    GWidget::init();                                                                //�������� ������������ �����
}
