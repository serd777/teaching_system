#pragma once

//
//  ������ ��� ���������� ���� ���������
//

#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>

#include "./debug_button/debug_button.h"
#include "./settings_button/settings_button.h"

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class MenuButtonWidget : public gui::widgets::GWidget                   //������ ��� ���������� ���� ���������
            {
            public:                                                                 //�������� ������

                explicit MenuButtonWidget(                                          //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~MenuButtonWidget() {}                                              //���������� ������

                void init() override;                                               //����� �������������

            private:                                                                //�������� ������

                const int height_ = 40;                                             //������ �������

                QHBoxLayout* layout_;                                               //���� �������

                menu_buttons::DebugButton*    debug_button_;                        //������ ������� �������
                menu_buttons::SettingsButton* settings_button_;                     //������ ��������
            };
        }
    }
}