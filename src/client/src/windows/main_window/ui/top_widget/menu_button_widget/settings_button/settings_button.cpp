#include "settings_button.h"

using namespace client::windows::ui_mainwindow::menu_buttons;

SettingsButton::SettingsButton(QWidget* parent)                                     //����������� ������
               :GPushButton(parent)                                                 //�������������� ��������
{
    setFixedSize(30, 30);                                                           //������ ������������� ������
}

//
//  ������������� ������
//

void SettingsButton::updateStyle()                                                  //�����, ����������� ����� ������
{
    setIcon(QIcon(":/icons/settings"));                                             //������ ������

    GPushButton::updateStyle();                                                     //�������� ������������ �����
}
void SettingsButton::updateLocalization()                                           //�����, ����������� �����������
{
    setToolTip((*resources::Loc::accessor())["settings"]);                          //������ ����������� ���������

    GPushButton::updateLocalization();                                              //�������� ������������ �����
}
