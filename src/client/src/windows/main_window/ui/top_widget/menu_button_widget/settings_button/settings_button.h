#pragma once

//
//  ������ ������ ��������
//

#include <widgets/gpushbutton.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            namespace menu_buttons
            {
                class SettingsButton : public gui::widgets::GPushButton             //������ ������ ��������
                {
                public:                                                             //�������� ������

                    explicit SettingsButton(                                        //����������� ������
                        QWidget* parent = nullptr                                   //Qt ��������
                    );

                    ~SettingsButton() {}                                            //���������� ������

                protected:
                    
                    void updateStyle() override;                                    //�����, ����������� ����� ������
                    void updateLocalization() override;                             //�����, ����������� �����������

                //private:   void initSlots();                                        //�����, ������������ �������
                };
            }
        }
    }
}