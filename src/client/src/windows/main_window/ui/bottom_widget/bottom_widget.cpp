#include "bottom_widget.h"

using namespace client::windows::ui_mainwindow;

BottomWidget::BottomWidget(QWidget* parent)                                         //����������� ������
             :GWidget(parent)                                                       //������������� ������������� ������
{
    setObjectName("client-main_wnd-bottom_wdj");                                    //������ ��� �������
    setFixedHeight(height_);                                                        //��������� ������ �������

    layout_ = new QHBoxLayout(this);                                                //�������������� ����
    status_label_ = new StatusLabel(this);                                          //�������������� ������ �� �������� ����

    layout_->addWidget(status_label_);                                              //��������� ������ �� ����
}

void BottomWidget::init()                                                           //����� �������������
{
    status_label_->init();                                                          //�������������� ������

    GWidget::init();                                                                //�������� ������������ �����
}
