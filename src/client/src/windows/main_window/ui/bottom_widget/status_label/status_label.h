#pragma once

//
//  ������ �� �������� ����
//

#include <widgets/glabel.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class StatusLabel : public gui::widgets::GLabel                         //������ �� �������� ����
            {
            public:                                                                 //�������� ������

                explicit StatusLabel(                                               //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~StatusLabel() {}                                                   //���������� ������
            };
        }
    }
}