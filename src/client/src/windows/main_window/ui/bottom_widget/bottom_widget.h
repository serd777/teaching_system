#pragma once

//
//  ������ ������ ���������
//

#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>

#include "./status_label/status_label.h"

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class BottomWidget : public gui::widgets::GWidget                       //������ ������ ���������
            {
            public:                                                                 //�������� ������

                explicit BottomWidget(                                              //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~BottomWidget() {}                                                  //���������� ������

                void init() override;                                               //����� �������������

            private:                                                                //�������� ������

                const int height_ = 30;                                             //������ �������
                QHBoxLayout* layout_;                                               //���� �������

                StatusLabel* status_label_;                                         //������ � ������� ������� ����������
            };
        }
    }
}