#pragma once

//
//  ����������� �������� ��������� ����
//

#include <widgets/gwidget.h>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QSizeGrip>

#include "./top_widget/top_widget.h"
#include "./body_widget/body_widget.h"
#include "./bottom_widget/bottom_widget.h"

namespace client
{
    namespace windows
    {
        class UIMainWindow : public gui::widgets::GWidget                           //����������� �������� ��������� ����
        {
        public:                                                                     //�������� ������

            explicit UIMainWindow(                                                  //����������� ������
                QWidget* parent = nullptr                                           //Qt ��������
            );

            ~UIMainWindow() {}                                                      //���������� ������

            void init() override;                                                   //����� �������������

            auto topWidget() { return top_widget_; }                                //����� ������� � ������������� �������

        protected:

            void resizeEvent(QResizeEvent* event) override;                         //�����, �������������� ������ ��������� ������� �������

        private:                                                                    //�������� ������

            QVBoxLayout* layout_;                                                   //���� �������
            QSizeGrip*   size_grip_;                                                //�������� ����

            ui_mainwindow::TopWidget* top_widget_;                                  //������ ��������� ����
            ui_mainwindow::BodyWidget* body_widget_;                                //������ ���� ����
            ui_mainwindow::BottomWidget* bottom_widget_;                            //������ ������ ����� ����

            void initLayout();                                                      //�����, ���������������� ���� �������
        };
    }
}