#include "ui_main_window.h"

using namespace client::windows;

UIMainWindow::UIMainWindow(QWidget* parent)                                         //����������� ������
             :GWidget(parent)                                                       //������������� ������������� ������
{
    setObjectName("client-main_window-ui");                                         //������ ��� �������

    layout_ = new QVBoxLayout(this);                                                //�������������� ����
    size_grip_ = new QSizeGrip(this);                                               //�������������� �������� ����

    top_widget_ = new ui_mainwindow::TopWidget(this);                               //�������������� ������������ ������
    body_widget_ = new ui_mainwindow::BodyWidget(this);                             //�������������� ������ ����
    bottom_widget_ = new ui_mainwindow::BottomWidget(this);                         //�������������� ������ ����� ����
}

//
//  ������ �������������
//

void UIMainWindow::init()                                                           //����� �������������
{
    top_widget_->init();                                                            //�������������� ������������ ������
    body_widget_->init();                                                           //�������������� ������ ����s
    bottom_widget_->init();                                                         //�������������� ������ ����� ����

    initLayout();                                                                   //�������������� ����

    GWidget::init();                                                                //�������� ������������ �����
}
void UIMainWindow::initLayout()                                                     //�����, ���������������� ���� �������
{
    layout_->setSpacing(5);                                                         //������ ������� ���������� ����� ��������
    layout_->setContentsMargins(1, 1, 1, 1);                                        //������ ������� �� ������ ����
    layout_->setDirection(QVBoxLayout::TopToBottom);                                //������ ���������� ���������

    layout_->addWidget(top_widget_);                                                //��������� ������ �� ����
    layout_->addWidget(body_widget_);                                               //��������� ������ �� ����
    layout_->addWidget(bottom_widget_);                                             //��������� ������ �� ����
}

//
//  ������������� ������
//

void UIMainWindow::resizeEvent(QResizeEvent* event)                                 //�����, �������������� ������ ��������� ������� �������
{
    size_grip_->move(width() - 20, height() - 20);                                  //����������� �������� � ������ ������ ����
    GWidget::resizeEvent(event);                                                    //�������� ������������ �����
}