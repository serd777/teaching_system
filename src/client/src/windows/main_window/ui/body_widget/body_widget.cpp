#include "body_widget.h"

#include <observables/observables.h>
#include <managers/managers.h>

using namespace client::windows::ui_mainwindow;

BodyWidget::BodyWidget(QWidget* parent)                                             //����������� ������
           :GWidget(parent)                                                         //������������� ������������� ������
{
    setObjectName("client-main_wnd-body_wdj");                                      //������ ��� �������

    layout_ = new QVBoxLayout(this);                                                //�������������� ���� �������
    editor_ = new gui::editors::CodeEditor(this);                                   //�������������� �������� ����

    layout_->addWidget(editor_);                                                    //���������� ��������

    initEditor();                                                                   //�������������� ��������
}

void BodyWidget::initEditor()                                                       //�����, ���������������� ��������
{
    auto project_observ = &Observables::Instance().project();                       //����������� ���������� ������

    connect(project_observ, &observables::ProjectObservable::projectLoaded,         //������ ��� ��������
            this,           [this]()                                                //�������� ������
    {
        auto& manager = Managers::Instance().projectManager();                      //�������� �������

        editor_->updateTabs(manager.project().getUnitNames());                      //������ ����� �������
    });
    connect(editor_, &gui::editors::CodeEditor::textUpdated,                        //��� ��������� ������
            this,    [this]()                                                       //�������� ������
    {
        auto& project = Managers::Instance().projectManager().project();            //������� ������

        project.getUnit(editor_->currentModule()).code() = editor_->currentText();  //��������� ��� � ����� ������
    });
}
