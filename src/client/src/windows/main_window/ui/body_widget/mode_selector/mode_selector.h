#pragma once

//
//  ������ ������ ������ ������
//

#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class ModeSelector : public gui::widgets::GWidget                       //������ ������ ������ ������
            {
            public:                                                                 //�������� ������

                explicit ModeSelector(                                              //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~ModeSelector() {};                                                 //���������� ������

            private:                                                                //�������� ������

                QHBoxLayout* layout_;                                               //���� �������
            };
        }
    }
}