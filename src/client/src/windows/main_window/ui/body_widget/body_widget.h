#pragma once

//
//  ������ ���� ����
//

#include <widgets/gwidget.h>
#include <QtWidgets/QVBoxLayout>
#include <editors/code_editor/code_editor.h>

namespace client
{
    namespace windows
    {
        namespace ui_mainwindow
        {
            class BodyWidget : public gui::widgets::GWidget                         //������ ���� ����
            {
            public:                                                                 //�������� ������

                explicit BodyWidget(                                                //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~BodyWidget() {}                                                    //���������� ������

            private:                                                                //�������� ������

                QVBoxLayout* layout_;                                               //���� �������

                gui::editors::CodeEditor* editor_;                                  //�������� ����

                void initEditor();                                                  //�����, ���������������� ��������
            };
        }
    }
}