#include "main_window.h"

#include <QtWidgets/QApplication>
#include <observables/observables.h>

using namespace client::windows;

//
//  ����������� ������
//

MainWindow::MainWindow(QWidget* parent)                                             //����������� ������
           :GMainWindow(parent)                                                     //�������������� ������������ �����
{
    setObjectName("client-main_window");                                            //������ ��� �������

    ui_ = new UIMainWindow(this);                                                   //�������������� �������� ����
}

//
//  ������ �������������
//

void MainWindow::init()                                                             //�����, ���������������� ����
{
    resize(800, 600);                                                               //������ ��������� ������� ����

    setMinimumSize(100, 100);                                                       //����������� ������ ����
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);                           //��������� ����� ��� ���� ����������
    setAttribute(Qt::WA_TranslucentBackground);                                     //��������� ������� ������������ ��� ����������
    setCentralWidget(ui_);                                                          //������ �������� ��� �������� ������
    setWindowIcon(QIcon(":/icons/favicon"));                                        //������������� ������ ����

    ui_->init();                                                                    //�������������� ��������
    initSlots();                                                                    //�������������� �����

    GMainWindow::init();                                                            //�������� ������������ ����� �������������
}
void MainWindow::initSlots()                                                        //����� ������������� ������
{
    auto obsrv = &Observables::Instance().app();                                    //�������� ������ � ����������� ��������� ����������

    //
    //  ���������� ����� � ������� ������������� �������
    //
    connect(obsrv,     &observables::AppObservable::closeApp,                       //��� ������� �� ������ ��������
            this,      [this]()                                                     //������ ���������
    {
        QApplication::quit();                                                       //��������� ������ ����������
    });
    connect(obsrv,     &observables::AppObservable::minimizeMain,                   //��� ������� �� ������ �������������
            this,      [this]()                                                     //������ ���������
    {
        setWindowState(Qt::WindowMinimized);                                        //�������������� ����
    });
    connect(obsrv,     &observables::AppObservable::maximizeMain,                   //��� ������� �� ������ ��������������
            this,      [this]()                                                     //������ ���������
    {
        if (windowState() == Qt::WindowMaximized)                                   //���� ���� �����������������
            setWindowState(Qt::WindowNoState);                                      //������ ��� ����������
        else
            setWindowState(Qt::WindowMaximized);                                    //����� ���������������
    });

    //
    //  ���������� �������� ����
    //
    connect(obsrv,        &observables::AppObservable::moveMain,                    //� ������� ������������� �������
            this,         [this](QPoint point)                                      //��������� ������
    {
        if (windowState() == Qt::WindowMaximized)                                   //���� ���� �����������������
            setWindowState(Qt::WindowNoState);                                      //������ ��� ����������

        move(point);                                                                //������� ���� � ������ �����
    });
}

//
//  ������������� �����������
//

void MainWindow::keyPressEvent(QKeyEvent* e)                                        //���������� ������� ������
{


    QMainWindow::keyPressEvent(e);                                                  //�������� ������������ �����
}