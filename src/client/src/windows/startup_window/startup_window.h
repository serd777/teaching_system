#pragma once

//
//  ��������� ���� ����������
//

#include <widgets/gwidget.h>

namespace client
{
    namespace windows
    {
        class StartupWindow : public gui::widgets::GWidget                          //��������� ���� ����������
        {
        public:                                                                     //�������� ������

            explicit StartupWindow(                                                 //����������� ������
                QWidget* parent = nullptr                                           //Qt ��������
            );

            ~StartupWindow() {};                                                    //���������� ������

        protected: void updateStyle() override;                                     //����� ����������� ����� ����
        };
    }
}