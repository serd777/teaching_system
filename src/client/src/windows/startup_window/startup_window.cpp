#include "startup_window.h"

using namespace client::windows;

StartupWindow::StartupWindow(QWidget* parent)                                       //����������� ������
              :GWidget(parent)                                                      //�������������� ������������ �����
{
    setFixedSize(768, 455);                                                         //������������� ������ ����
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);                           //��������� ����� ��� ���� ����������
    setAttribute(Qt::WA_TranslucentBackground);                                     //��������� ������� ������������ ��� ����������
    setWindowIcon(QIcon(":/icons/favicon"));                                        //������������� ������ ����
}

void StartupWindow::updateStyle()                                                   //����� ����������� ����� ����
{
    setStyleSheet(                                                                  //������ ����� �������
        "QWidget { background-image: url(:/start_logo/" +                           //����������� ���������� ����
        resources::Loc::accessor()->name() +                                        //��� ������ �����������
        ") }"
    );
}
