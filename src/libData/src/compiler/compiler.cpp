#include "compiler.h"

#include <directory/directory.h>
#include <file_types/pas_file/pas_file.h>

#include <QtCore/QProcess>

using namespace data;

Compiler::Compiler()                                                                //����������� ������
{
    tmp_dir_ = filesystem::Directory(tmp_path_);                                    //��������� ���������� �����������
    success_ = false;                                                               //���� ������� ����������

    if (!tmp_dir_.isExists())                                                       //���� ��������� ���������� �� ����������
        tmp_dir_.create();                                                          //�������� �

#if defined(_WIN32) || defined(_WIN64)
    compiler_path_ = "compilers\\pascal\\bin\\i386-win32\\fpc.exe";                 //��� Windows ���������
#endif
}

void Compiler::compile(Project& project)                                            //����� ������������� ����������� ���� �� �������
{
    tmp_dir_.clear();                                                               //������� ��� ����� �� ��������� ����������
    auto files = project.getFiles();                                                //�������� ����� �������

    for (auto& file : files)                                                        //���������� ��� �����
        writeFile(file);                                                            //���������� ��� �� ��������� ����

    auto args = generateArgs(project);                                              //��������� �������
    callCompiler(args);                                                             //�������� ����������

    if (success_)                                                                   //���� ���������� ������ �������
        copyExe(project);                                                           //�������� ����������� ����
}

void Compiler::writeFile(CodeFile& file) const                                      //�����, ������������ ��� � ��������� ����
{
    QString path = tmp_path_ + "/" + file.name() + ".pas";                          //���� �� �����

    filesystem::filetypes::PasFile pas_f(path);                                     //���� � ����� ������
    pas_f.writeCode(file.code());                                                   //���������� ��� �� ��������� ���� ������
}

void Compiler::callCompiler(const QStringList& args)
{
    QProcess process;                                                               //������� ������ ��������

    process.start(compiler_path_, args);                                            //�������� ����������
    process.waitForFinished(-1);                                                    //������� ���������� ��������

    success_   = process.exitCode() ? false : true;                                 //���� ��� ���������� ����� 0, �� �������
    error_msg_ = process.readAllStandardOutput();                                   //���������� ����� ����������� � ��� ������
}
void Compiler::copyExe(Project& project) const                                      //�����, ���������� ����������� ����
{
#if defined(_WIN32) || defined(_WIN64)                                              //��� Windows
    QString from = tmp_path_ + "/" + project.name() + ".exe";                       //������ ��������
    QString to   = project.path() + "/" + project.name() + ".exe";                  //����
#endif
#if defined(__linux__)                                                              //��� Linux
    QString from = tmp_path_ + "/" + project.name() + ".a";                         //������ ��������
    QString to   = project.path() + "/" + project.name() + ".a";                    //����
#endif

    QFile::copy(from, to);                                                          //�������� ����������� �����
}

QStringList Compiler::generateArgs(Project& project) const                          //�����, ������������ ��������� ��� �����������
{
    QStringList result;                                                             //�������������� ������

    result << tmp_path_ + "/program.pas";                                           //���� �� ��������� �����

#if defined(_WIN32) || defined(_WIN64)                                              //��� Windows
    result << "-o" + project.name() + ".exe";                                       //��� ��������� �����
#endif
#if defined(__linux__)                                                              //��� Linux
    result << "-o" + project.name() + ".a";                                         //��� ��������� �����
#endif

    return result;                                                                  //���������� ������
}
