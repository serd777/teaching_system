#pragma once

//
//  ����� ����������� ������ � ����������� Pascal
//

#include <export.h>

#include <project/project.h>
#include <directory/directory.h>

namespace data
{
    class DATA_EXPORT Compiler                                                      //����� ����������� ������ � ����������� Pascal
    {
    public:                                                                         //�������� ������

        static Compiler& Instance()                                                 //����� �������
        {
            static Compiler compiler;                                               //������� ����� ���� �� �� ��� ��� ������
            return compiler;                                                        //���������� ������
        }

        void compile(Project& project);                                             //����� ������������� ����������� ���� �� �������

        bool isSuccess() const { return success_; }                                 //����� �������� �� ���������� ����������
        const QString& error() const { return error_msg_; }                         //�����, ������������ ����� ������ ����������

    private:                                                                        //�������� ������

        Compiler();                                                                 //����������� ������
        ~Compiler() {}                                                              //���������� ������

        Compiler(Compiler const&)             = delete;                             //��������� ����������� �����������
        Compiler& operator= (Compiler const&) = delete;                             //� �������� ������������

        const QString         tmp_path_ = "tmp/compiler";                           //���� �� ��������� �����
        filesystem::Directory tmp_dir_;                                             //��������� ����������

        bool success_;                                                              //���� �������� ����������

        QString compiler_path_;                                                     //���� �� �����������
        QString error_msg_;                                                         //����� ������ �����������

        void writeFile(CodeFile& file) const;                                       //�����, ������������ ��� � ��������� ����
        void callCompiler(const QStringList& args);                                 //�����, ���������� ���������� �������
        void copyExe(Project& project) const;                                       //�����, ���������� ����������� ����

        QStringList generateArgs(Project& project) const;                           //�����, ������������ ��������� ��� �����������
    };
}
