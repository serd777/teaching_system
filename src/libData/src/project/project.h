#pragma once

//
//  ����� ���������� �����
//

#include <export.h>
#include <QtCore/QVector>

#include "./code_file/code_file.h"

namespace data
{
    class DATA_EXPORT Project                                                       //����� ���������� �����
    {
        typedef QVector<CodeFile> VecFiles;                                         //��� ������ - ������ ��������� ������
        typedef QVector<QString>  VecSt;                                            //��� ������ - ������ �����

    public:                                                                         //�������� ������

        explicit Project(                                                           //����������� ������
            QString path                                                            //���� �� ����� �������
        );

        Project()  {}                                                               //������ �����������
        ~Project() {}                                                               //���������� ������

        void launchExe() const;                                                     //�����, ����������� ����������� ���� �������

        bool addUnit(const QString& name);                                          //�����, ����������� ����� ������
        bool deleteUnit(const QString& name);                                       //�����, ��������� ������ �� �����

        const auto& name() { return name_; }                                        //������ � ����� �������
        const auto& path() { return path_; }                                        //������ � ���� �������

        CodeFile& getUnit(const QString& name);                                     //�����, ������������ ������ �� �����
        VecFiles  getFiles() const;                                                 //�����, ������������ ������ ������
        VecSt     getUnitNames();                                                   //�����, ������������ ������ � ������� �������

    private:                                                                        //�������� ������

        QString  path_;                                                             //���� �������
        QString  name_;                                                             //��� �������

        CodeFile program_;                                                          //�������� ���� ���������
        VecFiles units_;                                                            //������ ���������

        void loadProject();                                                         //�����, ����������� ������
    };
}