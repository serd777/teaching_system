#pragma once

//
//  ����� ����������� ������������� ����� � ����� (��������� � �������������)
//

#include <semantic_tree/semantic_tree.h>

namespace data
{
    class CodeFile                                                                  //����� ����������� ������������� ����� � ����� (��������� � �������������)
    {
    public:                                                                         //�������� ������

        explicit CodeFile(                                                          //����������� ������
            const QString& path,                                                    //���� �� ����������
            const QString& name                                                     //��� �����
        );

        CodeFile(const CodeFile& file);                                             //����������� �����������
        CodeFile()  {}                                                              //������ �����������

        ~CodeFile() {}                                                              //���������� ������

        CodeFile& operator=(const CodeFile& file);                                  //�������� �����������

        const auto& path() { return path_; }                                        //������ � ����
        const auto& name() { return name_; }                                        //������ � �����

        auto& code() { return code_; }                                              //������ � ���� �� ������
        auto& tree() { return tree_; }                                              //������ � ������ �� ������

        void load();                                                                //�����, ����������� ������

    private:                                                                        //�������� ������

        QString path_;                                                              //���� �� ���������� � �������
        QString name_;                                                              //��� �����
        QString code_;                                                              //��������� �������������

        semantic::SemanticTree tree_;                                               //������������� �������������

        void init();                                                                //�����, ���������������� �����
        
        void loadTree();                                                            //�����, ����������� ������ �� �����
        void loadCode();                                                            //�����, ����������� ��� �� �����
    };
}