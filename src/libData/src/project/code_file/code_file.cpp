#include "code_file.h"

#include <directory/directory.h>

#include <file_types/pas_file/pas_file.h>
#include <file_types/tree_file/tree_file.h>

using namespace data;

CodeFile::CodeFile(const QString& path, const QString& name)                        //����������� ������
{
    path_ = path;                                                                   //���������� ����
    name_ = name;                                                                   //���������� ���

    init();                                                                         //�������������� �����
    load();                                                                         //��������� ������
}
CodeFile::CodeFile(const CodeFile& file)                                            //����������� �����������
{
    path_ = file.path_;                                                             //�������� ����
    name_ = file.name_;                                                             //�������� ���
    code_ = file.code_;                                                             //�������� ���

    //TODO: �������� ����������� ������
}

CodeFile& CodeFile::operator=(const CodeFile& file)                                 //�������� �����������
{
    path_ = file.path_;                                                             //�������� ����
    name_ = file.name_;                                                             //�������� ���
    code_ = file.code_;                                                             //�������� ���

    //TODO: �������� ����������� ������

    return *this;                                                                   //���������� ������������� ������
}

void CodeFile::init()                                                               //����� �������������
{
    filesystem::Directory dir(path_);                                               //�������� ������ � ����������

    if (!dir.isExists())                                                            //���� ���������� �� ����������
        dir.create();                                                               //�� ������� �
}

//
//  ������ ��������������
//

void CodeFile::load()                                                               //�����, ����������� ������
{
    loadCode();                                                                     //��������� ���
    loadTree();                                                                     //��������� ������
}
void CodeFile::loadCode()
{   
    filesystem::filetypes::PasFile  code_file(path_ + "/" + name_ + ".pas");        //���� � ��������� ��������������
    code_ = code_file.getCode();                                                    //��������� �� �����
}
void CodeFile::loadTree()
{
    filesystem::filetypes::TreeFile tree_file(path_ + "/" + name_ + ".tree");       //���� � ������������� ��������������
}