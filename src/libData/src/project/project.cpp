#include "project.h"

#include <directory/directory.h>
#include <localizations/loc.h>

#include <exception_handler/exception_handler.h>
#include <exceptions/project_exception/project_exception.h>

#include <QtCore/QProcess>

#if defined(_WIN32) || defined(_WIN64)
    #include <Windows.h>
#endif

using namespace data;
using namespace debug::exceptions;

Project::Project(QString path)                                                      //����������� ������
{
    path_ = path;                                                                   //���������� ���� �������

    loadProject();                                                                  //�������� ������
}

//
//  ������ ��������������
//

void Project::launchExe() const                                                     //�����, ����������� ����������� ���� �������
{
    QProcess process;                                                               //������� ������ ��������

#if defined(_WIN32) || defined(_WIN64)                                              //���� ��������� Windows
    process.setCreateProcessArgumentsModifier                                       //�������� ������������ ��� ��������
    (
        [](QProcess::CreateProcessArguments * args)                                 //������ ������� �������� ���������� ��������
    {
        args->flags |= CREATE_NEW_CONSOLE;                                          //��������� � ��������� �������

        args->startupInfo->dwFlags &= ~STARTF_USESTDHANDLES;                        //������� ����� ������������ STD ������
        args->startupInfo->dwFlags |= STARTF_USEFILLATTRIBUTE;                      //������� ����� ������������ ����������� ��������
        args->startupInfo->dwFillAttribute = BACKGROUND_BLUE |                      //��� ������ �����
            FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY;               //� ���� ������ ������
    });

    QString path = path_ + "/" + name_ + ".exe";                                    //���� �������
#endif

    process.start(path);                                                            //��������� �������
    process.waitForFinished(-1);                                                    //������� ���������� ��������
}

bool Project::addUnit(const QString& name)                                          //�����, ����������� ����� ������
{
    if (program_.name() == name)                                                    //���� ��� == ����� ��������� ������
        return false;                                                               //�� �������� �����������

    for (auto& unit : units_)                                                       //���������� ������
        if (unit.name() == name)                                                    //���� ��� ������ == ����������� �����
            return false;                                                           //�� �������� �����������

    units_.push_back(CodeFile(path_ + "/" + name, name));                           //��������� ����� ������

    return true;                                                                    //��������� �������
}
bool Project::deleteUnit(const QString& name)                                       //�����, ��������� ������ �� �����
{
    return true;
}

//
//  ������ ����� ������
//

Project::VecFiles Project::getFiles() const                                         //�����, ������������ ������ ������
{
    VecFiles vec;                                                                   //�������������� ������

    vec.push_back(program_);                                                        //������ �������� ������
    for (auto& unit : units_)                                                       //���������� ��������������� ������
        vec.push_back(unit);                                                        //������ � ������

    return vec;                                                                     //���������� ������
}
Project::VecSt Project::getUnitNames()                                              //�����, ������������ ������ � ������� �������
{
    VecSt result;                                                                   //�������������� ������
    result.push_back(program_.name());                                              //��� ���������

    for (auto& unit : units_)                                                       //���������� ������
        result.push_back(unit.name());                                              //������ ��� ������

    return result;                                                                  //���������� ������
}
CodeFile& Project::getUnit(const QString& name)                                     //�����, ������������ ������ �� �����
{
    if (program_.name() == name)                                                    //���� ��� == ����� ��������� ������
        return program_;                                                            //�� ���������� �������� ������

    for (auto& unit : units_)                                                       //���������� ������
        if (unit.name() == name)                                                    //���� ��� ������ == ����������� �����
            return unit;                                                            //���������� ������

    auto message = (*resources::Loc::accessor())["exceptions.unit_not_found"];      //������� ��������� �� ��������
    debug::ExceptionHandler::Instance().projectException(message);                  //�������� ���������� ����, ��� ������ �� ������

    return CodeFile();                                                              //���������� ������ ������ ��� ��������
}

//
//  ������ �������������
//

void Project::loadProject()                                                         //�����, ����������� ������
{
    filesystem::Directory dir(path_);                                               //���������� � ��������

    name_ = dir.name();                                                             //��� ������� = ����� ����������
    auto files_p = dir.getSubDirsPaths();                                           //���� ���� ��������� ����������

    if (!files_p.count())                                                           //���� ����� � �������� ������
        program_ = CodeFile(path_ + "/program", "program");                         //������� �������� ������

    for(auto& path : files_p)                                                       //���������� ��� ��������� ����������
    {
        filesystem::Directory f_dir(path);                                          //�������� ������ � ����������

        auto name = f_dir.name();                                                   //��� ���������� ������
        if (name == "program")                                                      //���� ��� �������� ������
            program_ = CodeFile(path, name);                                        //������ ������� �������� ������
        else
            units_.push_back(CodeFile(path, name));                                 //����� ������ � ���������
    }
}
