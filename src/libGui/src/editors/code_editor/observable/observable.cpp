#include "observable.h"

#include <syntax/accessor/accessor.h>

using namespace gui::editors::code_editor;

Observable::Observable(QObject* parent)                                             //����������� ������
           :QObject(parent)                                                         //�������������� ������������ �����
{

}

//
//  ������ �������������
//

void Observable::setEditor(TextEditor* editor)                                      //�����, �������� ��������� ��������
{
    editor_ = editor;                                                               //������ ���������� ���������
}

//
//  ����� ���������
//

void Observable::onOperatorFound(const QString word)                                //���� ��������� ���������� ���������
{
    auto syntax = semantic::SyntaxAccessor::get();                                  //������ � ����������
    auto key    = syntax->getKey(word);                                             //�������� ���� ���������
    auto spaces = editor_->getSpacesAtStart();                                      //�������� ���-�� �������� � ������ ������� ������

    addOperator(key, spaces);                                                       //��������� ������� ��������

    if(syntax->hasTemplate(key))                                                    //���� � ����� ���� ������������� ������
    {
        auto tmpl  = syntax->getTemplate(key);                                      //�������� ������������� ������
        auto count = tmpl.countNodes();                                             //�������� ���-�� �����

        for(decltype(count) i = 0; i < count; i++)                                  //���������� ������������� ����
        {
            auto value  = tmpl.getValue(i);                                         //�������� ��������
            auto is_key = tmpl.isKey(i);                                            //�������� ���� �������� �� ������

            if (is_key) addOperator(value, spaces);                                 //���� ��� ����, �� ��������� ��� ��������
            else
            {
                auto pos = editor_->getCursorPos();                                 //������� ������� � ������
                auto length = value.length();                                       //����� �����

                tree_.increasePos(pos, length);                                     //�������� ������� � ����� ������
                editor_->addText(value, spaces);                                    //����� ��������� ������� ����� � ��������
            }
        }
    }
}
void Observable::onOperatorDelete(int pos)                                          //���� ��������� �������� ���������
{
    auto node = tree_.findNode(pos);                                                //������� ���� � ������

    if (node->type()->isDeletable())                                                //���� ���� ���������
        delOperator(pos);                                                           //�������� �������� ���������
}

void Observable::onCharsAdded(int pos, int count)                                   //��� ���������� N-��� ���-�� ��������
{
    tree_.increasePos(pos, count);                                                  //�������� �������������
}
void Observable::onCharsDeleted(int pos, int count)                                 //��� �������� N-��� ���-�� ��������
{
    tree_.decreasePos(pos, count);                                                  //�������� �������������
}

//
//  ������ ��������������: �������� - ������
//

void Observable::addOperator(const QString& key, const int& spaces)                 //�����, ����������� �������� � �������� � � ������
{
    auto syntax = semantic::SyntaxAccessor::get();                                  //������ � ����������
    auto word = syntax->getWord(key);                                               //��������� �������������
    auto pos = editor_->getCursorPos();                                             //������� ������
    auto length = word.length() + 1;                                                //����� ������������ �����

    editor_->addText(word, spaces);                                                 //��������� � �������� �����
    editor_->addText(" ", spaces);                                                  //��������� ������ � �������� ����� ���������

    tree_.increasePos(pos, length);                                                 //�������� ������� � ����� ������
    tree_.addNode(key, pos);                                                        //��������� � ������
}
void Observable::delOperator(const int& pos)                                        //�����, ��������� �������� �� ��������� � ������
{
    tree_.deleteNode(pos);                                                          //������� ���� �� ������
    if (editor_->isCurLineEmpty())                                                  //���� ������� ����� ������
        deleteLine();                                                               //�� ������� �

    while(!tree_.isDelQueueEmpty())                                                 //���� � ������� �������� ���-�� �����
    {
        auto del_pos = tree_.popDelQueue();                                         //������� ������� �� �������
        auto length  = editor_->getWord(del_pos).length();                          //����� ���������� �����

        editor_->deleteWord(del_pos);                                               //������� ����� �� ���������
        tree_.decreasePos(del_pos, length);                                         //�������� ���������������� ������

        if (editor_->isCurLineEmpty())                                              //���� ������� ����� ������
            deleteLine();                                                           //�� ������� �
    }
}

void Observable::deleteLine()                                                       //�����, ��������� ������� ������
{
    auto pos = editor_->getCurLinePos();                                            //�������� ������� �����
    auto length = editor_->deleteCurrentLine();                                     //���-�� ��������� ��������

    tree_.decreasePos(pos, length);                                                 //�������� ���������������� ������
}
