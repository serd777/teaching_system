#pragma once

//
//  ����������� ��������� ������
//

#include <QtCore/QObject>

#include <semantic_tree/semantic_tree.h>
#include <editors/code_editor/text_editor/text_editor.h>

namespace gui
{
    namespace editors
    {
        namespace code_editor
        {
            class Observable : public QObject                                       //����������� ��������� ������
            {
                Q_OBJECT                                                            //MOC ����

            public:                                                                 //�������� ������

                explicit Observable(                                                //����������� ������
                    QObject* parent = nullptr                                       //Qt ��������
                );
                ~Observable() {}                                                    //���������� ������

                void setEditor(TextEditor* editor);                                 //�����, �������� ��������� ��������

            public slots:                                                           //�������� �����

                void onOperatorFound(const QString word);                           //���� ��������� ���������� ���������
                void onOperatorDelete(int pos);                                     //���� ��������� �������� ���������

                void onCharsAdded(int pos, int count);                              //��� ���������� N-��� ���-�� ��������
                void onCharsDeleted(int pos, int count);                            //��� �������� N-��� ���-�� ��������

            private:                                                                //�������� ������

                TextEditor* editor_ = nullptr;                                      //��������� ��������
                semantic::SemanticTree tree_;                                       //������������� ������

                void addOperator(const QString& key, const int& spaces);            //�����, ����������� �������� � �������� � � ������
                void delOperator(const int& pos);                                   //�����, ��������� �������� �� ��������� � ������
                
                void deleteLine();                                                  //�����, ��������� ������� ������
            };
        }
    }
}