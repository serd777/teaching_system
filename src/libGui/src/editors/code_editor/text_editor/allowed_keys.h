#pragma once

//
//  ������ ��������� ����� ������ ��� �����
//

#include <QtCore/QVector>

namespace gui
{
    namespace editors
    {
        namespace code_editor
        {
            static QVector<int> allowed_keys = {                                    //������ ����������� ����� ������� ������
                /*Key_0 =*/                                 0x30,                   //0
                /*Key_1 =*/                                 0x31,                   //1
                /*Key_2 =*/                                 0x32,                   //2
                /*Key_3 =*/                                 0x33,                   //3
                /*Key_4 =*/                                 0x34,                   //4
                /*Key_5 =*/                                 0x35,                   //5
                /*Key_6 =*/                                 0x36,                   //6
                /*Key_7 =*/                                 0x37,                   //7
                /*Key_8 =*/                                 0x38,                   //8
                /*Key_9 =*/                                 0x39,                   //9
                /*Key_A =*/                                 0x41,                   //A, a
                /*Key_B =*/                                 0x42,                   //B, b
                /*Key_C =*/                                 0x43,                   //C, c
                /*Key_D =*/                                 0x44,                   //D, d
                /*Key_E =*/                                 0x45,                   //E, e
                /*Key_F =*/                                 0x46,                   //F, f
                /*Key_G =*/                                 0x47,                   //G, g
                /*Key_H =*/                                 0x48,                   //H, h
                /*Key_I =*/                                 0x49,                   //I, i
                /*Key_J =*/                                 0x4A,                   //J, j
                /*Key_K =*/                                 0x4B,                   //K, k
                /*Key_L =*/                                 0x4C,                   //L, l
                /*Key_M =*/                                 0x4D,                   //M, m
                /*Key_N =*/                                 0x4E,                   //N, n
                /*Key_O =*/                                 0x4F,                   //O, o
                /*Key_P =*/                                 0x50,                   //P, p
                /*Key_Q =*/                                 0x51,                   //Q, q
                /*Key_R =*/                                 0x52,                   //R, r
                /*Key_S =*/                                 0x53,                   //S, s
                /*Key_T =*/                                 0x54,                   //T, t
                /*Key_U =*/                                 0x55,                   //U, u
                /*Key_V =*/                                 0x56,                   //V, v
                /*Key_W =*/                                 0x57,                   //W, w
                /*Key_X =*/                                 0x58,                   //X, x
                /*Key_Y =*/                                 0x59,                   //Y, y
                /*Key_Z =*/                                 0x5A,                   //Z, z
                /*Key_AddEqual*/                            0xBB,                   //���� � �����
                /*Key_Comma =*/                             0xBC,                   //�������
                /*Key_Subtract*/                            0xBD,                   //�����
                /*Key_Dot =*/                               0xBE,                   //�����
                /*Key_Question =*/                          0xBF,                   //������
                /*Key_BackSlash =*/                         0xDC,                   //�������� ����
            };
        }
    }
}