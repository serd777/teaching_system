#include "text_editor.h"
#include "allowed_keys.h"

#include <container_search.h>
#include <syntax/accessor/accessor.h>
#include <code_helper/code_helper.h>

#include <QtGui/QTextBlock>
#include <QtWidgets/QToolTip>

using namespace gui::editors::code_editor;
using namespace templates;

TextEditor::TextEditor(QWidget* parent)                                             //����������� ������
           :GPlainTextEdit(parent)                                                  //������������� ������������� ������
{
    //line_area_ = new LineArea(this);                                                //�������������� ������ ����� ����
}

void TextEditor::init()                                                             //����� �������������
{
    //line_area_->init();                                                             //�������������� ������ ����� ����

    GPlainTextEdit::init();                                                         //�������� ������������ ������
}

//
//  ������������� �����������
//

void TextEditor::keyPressEvent(QKeyEvent* e)                                        //����� ��������� ������� ������
{
    auto key = e->key();                                                            //�������� ��� ������� ������
    auto syntax = semantic::SyntaxAccessor::get();                                  //�������� ������ � ����������
    auto pos = getCursorPos();                                                      //������� �������

    if(key == Qt::Key_End)
    {
        auto word = getCurWord();
        if (syntax->isOperator(word))
            showTooltip(syntax->getKey(word));
    }

    if(isInputKey(key))                                                             //���� ��� ������� �����
    {
        GPlainTextEdit::keyPressEvent(e);                                           //�������� ������������ �����

        auto cur_word = getCurWord();                                               //������� �����
        if (syntax->isInsertableOperator(cur_word))                                 //���� ��������� ����� - ����������� ��������
        {
            delCurWord();                                                           //������� ������� �����
            emit operatorFound(cur_word);                                           //�� �������� ������ ������� ��������� � ������
        }
        else emit charsAdded(pos, 1);                                               //�������� ���� ������
    }

    if(isDeleteKey(key))                                                            //���� ��� ������ ��������
    {
        auto cur_word = getCurWord();                                               //������� �����
        if(syntax->isOperator(cur_word))                                            //���� ������� ����� ��������
            emit operatorDeleted(getCurWordPos());                                  //�������� ������
    }
}

//
//  ������ �������� ��������� �������
//

bool TextEditor::isInputKey(int key) const                                          //����� �������� ���� ������ �� ������������ �� ����
{
    return ContainerSearch::find(allowed_keys, key) != ContainerSearch::NOT_FOUND;  //���������� ��������� ���������
}
bool TextEditor::isDeleteKey(int key) const                                         //����� �������� ���� ������ �� ������ ��������
{
    return key == Qt::Key_Backspace ||                                              //��� ��������� (������� ��������) ���
           key == Qt::Key_Delete;                                                   //��� ������ Delete
}
bool TextEditor::isCurLineEmpty() const                                             //�����, ����������� �� ������� ������� ������
{
    auto text = textCursor().block().text();                                        //����� ������� �����

    for (auto& ch : text)                                                           //���������� ������� ������
        if (ch != ' ')                                                              //���� ��������� ���-�� ����� �������
            return false;                                                           //�� ������ �� ������

    return true;                                                                    //������ - �����
}

//
//  ������ �������������� � �������
//

void TextEditor::addText(                                                           //�����, ����������� �����
    const QString& text,                                                            //�����
    const int& spaces                                                               //���-�� �������� � ������ ������
)
{
    insertPlainText(text);                                                          //��������� �����

    if (text == "\n")                                                               //���� ��� ��� ������� �� ����� ������
    {
        auto pos = getCursorPos();                                                  //������� �������

        for (int i = 0; i < spaces; i++)                                            //�� ������� ����������� �������
            insertPlainText(" ");                                                   //����� ������ �������� � �����

        emit charsAdded(pos, spaces);                                               //��������� 2 ������� + ���-�� ��������
    }
}
void TextEditor::addNewLine()                                                       //�����, ����������� ����� ������
{
    auto spaces = getSpacesAtStart();                                               //�������� ���-�� �������� � ������� ������
    auto pos = getCursorPos();                                                      //������� �������
    insertPlainText("\n");                                                          //��������� ����� ������

    for (int i = 0; i < spaces; i++)                                                //������� ����������� �������
        insertPlainText(" ");                                                       //����� ������ �������� � �����

    emit charsAdded(pos, 1 + spaces);                                               //��������� 2 ������� + ���-�� ��������
}

int TextEditor::getCurLinePos() const                                               //�����, ������������ ��������� ������� ������ � ������
{
    auto cursor = textCursor();                                                     //�������� ������ ������
    cursor.movePosition(QTextCursor::StartOfBlock);                                 //��������� � ������ �����

    return cursor.position();                                                       //���������� �������
}
int TextEditor::getCursorPos() const                                                //�����, ������������ ������� �������
{
    return textCursor().position();                                                 //���������� �������
}
int TextEditor::getSpacesAtStart() const                                            //�����, ������������ ���-�� �������� � ������ ������
{
    auto line = textCursor().block().text();                                        //�������� ������� ������
    int count = 0;                                                                  //���-�� �������� � ������ ������

    for (auto& ch : line)                                                           //���������� ������� ������
    {
        if (ch != ' ') break;                                                       //���� ����������� ������� � ������
        count++;                                                                    //����� ���������� �������
    }

    return count;                                                                   //���������� ���������
}

void TextEditor::deleteWord(const int& pos)                                         //�����, ��������� ����� �� ��������� �������
{
    auto cursor = textCursor();                                                     //������ ������
    cursor.setPosition(pos);                                                        //�������� � ��������� �������

    setTextCursor(cursor);                                                          //������ ���� ������
    delCurWord();                                                                   //������� ������� �����
}
QString TextEditor::getWord(const int& pos) const                                   //�����, ������������ ����� �� ��������� �������
{
    auto cursor = textCursor();                                                     //������ ������
    cursor.setPosition(pos);                                                        //�������� � ��������� �������
    cursor.select(QTextCursor::WordUnderCursor);                                    //�������� �����

    return cursor.selectedText();                                                   //���������� �����
}

int TextEditor::getCurWordPos() const                                               //�����, ������������ ������� �������� �����
{
    auto cursor = textCursor();                                                     //�������� ������ ������
    cursor.movePosition(QTextCursor::StartOfWord);                                  //��������� � ������ �����

    return cursor.position();                                                       //���������� �������
}

QString TextEditor::getCurWord() const                                              //�����, ������������ ������� ����� ��� ��������
{
    auto cursor = textCursor();                                                     //�������� ������ ������
    cursor.select(QTextCursor::WordUnderCursor);                                    //�������� �����

    return cursor.selectedText();                                                   //���������� �����
}
void TextEditor::delCurWord()                                                       //�����, ��������� ������� ����� ��� ��������
{
    auto cursor = textCursor();                                                     //�������� ������ ������
    cursor.select(QTextCursor::WordUnderCursor);                                    //�������� �����
    cursor.removeSelectedText();                                                    //������� �����

    setTextCursor(cursor);                                                          //������ ������ ������ �������
}

int TextEditor::deleteCurrentLine()                                                 //�����, ��������� ������� ������
{
    auto cursor = textCursor();                                                     //��������� ������
    auto length = cursor.block().text().count() + 1;                                //���-�� �������� � ������ + ������ �������� ������

    cursor.select(QTextCursor::BlockUnderCursor);                                   //�������� ������
    cursor.removeSelectedText();                                                    //������� �

    setTextCursor(cursor);                                                          //������ ������ ������ �������

    return length;                                                                  //���������� ���-�� ��������� ��������
}

//
//  ������ ������ ���������������� �����������
//

void TextEditor::showTooltip(const QString& key)                                    //�����, ���������� ����������� ���������
{
    auto& helper = resources::CodeHelper::Instance();                               //������ � ���������
    auto message = helper.getShort(key);                                            //�������� ���������

    if(!message.isEmpty())                                                          //���� ���� ���������
        QToolTip::showText(cursor().pos(), message);                                //�� ������� �
}