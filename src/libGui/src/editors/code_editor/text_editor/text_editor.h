#pragma once

//
//  ��������� �������� ����
//

#include <widgets/gplaintextedit.h>

#include "./line_area/line_area.h"

namespace gui
{
    namespace editors
    {
        namespace code_editor
        {
            class TextEditor : public widgets::GPlainTextEdit                       //��������� �������� ����
            {
                Q_OBJECT                                                            //MOC ����

            public:                                                                 //�������� ������

                explicit TextEditor(                                                //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~TextEditor() {}                                                    //���������� ������

                void init() override;                                               //����� �������������

                void addText(const QString& text, const int& spaces);               //�����, ����������� �����
                void addNewLine();                                                  //�����, ����������� ����� ������
                
                void showTooltip(const QString& key);                               //�����, ���������� ����������� ���������

                int getCurLinePos() const;                                          //�����, ������������ ��������� ������� ������ � ������
                int getCursorPos() const;                                           //�����, ������������ ������� �������
                int getSpacesAtStart() const;                                       //�����, ������������ ���-�� �������� � ������ ������

                void deleteWord(const int& pos);                                    //�����, ��������� ����� �� ��������� �������
                QString getWord(const int& pos) const;                              //�����, ������������ ����� �� ��������� �������

                bool isCurLineEmpty() const;                                        //�����, ����������� �� ������� ������� ������
                int  deleteCurrentLine();                                           //�����, ��������� ������� ������

            signals:                                                                //�������

                void operatorFound(const QString word);                             //������ ���������� ���������
                void operatorDeleted(int pos);                                      //������ ������� �� �������� ���������

                void charsAdded(int pos, int count);                                //������ ����������� ��������
                void charsDeleted(int pos, int count);                              //������ ��������� ��������

            protected:                                                              //����������� ������

                void keyPressEvent(QKeyEvent * e) override;                         //����� ��������� ������� ������

            private:                                                                //�������� ������

                LineArea* line_area_;                                               //������ � �������� ����� ����

                bool isInputKey(int key) const;                                     //����� �������� ���� ������ �� ������������ �� ����
                bool isDeleteKey(int key) const;                                    //����� �������� ���� ������ �� ������ ��������

                int     getCurWordPos() const;                                      //�����, ������������ ������� �������� �����
                QString getCurWord() const;                                         //�����, ������������ ������� ����� ��� ��������
                void    delCurWord();                                               //�����, ��������� ������� ����� ��� ��������
            };
        }
    }
}