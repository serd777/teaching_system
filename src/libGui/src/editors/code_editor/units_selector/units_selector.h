#pragma once

//
//  ������ ������ ������
//

#include <QtWidgets/QTabBar>

namespace gui
{
    namespace editors
    {
        namespace code_editor
        {
            class UnitsSelector : public QTabBar                                    //������ ������ ������
            {
                typedef QVector<QString> VecSt;                                     //��� ������ - ������ �����

            public:                                                                 //�������� ������

                explicit UnitsSelector(                                             //����������� ������
                    QWidget* parent = nullptr                                       //Qt ��������
                );

                ~UnitsSelector() {}                                                 //���������� ������

                void updateTabs(const VecSt& names);                                //�����, ����������� ������� � ������� �������

            private:                                                                //�������� ������

                int tabs_count_;                                                    //���-�� �������
            };
        }
    }
}