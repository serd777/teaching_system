#include "units_selector.h"

using namespace gui::editors::code_editor;

UnitsSelector::UnitsSelector(QWidget* parent)                                       //����������� ������
              :QTabBar(parent)                                                      //�������������� ������������ �����
{
    setExpanding(false);                                                            //��������� ���������� ������������

    tabs_count_ = 0;                                                                //�������������� ���-�� �������
}

//
//  ������ �������������� � ���������
//

void UnitsSelector::updateTabs(const VecSt& names)                                  //�����, ����������� ������� � ������� �������
{
    for (int i = 0; i < tabs_count_; i++)                                           //���������� ��� �������
        removeTab(0);                                                               //������� �� � ������

    for (auto& name : names)                                                        //���������� ���������� ����� �������
        addTab(name);                                                               //��������� �� ������

    tabs_count_ = names.count();                                                    //���������� ���-�� �������
}
