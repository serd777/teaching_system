#pragma once

//
//  �������� ����
//

#include <export.h>
#include <widgets/gwidget.h>
#include <QtWidgets/QHBoxLayout>

#include "./text_editor/text_editor.h"
#include "./text_highlighter/text_highlighter.h"
#include "./observable/observable.h"
#include "./units_selector/units_selector.h"

namespace gui
{
    namespace editors
    {
        class GUI_EXPORT CodeEditor : public widgets::GWidget                       //�������� ����
        {
            Q_OBJECT                                                                //MOC ����

            typedef QVector<QString> VecSt;                                         //��� ������ - ������ �����

        public:                                                                     //�������� ������

            explicit CodeEditor(                                                    //����������� ������
                QWidget* parent = nullptr                                           //Qt ��������
            );

            ~CodeEditor() {}                                                        //���������� ������

            void updateTabs(const VecSt& names);                                    //����� ����������� ������� �������

            QString currentModule() const;                                          //�����, ������������ �������� �������� ������
            QString currentText() const;                                            //�����, ������������ ������� ����� � ����

        signals:                                                                    //�������

            void textUpdated();                                                     //������ ����, ��� ����� � ������� ������ ��� ��������

        private:                                                                    //�������� ������

            QVBoxLayout* layout_;                                                   //���� �������

            code_editor::TextEditor*      text_editor_;                             //�������� ������
            code_editor::TextHighlighter* text_highlighter_;                        //��������� ������
            code_editor::Observable*      observable_;                              //����������� ���������
            code_editor::UnitsSelector*   units_selector_;                          //������ ������ ������

            void initSlots();                                                       //�����, ������������ ������� � ������
        };
    }
}