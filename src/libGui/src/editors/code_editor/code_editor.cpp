#include "code_editor.h"

using namespace gui::editors;
using namespace code_editor;

CodeEditor::CodeEditor(QWidget* parent)                                             //����������� ������
           :GWidget(parent)                                                         //�������������� ������������ �����
{
    text_editor_      = new TextEditor(this);                                       //�������� ������
    text_highlighter_ = new TextHighlighter(text_editor_->document());              //��������� ������
    observable_       = new Observable(this);                                       //����������� ���������
    units_selector_   = new UnitsSelector(this);                                    //������ ������ ������

    layout_ = new QVBoxLayout(this);                                                //���� �������
    layout_->addWidget(units_selector_);                                            //��������� ������ ������ ������
    layout_->addWidget(text_editor_);                                               //��������� �������� ������

    observable_->setEditor(text_editor_);                                           //������ ��������� �������� �����������

    initSlots();                                                                    //�������������� �����
}

void CodeEditor::initSlots()
{
    connect(text_editor_, SIGNAL(operatorFound(const QString)),                     //���������� ������ ���������� ������ ���������
            observable_,  SLOT(onOperatorFound(const QString)));                    //� ����� ����������� � �����������
    connect(text_editor_, SIGNAL(operatorDeleted(int)),                             //���������� ������ �������� ���������
            observable_,  SLOT(onOperatorDelete(int)));                             //� ����� ����������� � �����������

    connect(text_editor_, SIGNAL(charsAdded(int, int)),                             //���������� ������ ���������� ��������
            observable_,  SLOT(onCharsAdded(int, int)));                            //� ����� ����������� � �����������
    connect(text_editor_, SIGNAL(charsDeleted(int, int)),                           //���������� ������ �������� ��������
            observable_,  SLOT(onCharsDeleted(int, int)));                          //� ����� ����������� � �����������

    connect(text_editor_, SIGNAL(textChanged()),                                    //��� ��������� ������
            this,         SIGNAL(textUpdated()));                                   //���������� ������
}

//
//  ������ ��������������
//

void CodeEditor::updateTabs(const VecSt& names)                                     //����� ����������� ������� �������
{
    units_selector_->updateTabs(names);                                             //��������� ������� �������
}

QString CodeEditor::currentModule() const                                           //�����, ������������ �������� �������� ������
{
    return units_selector_->tabText(units_selector_->currentIndex());               //���������� ��������
}
QString CodeEditor::currentText() const                                             //�����, ������������ ������� ����� � ����
{
    return text_editor_->toPlainText();                                             //���������� ����� ����
}
