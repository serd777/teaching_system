#pragma once

//
//  ����� �������������� ��������� ����
//

#include <QtGui/QSyntaxHighlighter>
#include <QtCore/QHash>

namespace gui
{
    namespace editors
    {
        namespace code_editor
        {
            class TextHighlighter : public QSyntaxHighlighter                       //����� �������������� ��������� ����
            {
                typedef QHash<QString, QTextCharFormat> HashTable;                  //��� ������ - ��� �������, ����� - ����������

            public:                                                                 //�������� ������

                explicit TextHighlighter(                                           //����������� ������
                    QTextDocument* document                                         //��������� �� ��������� ��������
                );

                ~TextHighlighter() {};                                              //���������� ������

            protected: void highlightBlock(const QString& text) override;           //����� ��������� ��������� ���������� ������
            private:                                                                //�������� ������

                HashTable table_;                                                   //������� ���������

                void updateOperators();                                             //�����, ����������� ��������� ����������
                void updateOperands();                                              //�����, ����������� ��������� ���������
            };
        }
    }
}