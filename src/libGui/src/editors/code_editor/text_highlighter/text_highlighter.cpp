#include "text_highlighter.h"
#include <syntax/accessor/accessor.h>

using namespace gui::editors::code_editor;

TextHighlighter::TextHighlighter(QTextDocument* document)                           //����������� ������
                :QSyntaxHighlighter(document)                                       //�������������� ������������ �����
{
    updateOperators();                                                              //��������� ��������� ����������
    updateOperands();                                                               //��������� ��������� ���������
}

//
//  ������ ������
//

void TextHighlighter::highlightBlock(const QString& text)                           //����� ��������� ��������� ���������� ������
{
    auto length = text.length();                                                    //�������� ����� ������
    auto words  = text.split(' ');                                                  //��������� ������ �� �����

    decltype(length) index = 0;                                                     //������� ������

    for(auto& word : words)                                                         //���������� ����� � ������
    {
        auto count = word.length();                                                 //���-�� �������� ��������� �� �������� �������
        auto it = table_.find(word);                                                //���� ����� � ��� �������

        if(it != table_.end())                                                      //���� ��� ���� � �������
        {
            auto frm = it.value();                                                  //������� ����� ���������� �����
            setFormat(index, count, frm);                                           //������ ��������������
        }

        index += count + 1;                                                         //������� ������
    }
}

//
//  ����� ���������� �����
//

void TextHighlighter::updateOperators()                                             //�����, ����������� ��������� ����������
{
    QList<QString> list;                                                            //������ ����������
    QTextCharFormat frm;                                                            //��� �������

    auto syntax = semantic::SyntaxAccessor::get();                                  //������� ��������� ����������

    list.append(syntax->mainWords());                                               //��������� � ������ �������� ���������
    list.append(syntax->undeletables());                                            //��������� � ������ ����������� ���������

    frm.setForeground(Qt::darkBlue);                                                //�����-����� �����
    frm.setFontWeight(QFont::Bold);                                                 //���������� �����

    for (auto& word : list)                                                         //���������� ��� ���������
        table_.insert(word, frm);                                                   //��������� � �������
}
void TextHighlighter::updateOperands()                                              //�����, ����������� ��������� ���������
{
    QList<QString> list = semantic::SyntaxAccessor::get()->operands();              //������ ���������
    QTextCharFormat frm;                                                            //��� �������

    frm.setForeground(Qt::darkYellow);                                                  //������ �����
    frm.setFontWeight(QFont::Bold);                                                 //���������� �����

    for (auto& word : list)                                                         //���������� ��� ���������
        table_.insert(word, frm);                                                   //��������� � �������
}
