#include "error_wnd.h"
#include <QtWidgets/QMessageBox>

using namespace gui::messages;

ErrorWnd::ErrorWnd(QString message)                                                 //����������� ������
{
    QMessageBox msg(                                                                //��������� ����
        QMessageBox::Icon::Critical,                                                //������ ����
        "Error",                                                                    //��������� ����
        message,                                                                    //���������
        QMessageBox::StandardButton::Ok                                             //������ ��
    );
    msg.exec();                                                                     //��������� ����
}
