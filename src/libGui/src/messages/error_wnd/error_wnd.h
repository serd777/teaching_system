#pragma once

//
//  ����� ���������� ���� � �������
//

#include <QtCore/QString>
#include <export.h>

namespace gui
{
    namespace messages
    {
        class GUI_EXPORT ErrorWnd                                                   //����� ���������� ���� � �������
        {
        public:                                                                     //�������� ������

            explicit ErrorWnd(                                                      //����������� ������
                QString message                                                     //��������� � �������
            );

            ~ErrorWnd() {};                                                         //���������� ������

        private:                                                                    //�������� ������
        };
    }
}