#pragma once

//
//  ����� ���������� ���� � �����������
//

#include <QtCore/QString>
#include <export.h>

namespace gui
{
    namespace messages
    {
        class GUI_EXPORT InfoWnd                                                    //����� ���������� ���� � �����������
        {
        public:                                                                     //�������� ������

            explicit InfoWnd(                                                       //����������� ������
                QString message                                                     //��������� � �������
            );

            ~InfoWnd() {}                                                           //���������� ������

        private:                                                                    //�������� ������
        };
    }
}