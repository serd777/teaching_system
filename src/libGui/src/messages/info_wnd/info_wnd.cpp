#include "info_wnd.h"
#include <QtWidgets/QMessageBox>

using namespace gui::messages;

InfoWnd::InfoWnd(QString message)                                                   //����������� ������
{
    QMessageBox msg(                                                                //��������� ����
        QMessageBox::Icon::Information,                                             //������ ����
        "Information",                                                              //��������� ����
        message,                                                                    //���������
        QMessageBox::StandardButton::Ok                                             //������ ��
    );
    msg.exec();                                                                     //��������� ����
}
