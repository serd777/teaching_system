#pragma once

//
//  ����� ��������� ������� ��������� ����
//

#include <QtWidgets/QMainWindow>

#include <styles/styles.h>
#include <localizations/loc.h>

namespace gui
{
    namespace widgets
    {
        //
        //  �������� ������
        //

        class GMainWindow : public QMainWindow                                      //����� ��������� ������� ��������� ����
        {
        public:                                                                     //�������� ������

            explicit GMainWindow(                                                   //����������� ������
                QWidget* parent = nullptr                                           //�������� �������
            );                                             

            ~GMainWindow() {}                                                       //���������� ������

            virtual void init();                                                    //�����, ���������������� ������

        protected:                                                                  //����������� ������

            virtual void updateStyle();                                             //�����, ����������� �����
            virtual void updateLocalization();                                      //�����, ����������� �����������
        };

        //
        //  �������� ������������
        //
        inline GMainWindow::GMainWindow(QWidget* parent)                            //�������� ������������
                           :QMainWindow(parent)                                     //������������� ������������� ������
        {
            auto style_accessor = resources::Styles::accessor();                    //�������� ������ � ��������� ���������
            auto loc_accessor = resources::Loc::accessor();                         //�������� ������ � ���������������� ���������
            
            connect(style_accessor, &resources::StyleAccessor::styleUpdated,        //���������� � ������� ���������� �����
                    this,           [this]()                                        //������-�������
            {
                updateStyle();                                                      //�������� ����������� ����� ���������� �����
            });
            connect(loc_accessor, &resources::LocaleAccessor::localeUpdated,        //���������� � ������� ���������� �����������
                    this,         [this]()                                          //������-�������
            {
                updateLocalization();                                               //�������� ����������� ����� ���������� �����������
            });
        }

        //
        //  �������� �������
        //
        inline void GMainWindow::init()                                             //�����, ���������������� ������
        {
            updateStyle();                                                          //��������� �����
            updateLocalization();                                                   //��������� �����������
        }

        inline void GMainWindow::updateStyle()                                      //�����, ����������� �����
        {
            setStyleSheet((*resources::Styles::accessor())[objectName()]);          //������ ����� ������� ��� ������ ��� �����
        }
        inline void GMainWindow::updateLocalization()                               //�����, ����������� �����������
        {
            setWindowTitle((*resources::Loc::accessor())[objectName()]);            //������ ��������� ����
        }
    }
}