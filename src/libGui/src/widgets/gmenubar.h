#pragma once

//
//  ����� ��������� ��������� ����
//

#include <QtWidgets/QMenuBar>

#include <styles/styles.h>
#include <localizations/loc.h>

namespace gui
{
    namespace widgets
    {
        //
        //  �������� ������
        //

        class GMenuBar : public QMenuBar                                            //����� ��������� ��������� ����
        {
        public:                                                                     //�������� ������

            explicit GMenuBar(                                                      //����������� ������
                QWidget* parent = nullptr                                           //�������� �������
            );                                             

            ~GMenuBar() {}                                                          //���������� ������

            virtual void init();                                                    //�����, ���������������� ������

        protected:                                                                  //����������� ������

            virtual void updateStyle();                                             //�����, ����������� �����
            virtual void updateLocalization();                                      //�����, ����������� �����������
        };

        //
        //  �������� ������������
        //
        inline GMenuBar::GMenuBar(QWidget* parent)                                  //�������� ������������
                        :QMenuBar(parent)                                           //������������� ������������� ������
        {
            auto style_accessor = resources::Styles::accessor();                    //�������� ������ � ��������� ���������
            auto loc_accessor = resources::Loc::accessor();                         //�������� ������ � ���������������� ���������
            
            connect(style_accessor, &resources::StyleAccessor::styleUpdated,        //���������� � ������� ���������� �����
                    this,           [this]()                                        //������-�������
            {
                updateStyle();                                                      //�������� ����������� ����� ���������� �����
            });
            connect(loc_accessor, &resources::LocaleAccessor::localeUpdated,        //���������� � ������� ���������� �����������
                    this,         [this]()                                          //������-�������
            {
                updateLocalization();                                               //�������� ����������� ����� ���������� �����������
            });
        }

        //
        //  �������� �������
        //
        inline void GMenuBar::init()                                                //�����, ���������������� ������
        {
            updateStyle();                                                          //��������� �����
            updateLocalization();                                                   //��������� �����������
        }

        inline void GMenuBar::updateStyle()                                         //�����, ����������� �����
        {
            setStyleSheet((*resources::Styles::accessor())[objectName()]);          //������ ����� ������� ��� ������ ��� �����
        }
        inline void GMenuBar::updateLocalization()                                  //�����, ����������� �����������
        { }
    }
}