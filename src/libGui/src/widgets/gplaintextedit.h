#pragma once

//
//  ����� ��������� �������������� ��������� ������
//

#include <QtWidgets/QPlainTextEdit>

#include <styles/styles.h>
#include <localizations/loc.h>

namespace gui
{
    namespace widgets
    {
        //
        //  �������� ������
        //

        class GPlainTextEdit : public QPlainTextEdit                                //����� ��������� �������������� ��������� ������
        {
        public:                                                                     //�������� ������

            explicit GPlainTextEdit(                                                //����������� ������
                QWidget* parent = nullptr                                           //�������� �������
            );                                             

            ~GPlainTextEdit() {}                                                    //���������� ������

            virtual void init();                                                    //�����, ���������������� ������

        protected:                                                                  //����������� ������

            virtual void updateStyle();                                             //�����, ����������� �����
            virtual void updateLocalization();                                      //�����, ����������� �����������
        };

        //
        //  �������� ������������
        //
        inline GPlainTextEdit::GPlainTextEdit(QWidget* parent)                      //�������� ������������
                       :QPlainTextEdit(parent)                                      //������������� ������������� ������
        {
            auto style_accessor = resources::Styles::accessor();                    //�������� ������ � ��������� ���������
            auto loc_accessor = resources::Loc::accessor();                         //�������� ������ � ���������������� ���������
            
            connect(style_accessor, &resources::StyleAccessor::styleUpdated,        //���������� � ������� ���������� �����
                    this,           [this]()                                        //������-�������
            {
                updateStyle();                                                      //�������� ����������� ����� ���������� �����
            });
            connect(loc_accessor, &resources::LocaleAccessor::localeUpdated,        //���������� � ������� ���������� �����������
                    this,         [this]()                                          //������-�������
            {
                updateLocalization();                                               //�������� ����������� ����� ���������� �����������
            });
        }

        //
        //  �������� �������
        //
        inline void GPlainTextEdit::init()                                          //�����, ���������������� ������
        {
            updateStyle();                                                          //��������� �����
            updateLocalization();                                                   //��������� �����������
        }

        inline void GPlainTextEdit::updateStyle()                                   //�����, ����������� �����
        {
            setStyleSheet((*resources::Styles::accessor())[objectName()]);          //������ ����� ������� ��� ������ ��� �����
        }
        inline void GPlainTextEdit::updateLocalization()                            //�����, ����������� �����������
        { }
    }
}