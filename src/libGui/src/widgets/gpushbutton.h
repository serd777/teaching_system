#pragma once

//
//  ����� ��������� ������
//

#include <QtWidgets/QPushButton>

#include <styles/styles.h>
#include <localizations/loc.h>

namespace gui
{
    namespace widgets
    {
        //
        //  �������� ������
        //

        class GPushButton : public QPushButton                                      //����� ��������� ������
        {
        public:                                                                     //�������� ������

            explicit GPushButton(                                                   //����������� ������
                QWidget* parent = nullptr                                           //�������� �������
            );                                             

            ~GPushButton() {}                                                       //���������� ������

            virtual void init();                                                    //�����, ���������������� ������

        protected:                                                                  //����������� ������

            virtual void updateStyle();                                             //�����, ����������� �����
            virtual void updateLocalization();                                      //�����, ����������� �����������

            void enterEvent(QEvent* event) override;                                //�����, ���������� ��� ��������� �������
            void leaveEvent(QEvent* event) override;                                //�����, ���������� ��� ����� ������� � �������
        };

        //
        //  �������� ������������
        //
        inline GPushButton::GPushButton(QWidget* parent)                            //�������� ������������
                           :QPushButton(parent)                                     //������������� ������������� ������
        {
            auto style_accessor = resources::Styles::accessor();                    //�������� ������ � ��������� ���������
            auto loc_accessor = resources::Loc::accessor();                         //�������� ������ � ���������������� ���������
            
            connect(style_accessor, &resources::StyleAccessor::styleUpdated,        //���������� � ������� ���������� �����
                    this,           [this]()                                        //������-�������
            {
                updateStyle();                                                      //�������� ����������� ����� ���������� �����
            });
            connect(loc_accessor, &resources::LocaleAccessor::localeUpdated,        //���������� � ������� ���������� �����������
                    this,         [this]()                                          //������-�������
            {
                updateLocalization();                                               //�������� ����������� ����� ���������� �����������
            });
        }

        //
        //  �������� �������
        //
        inline void GPushButton::init()                                             //�����, ���������������� ������
        {
            updateStyle();                                                          //��������� �����
            updateLocalization();                                                   //��������� �����������
        }

        inline void GPushButton::updateStyle()                                      //�����, ����������� �����
        {
            setStyleSheet((*resources::Styles::accessor())[objectName()]);          //������ ����� ������� ��� ������ ��� �����
        }
        inline void GPushButton::updateLocalization()                               //�����, ����������� �����������
        {
            setText((*resources::Loc::accessor())[objectName()]);                   //������ ����� �� �����������
        }

        inline void GPushButton::enterEvent(QEvent* event)                          //�����, ���������� ��� ��������� �������
        {
            setCursor(Qt::PointingHandCursor);                                      //������ ������
            QPushButton::enterEvent(event);                                         //�������� ������������ �����
        }
        inline void GPushButton::leaveEvent(QEvent* event)                          //�����, ���������� ��� ����� ������� � �������
        {
            setCursor(Qt::ArrowCursor);                                             //������ ������
            QPushButton::leaveEvent(event);                                         //�������� ������������ �����
        }
    }
}