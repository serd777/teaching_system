#pragma once

//
//  ����� ��������� �������
//

#include <QtWidgets/QLabel>

#include <styles/styles.h>
#include <localizations/loc.h>

namespace gui
{
    namespace widgets
    {
        //
        //  �������� ������
        //

        class GLabel : public QLabel                                                //����� ��������� �������
        {
        public:                                                                     //�������� ������

            explicit GLabel(                                                        //����������� ������
                QWidget* parent = nullptr                                           //�������� �������
            );                                             

            ~GLabel() {}                                                            //���������� ������

            virtual void init();                                                    //�����, ���������������� ������

        protected:                                                                  //����������� ������

            virtual void updateStyle();                                             //�����, ����������� �����
            virtual void updateLocalization();                                      //�����, ����������� �����������
        };

        //
        //  �������� ������������
        //
        inline GLabel::GLabel(QWidget* parent)                                      //�������� ������������
                      :QLabel(parent)                                               //������������� ������������� ������
        {
            auto style_accessor = resources::Styles::accessor();                    //�������� ������ � ��������� ���������
            auto loc_accessor = resources::Loc::accessor();                         //�������� ������ � ���������������� ���������
            
            connect(style_accessor, &resources::StyleAccessor::styleUpdated,        //���������� � ������� ���������� �����
                    this,           [this]()                                        //������-�������
            {
                updateStyle();                                                      //�������� ����������� ����� ���������� �����
            });
            connect(loc_accessor, &resources::LocaleAccessor::localeUpdated,        //���������� � ������� ���������� �����������
                    this,         [this]()                                          //������-�������
            {
                updateLocalization();                                               //�������� ����������� ����� ���������� �����������
            });
        }

        //
        //  �������� �������
        //
        inline void GLabel::init()                                                  //�����, ���������������� ������
        {
            updateStyle();                                                          //��������� �����
            updateLocalization();                                                   //��������� �����������
        }

        inline void GLabel::updateStyle()                                           //�����, ����������� �����
        {
            setStyleSheet((*resources::Styles::accessor())[objectName()]);          //������ ����� ������� ��� ������ ��� �����
        }
        inline void GLabel::updateLocalization()                                    //�����, ����������� �����������
        {
            setText((*resources::Loc::accessor())[objectName()]);                   //������ ����� �� �����������
        }
    }
}