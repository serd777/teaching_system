#pragma once

//
//  ����� ��������� �������
//

#include <QtWidgets/QWidget>
#include <QtWidgets/QStyle>
#include <QtWidgets/QStyleOption>

#include <QtGui/QPainter>

#include <styles/styles.h>
#include <localizations/loc.h>

namespace gui
{
    namespace widgets
    {
        //
        //  �������� ������
        //

        class GWidget : public QWidget                                              //����� ��������� �������
        {
        public:                                                                     //�������� ������

            explicit GWidget(                                                       //����������� ������
                QWidget* parent = nullptr                                           //�������� �������
            );                                             

            ~GWidget() {}                                                           //���������� ������

            virtual void init();                                                    //�����, ���������������� ������

        protected:                                                                  //����������� ������

            void paintEvent(QPaintEvent* event) override;                           //����� ��������� �������

            virtual void updateStyle();                                             //�����, ����������� �����
            virtual void updateLocalization();                                      //�����, ����������� �����������
        };

        //
        //  �������� ������������
        //
        inline GWidget::GWidget(QWidget* parent)                                    //�������� ������������
                       :QWidget(parent)                                             //������������� ������������� ������
        {
            auto style_accessor = resources::Styles::accessor();                    //�������� ������ � ��������� ���������
            auto loc_accessor = resources::Loc::accessor();                         //�������� ������ � ���������������� ���������
            
            connect(style_accessor, &resources::StyleAccessor::styleUpdated,        //���������� � ������� ���������� �����
                    this,           [this]()                                        //������-�������
            {
                updateStyle();                                                      //�������� ����������� ����� ���������� �����
            });
            connect(loc_accessor, &resources::LocaleAccessor::localeUpdated,        //���������� � ������� ���������� �����������
                    this,         [this]()                                          //������-�������
            {
                updateLocalization();                                               //�������� ����������� ����� ���������� �����������
            });
        }

        //
        //  �������� �������
        //
        inline void GWidget::init()                                                 //�����, ���������������� ������
        {
            updateStyle();                                                          //��������� �����
            updateLocalization();                                                   //��������� �����������
        }

        inline void GWidget::paintEvent(QPaintEvent* event)                         //����� ��������� �������
        {
            QStyleOption option;                                                    //������� ����� �����
            QPainter painter(this);                                                 //������� ����������

            option.initFrom(this);                                                  //�������������� �����
            style()->drawPrimitive(QStyle::PE_Widget, &option, &painter, this);     //������������ ������
        }

        inline void GWidget::updateStyle()                                          //�����, ����������� �����
        {
            setStyleSheet((*resources::Styles::accessor())[objectName()]);          //������ ����� ������� ��� ������ ��� �����
        }
        inline void GWidget::updateLocalization()                                   //�����, ����������� �����������
        { }
    }
}