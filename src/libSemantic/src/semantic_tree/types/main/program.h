#pragma once

//
//  �������� "program"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Program : public NodeType                                         //�������� "program"
            {
            public:                                                                 //�������� ������

                Program();                                                          //����������� ������
                ~Program() {}                                                       //���������� ������
            };

            //
            //  �������� ������
            //

            inline Program::Program()                                               //����������� ������
                           :NodeType(TypeEnum::PROGRAM)                             //�������������� ������������ �����
            {
                main() = true;                                                      //������ �������� ��������
            }
        }
    }
}