#pragma once

//
//  �������� "var"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Var : public NodeType                                             //�������� "var"
            {
            public:                                                                 //�������� ������

                Var();                                                              //����������� ������
                ~Var() {}                                                           //���������� ������
            };

            //
            //  �������� ������
            //

            inline Var::Var()                                                       //����������� ������
                       :NodeType(TypeEnum::VAR)                                     //�������������� ������������ �����
            {
                main() = true;                                                      //������ �������� ��������
            }
        }
    }
}