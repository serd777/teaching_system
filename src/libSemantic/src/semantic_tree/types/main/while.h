#pragma once

//
//  �������� "while"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class While : public NodeType                                           //�������� "while"
            {
            public:                                                                 //�������� ������

                While();                                                            //����������� ������
                ~While() {}                                                         //���������� ������
            };

            //
            //  �������� ������
            //

            inline While::While()                                                   //����������� ������
                         :NodeType(TypeEnum::WHILE)                                 //�������������� ������������ �����
            {
                main() = true;                                                      //������ �������� ��������
            }
        }
    }
}