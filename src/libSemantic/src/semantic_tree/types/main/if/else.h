#pragma once

//
//  �������� "else"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Else : public NodeType                                            //�������� "else"
            {
            public:                                                                 //�������� ������

                Else();                                                             //����������� ������
                ~Else() {}                                                          //���������� ������
            };

            //
            //  �������� ������
            //

            inline Else::Else()                                                     //����������� ������
                        :NodeType(TypeEnum::ELSE)                                   //�������������� ������������ �����
            {
                deletable() = true;                                                 //������ �������� �����������
                main()      = true;                                                 //������ �������� ��������
            }
        }
    }
}