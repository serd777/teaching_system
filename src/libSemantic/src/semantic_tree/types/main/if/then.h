#pragma once

//
//  �������� "then"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Then : public NodeType                                            //�������� "then"
            {
            public:                                                                 //�������� ������

                Then();                                                             //����������� ������
                ~Then() {}                                                          //���������� ������
            };

            //
            //  �������� ������
            //

            inline Then::Then()                                                     //����������� ������
                        :NodeType(TypeEnum::THEN)                                   //�������������� ������������ �����
            {
                deletable() = true;                                                 //������ �������� �����������
            }
        }
    }
}