#pragma once

//
//  �������� "if"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class If : public NodeType                                              //�������� "if"
            {
            public:                                                                 //�������� ������

                If();                                                               //����������� ������
                ~If() {}                                                            //���������� ������
            };

            //
            //  �������� ������
            //

            inline If::If()                                                         //����������� ������
                      :NodeType(TypeEnum::IF)                                       //�������������� ������������ �����
            {
                main() = true;                                                      //������ �������� ��������
            }
        }
    }
}