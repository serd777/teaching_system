#pragma once

//
//  �����-�������� ���� ���� ������
//

namespace semantic
{
    namespace tree
    {
        enum class TypeEnum                                                         //Enum ����� �����
        {
            EMPTY,                                                                  //������ ���
            EDITABLE,                                                               //������������� ��������

            PROGRAM,                                                                //Program
            VAR,                                                                    //Var
            IF,                                                                     //If
            THEN,                                                                   //Then
            ELSE,                                                                   //Else

            WHILE,                                                                  //While
            DO,                                                                     //Do

            BEGIN_MAIN,                                                             //Begin Main
            END_MAIN,                                                               //End Main
            BEGIN,                                                                  //Begin
            END,                                                                    //End

            END_SEPARATOR,                                                          //����� � �������
            DOUBLE_DOTS,                                                            //��������� (�����������)
            BRACKET_OPEN,                                                           //�������� ������
            BRACKET_CLOSED,                                                         //�������� ������
        };

        class NodeType                                                              //�����-�������� ���� ���� ������
        {
        public:                                                                     //�������� ������

            explicit NodeType(TypeEnum type) :                                      //����������� ������
                type_(type),                                                        //��� ����
                deletable_(true),                                                   //���� ��������
                main_(false)                                                        //���� ��������� ���������
            {}

            ~NodeType() {}                                                          //���������� ������

            const auto& isDeletable() { return deletable_; }                        //����� �������� �� �����������
            const auto& isMain()      { return main_;      }                        //����� �������� �� �������� �����

        protected:                                                                  //����������� ������

            auto& type()       { return type_; }                                    //����� ������� � ���� ����
            auto& deletable()  { return deletable_; }                               //����� ������� � ����� ��������
            auto& main()       { return main_; }                                    //����� ������� � ��������� �����

        private:                                                                    //�������� ������

            TypeEnum type_;                                                         //��� ����

            bool deletable_;                                                        //���� ����������� ��������
            bool main_;                                                             //���� ����, ��� �������� - ��������
        };
    }
}