#pragma once

//
//  �������� "begin"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Begin : public NodeType                                           //�������� "begin"
            {
            public:                                                                 //�������� ������

                Begin();                                                            //����������� ������
                ~Begin() {}                                                         //���������� ������
            };

            //
            //  �������� ������
            //

            inline Begin::Begin()                                                   //����������� ������
                         :NodeType(TypeEnum::BEGIN)                                 //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}