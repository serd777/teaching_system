#pragma once

//
//  ������������� ������
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class BracketClosed : public NodeType                                   //������������� ������
            {
            public:                                                                 //�������� ������

                BracketClosed();                                                    //����������� ������
                ~BracketClosed() {}                                                 //���������� ������
            };

            //
            //  �������� ������
            //

            inline BracketClosed::BracketClosed()                                   //����������� ������
                                 :NodeType(TypeEnum::BRACKET_CLOSED)                //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}