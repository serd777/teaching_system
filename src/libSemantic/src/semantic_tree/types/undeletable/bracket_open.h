#pragma once

//
//  ������������� ������
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class BracketOpen : public NodeType                                     //������������� ������
            {
            public:                                                                 //�������� ������

                BracketOpen();                                                      //����������� ������
                ~BracketOpen() {}                                                   //���������� ������
            };

            //
            //  �������� ������
            //

            inline BracketOpen::BracketOpen()                                       //����������� ������
                               :NodeType(TypeEnum::BRACKET_OPEN)                    //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}