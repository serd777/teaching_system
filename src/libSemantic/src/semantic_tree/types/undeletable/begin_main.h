#pragma once

//
//  �������� "begin" (��������, �.�. ��� Unit � Program)
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class BeginMain : public NodeType                                       //�������� "begin" (��������, �.�. ��� Unit � Program)
            {
            public:                                                                 //�������� ������

                BeginMain();                                                        //����������� ������
                ~BeginMain() {}                                                     //���������� ������
            };

            //
            //  �������� ������
            //

            inline BeginMain::BeginMain()                                           //����������� ������
                             :NodeType(TypeEnum::BEGIN_MAIN)                        //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}