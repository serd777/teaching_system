#pragma once

//
//  �������� "end" (��������, �.�. ��� Unit � Program)
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class EndMain : public NodeType                                         //�������� "end" (��������, �.�. ��� Unit � Program)
            {
            public:                                                                 //�������� ������

                EndMain();                                                          //����������� ������
                ~EndMain() {}                                                       //���������� ������
            };

            //
            //  �������� ������
            //

            inline EndMain::EndMain()                                               //����������� ������
                           :NodeType(TypeEnum::END_MAIN)                            //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}