#pragma once

//
//  �������� "do"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Do : public NodeType                                              //�������� "do"
            {
            public:                                                                 //�������� ������

                Do();                                                               //����������� ������
                ~Do() {}                                                            //���������� ������
            };

            //
            //  �������� ������
            //

            inline Do::Do()                                                         //����������� ������
                      :NodeType(TypeEnum::DO)                                       //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}