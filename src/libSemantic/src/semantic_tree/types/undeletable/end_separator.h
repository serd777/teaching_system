#pragma once

//
//  ����������� ����� ��������� (����� � �������)
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class EndSeparator : public NodeType                                    //����������� ����� ��������� (����� � �������)
            {
            public:                                                                 //�������� ������

                EndSeparator();                                                     //����������� ������
                ~EndSeparator() {}                                                  //���������� ������
            };

            //
            //  �������� ������
            //

            inline EndSeparator::EndSeparator()                                     //����������� ������
                                :NodeType(TypeEnum::END_SEPARATOR)                  //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}