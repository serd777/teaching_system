#pragma once

//
//  �������� "end"
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class End : public NodeType                                             //�������� "end"
            {
            public:                                                                 //�������� ������

                End();                                                              //����������� ������
                ~End() {}                                                           //���������� ������
            };

            //
            //  �������� ������
            //

            inline End::End()                                                       //����������� ������
                       :NodeType(TypeEnum::END)                                     //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}