#pragma once

//
//  ����������� - ���������
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class DoubleDots : public NodeType                                      //����������� - ���������
            {
            public:                                                                 //�������� ������

                DoubleDots();                                                       //����������� ������
                ~DoubleDots() {}                                                    //���������� ������
            };

            //
            //  �������� ������
            //

            inline DoubleDots::DoubleDots()                                         //����������� ������
                              :NodeType(TypeEnum::DOUBLE_DOTS)                      //�������������� ������������ �����
            {
                deletable() = false;                                                //������ �������� �����������
            }
        }
    }
}