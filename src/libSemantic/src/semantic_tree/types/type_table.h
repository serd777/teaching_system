#pragma once

//
//  ������� ����� ��� ������� �� �����
//

#include <container_search.h>

#include <QtCore/QVector>
#include <QtCore/QPair>
#include <QtCore/QString>

#include "./types/main/program.h"
#include "./types/main/var.h"
#include "./types/main/if.h"
#include "./types/main/if/then.h"
#include "./types/main/if/else.h"

#include "./types/main/while.h"
#include "./types/undeletable/do.h"

#include "./types/undeletable/begin_main.h"
#include "./types/undeletable/end_main.h"

#include "./types/undeletable/begin.h"
#include "./types/undeletable/end.h"

#include "./types/undeletable/end_separator.h"
#include "./types/undeletable/double_dots.h"
#include "./types/undeletable/bracket_open.h"
#include "./types/undeletable/bracket_closed.h"

#include "./editable_operator.h"

namespace semantic
{
    namespace tree
    {
        QVector<QPair<QString, NodeType*>> types_table = {                          //������� ����� �������������� ������
            { "[editable]",                    new node_types::Editable  },         //������������� ��������

            { "[main.program]",                new node_types::Program   },         //program
            { "[main.if]",                     new node_types::If        },         //if
            { "[main.var]",                    new node_types::Var       },         //var
            { "[main.while]",                  new node_types::While     },         //while

            { "[undeletable.begin_main]",      new node_types::BeginMain     },     //begin (main)
            { "[undeletable.end_main]",        new node_types::EndMain       },     //end. (main)
            { "[undeletable.begin]",           new node_types::Begin         },     //begin
            { "[undeletable.end]",             new node_types::End           },     //end
            { "[undeletable.separator]",       new node_types::EndSeparator  },     //separator
            { "[undeletable.double_dots]",     new node_types::DoubleDots    },     //double dots
            { "[undeletable.bracket_open]",    new node_types::BracketOpen   },     //bracket_open
            { "[undeletable.bracket_closed]",  new node_types::BracketClosed },     //bracket_closed
            { "[undeletable.do]",              new node_types::Do            },     //do

            { "[undeletable.if.then]",         new node_types::Then },              //then
            { "[undeletable.if.else]",         new node_types::Else },              //else
        };

        inline NodeType* findNodeType(const QString& key)                           //����� ������ ���� � �������
        {
            auto index = templates::ContainerSearch::findByKey(types_table, key);   //������ � ����������

            if (index != templates::ContainerSearch::NOT_FOUND)                     //���� ����� ���������� �������
                return types_table[index].second;                                   //�� ���������� ���������

            return nullptr;                                                         //����� ���������� ������ ���������
        }
    }
}
