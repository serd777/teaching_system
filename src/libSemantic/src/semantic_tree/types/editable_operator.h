#pragma once

//
//  ������������� ��������
//

#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        namespace node_types
        {
            class Editable : public NodeType                                        //������������� ��������
            {
            public:                                                                 //�������� ������

                Editable();                                                         //����������� ������
                ~Editable() {}                                                      //���������� ������
            };

            //
            //  �������� ������
            //

            inline Editable::Editable()                                             //����������� ������
                            :NodeType(TypeEnum::EDITABLE)                           //�������������� ������������ �����
            {}
        }
    }
}