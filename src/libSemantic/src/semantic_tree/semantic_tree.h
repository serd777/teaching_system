#pragma once

//
//  ������������� ������ ����
//

#include <QtCore/QObject>
#include <QtCore/QQueue>
#include <export.h>

#include "./node/node.h"

namespace semantic
{
    class SEMANTIC_EXPORT SemanticTree : public QObject                             //������������� ������ ����
    {
        Q_OBJECT                                                                    //MOC ����

    public:                                                                         //�������� ������

        explicit SemanticTree(                                                      //����������� ������
            QObject* parent = nullptr                                               //Qt ��������
        );

        ~SemanticTree();                                                            //���������� ������

        void        addNode(const QString& key, int text_pos);                      //����� ���������� ����
        void        deleteNode(int text_pos);                                       //����� �������� ����
        tree::Node* findNode(int pos);                                              //����� ������ ���� �� �������� ��������� �������

        void increasePos(int min_pos, int count);                                   //�����, ������������� ������� �� ��������� ������� � �����
        void decreasePos(int min_pos, int count);                                   //�����, ����������� ������� �� ��������� ������� � �����

        void clear();                                                               //����� ������� ������

        bool isDelQueueEmpty() const { return del_queue_.isEmpty(); }               //�����, ����������� �� ������� ������� ��������
        int  popDelQueue()           { return del_queue_.dequeue(); }               //�����, ������������ �������� � ������ �������

    signals:                                                                        //�������

        void foundTemplate();                                                       //������ ������� �������������� �������

    private:                                                                        //�������� ������

        tree::Node* root_;                                                          //������ ������
        QQueue<int> del_queue_;                                                     //������� ������� �� ��������

        tree::Node* findNode(tree::Node* node, int pos);                            //����� ������ ���� �� �������� ��������� �������
        tree::Node* findPrevious(tree::Node* node, int pos);                        //����� ������ ���� ��������������� ��������� ��������� �������

        void rebalancePos(tree::Node* node, int min_p, int count);                  //�����, ����������� ������� ������� � ����� ������
        void rebalanceDelQueue(int min_pos, int count);                             //�����, ����������� ������� ������� � ������� �� ��������

        void connectSlots(tree::Node* node);                                        //����� ������������ ����� � ��������� ����
        void freeNode(tree::Node* node);                                            //����� ������ ����������� ������� ����
    };
}