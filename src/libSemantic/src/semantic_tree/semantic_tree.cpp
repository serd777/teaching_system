#include "semantic_tree.h"
#include "./types/type_table.h"

using namespace semantic;
using namespace tree;

//
//  ������������ � ����������
//

SemanticTree::SemanticTree(QObject* parent)                                         //����������� ������
             :QObject(parent)                                                       //�������������� ������������ �����
{
    root_ = nullptr;                                                                //������ ������ ������
}
SemanticTree::~SemanticTree()                                                       //���������� ������
{
    clear();                                                                        //������� ������
}

//
//  ������ �������� ��� �������
//

void SemanticTree::addNode(const QString& key, int text_pos)                        //����� ���������� ����
{
    auto node = new Node(findNodeType(key));                                        //����� ����
    node->pos() = text_pos;                                                         //������ ��������� ������� ����

    if (!root_) root_ = node;                                                       //���� ������ ����
    else
    {                                                                               //����� ������ ������ ���� � ������� ����� ����
        auto previous = findPrevious(root_, text_pos);                              //���� ���������� ����

        if (previous->type()->isMain())                                             //���� � ����������� �������� ���
            previous->addRight(node);                                               //�� ��������� � ������ �����
        else
            previous->addLeft(node);                                                //����� � �����
    }

    connectSlots(node);                                                             //���������� ������� ����
}
void SemanticTree::deleteNode(int text_pos)                                         //����� �������� ����
{
    auto node = findNode(text_pos);                                                 //������� ����������� ����
    node->deleteThis();                                                             //�������� �������� ��������
}
Node* SemanticTree::findNode(int pos)                                               //����� ������ ���� �� �������� ��������� �������
{
    if (!root_) return nullptr;                                                     //���� ������ ���� - �� ������ ������ ���������
    return findNode(root_, pos);                                                    //�������� ������������ �����
}
Node* SemanticTree::findNode(Node* node, int pos)                                   //����� ������ ���� �� �������� ��������� �������
{
    if (node->pos() == pos)                                                         //���� ����� ���������� ����
        return node;                                                                //���������� ���

    auto left = node->left();                                                       //�������� ������ �������
    auto right = node->right();                                                     //�������� ������� �������

    Node * left_node = nullptr;                                                     //���� �� ����� �����
    Node * right_node = nullptr;                                                    //���� �� ������ �����

    if (left)  left_node = findNode(left, pos);                                     //���� ���� ���� � ����� �����, �� ������� �
    if (right) right_node = findNode(right, pos);                                   //���� ���� ���� � ������ �����, �� ������� �

    if (left_node)  return left_node;                                               //���� ����� � ����� �����, ���������� ����
    if (right_node) return right_node;                                              //���� ����� � ������ �����, ���������� ����

    return nullptr;                                                                 //�� ����� �������� � ������ ����
}

Node* SemanticTree::findPrevious(Node* node, int pos)                               //����� ������ ���� ��������������� ��������� ��������� �������
{
    auto parent = node->parent();                                                   //�������� ��������
    auto left   = node->left();                                                     //�������� ������ �������
    auto right  = node->right();                                                    //�������� ������� �������

    if (node->pos() > pos)                                                          //���� ������� ���� ������ ��������� �������
        if (parent)                                                                 //���� ���� ��������
            return parent;                                                          //���������� ������������ ����

    Node* left_node  = nullptr;                                                     //���� �� ������ �� ����� �����
    Node* right_node = nullptr;                                                     //���� �� ������ �� ������ �����

    if (left)                                                                       //���� ���� ����� �������
        if (left->pos() <= pos)                                                     //��������� ���� �� ����� ���� � ����� �����
            left_node = findPrevious(left, pos);                                    //���� ��, �� ��������� �������� � ����� �����

    if (right)                                                                      //���� ���� ������ �������
        if (right->pos() <= pos)                                                    //��������� ���� �� ����� ���� � ������ �����
            right_node = findPrevious(right, pos);                                  //���� ��, �� ��������� �������� � ������ �����

    if (left_node && right_node)                                                    //���� ������ ��� �����
    {
        if (left_node->pos() > right_node->pos())                                   //�������� ��� ����, � �������� ������ �������
            return left_node;                                                       //�������� ����� ����

        return right_node;                                                          //�������� ������ ����
    }

    if (left_node)  return left_node;                                               //���� ������ ������ ����� ����� � ����� ����
    if (right_node) return right_node;                                              //���� ������ ������ ������ ����� � ����� ����

    return node;                                                                    //���������� ������ ����
}

void SemanticTree::increasePos(int min_pos, int count)                              //�����, ������������� ������� �� ��������� ������� � �����
{
    if(root_)                                                                       //���� � ����� ���-�� ����
    {
        rebalancePos(root_, min_pos, count);                                        //��������� ����������� ����� � ����������� �������
        rebalanceDelQueue(min_pos, count);                                          //��������� ������� � ������� �� ��������
    }
}
void SemanticTree::decreasePos(int min_pos, int count)                              //�����, ����������� ������� �� ��������� ������� � �����
{
    if(root_)                                                                       //���� � ����� ���-�� ����
    {
        rebalancePos(root_, min_pos, -count);                                       //��������� ����������� ����� � ����������� �������
        rebalanceDelQueue(min_pos, -count);                                         //��������� ������� � ������� �� ��������
    }
}
void SemanticTree::rebalancePos(Node* node, int min_p, int count)                   //�����, ����������� ������� ������� � ����� ������
{
    auto old_pos = node->pos();                                                     //�������� ������ �������
    auto left    = node->left();                                                    //�������� ������ �������
    auto right   = node->right();                                                   //�������� ������� �������

    if (old_pos >= min_p)                                                           //���� ������� ���� � ������ ������ ��� ���������
    {
        auto new_pos = old_pos + count;                                             //��������� ����� ������� ���� � ������
        node->pos() = new_pos;                                                      //������ ���� ����� �������
    }

    if (right) rebalancePos(right, min_p, count);                                   //���� ���� ������ �������, �� ��������� � ����
    if (left)  rebalancePos(left, min_p, count);                                    //���� ���� ����� �������, �� ��������� � ����
}
void SemanticTree::rebalanceDelQueue(int min_pos, int count)                        //�����, ����������� ������� ������� � ������� �� ��������
{
    auto elem_count = del_queue_.count();                                           //���-�� ��������� �������

    for (decltype(elem_count) i = 0; i < elem_count; i++)                           //���������� �������� �������
        if (del_queue_[i] >= min_pos)                                               //���� ������� ������ ���������
            del_queue_[i] += count;                                                 //��������� �
}

void SemanticTree::clear()                                                          //����� ������� ������
{
    if (root_) freeNode(root_);                                                     //���� � ����� ���-�� ����, �������
}

//
//  ��������� ������
//

void SemanticTree::connectSlots(Node* node)                                         //����� ������������ ����� � ��������� ����
{
    connect(node, &Node::rootDeleted,                                               //��� ��������� ������� �������� �����
            this, [this](Node* new_root)                                            //���������� ������
    {
        root_ = new_root;                                                           //��������� ������
    });
    connect(node, &Node::nodeDeleted,                                               //��� ��������� ������� �������� ����
            this, [this](int pos)                                                   //���������� ������
    {
        del_queue_.enqueue(pos);                                                    //������ ��������� ������� � �������
    });
}

void SemanticTree::freeNode(Node* node)                                             //����� ������ ����������� ������� ����
{
    auto left = node->left();                                                       //������� ������ �������
    auto right = node->right();                                                     //������� ������� �������

    if (right) freeNode(right);                                                     //��������� �������� � ������� �������
    if (left)  freeNode(left);                                                      //��������� �������� � ������ �������

    node->deleteThis();                                                             //������� ������ ����
}
