#include "node.h"

using namespace semantic::tree;

//
//  ����������� � ����������
//

Node::Node(NodeType* type, QObject* parent)                                         //����������� ������
     :QObject(parent)                                                               //�������������� ������������ �����
{
    type_ = type;                                                                   //���������� ��� ����

    parent_ = reinterpret_cast<Node*>(parent);                                      //���������� ������������ ����
    left_   = nullptr;                                                              //����� ������� - ������
    right_  = nullptr;                                                              //������ ������� - ������

    del_left_ = true;                                                               //���� �������� ���������� �������
    pos_ = 0;                                                                       //������� ����
    layer_ = 0;                                                                     //������� ����
}
Node::~Node()                                                                       //���������� ������
{
    if (left_ && del_left_) delete left_;                                           //������� ���� ���������� ������ �������
    if (right_) delete right_;                                                      //������� ���� ���������� ������� �������

    emit nodeDeleted(pos_);                                                         //�������� ������ �� �������� ������� ����
}

//
//  ������ �������� ��� �����
//

void Node::addLeft(Node* node)                                                      //����� ���������� ������ �������
{
    if(left_)                                                                       //���� ���� ����� �������
    {
        left_->parent() = node;                                                     //������ ������ ������� �������� - ����� ����
        node->left() = left_;                                                       //������ ���� ������ ������ �������
    }
    left_ = node;                                                                   //����� ������� = ����� ����
    left_->parent() = this;                                                         //������ ������ ������� ��������
}
void Node::addRight(Node* node)                                                     //����� ���������� ������� �������
{
    if (right_)                                                                     //���� ���� ������ �������
    {
        right_->parent() = node;                                                    //������ ������� ������� �������� - ����� ����
        node->right() = right_;                                                     //������ ���� ������ ������� �������
    }
    right_ = node;                                                                  //������ ������� = ����� ����
    right_->parent() = this;                                                        //������ ������� ������� ��������
}

void Node::deleteThis()                                                             //����� �������� �������� ����
{
    if(parent_)                                                                     //���� ���� ��������
    {
        if (parent_->right() == this)                                               //���� ������ ���� � ������ �����
            parent_->right() = nullptr;                                             //������ ������� = ������
        else
            parent_->left() = left_;                                                //����� �������� ���� ����� �����

        if(left_)                                                                   //���� ���� ���-�� � ����� �����
        {
            left_->parent() = parent_;                                              //�� ������� ��������
            del_left_ = false;                                                      //������� ���� �������� ���������� ����
        } 
    }
    else
    {                                                                               //���� ��� ������
        if (left_) emit rootDeleted(left_);                                         //���� ���� ��������� ����
        else       emit rootDeleted(nullptr);                                       //���� ��� ���
    }

    delete this;                                                                    //������� ������ ���� �� ������
}
