#pragma once

//
//  ���� �������������� ������
//

#include <QtCore/QObject>
#include <semantic_tree/types/node_type.h>

namespace semantic
{
    namespace tree
    {
        class Node : public QObject                                                 //���� �������������� ������
        {
            Q_OBJECT                                                                //MOC ����

        public:                                                                     //�������� ������

            explicit Node(                                                          //����������� ������
                NodeType* type,
                QObject* parent = nullptr                                           //Qt ��������
            );

            ~Node();                                                                //���������� ������

            void addLeft(Node* node);                                               //����� ���������� ������ �������
            void addRight(Node* node);                                              //����� ���������� ������� �������

            void deleteThis();                                                      //����� �������� �������� ����

            auto& parent() { return parent_; }                                      //������ � ��������
            auto& left()   { return left_;   }                                      //������ � ������ �������
            auto& right()  { return right_;  }                                      //������ � ������� �������

            auto& pos()    { return pos_;    }                                      //������ � ������ ������� ������
            auto& type()   { return type_;   }                                      //������ � ���� ����
            auto& layer()  { return layer_;  }                                      //������ � ������ ���� ������

        signals:                                                                    //�������

            void nodeDeleted(int pos);                                              //������ �������� ����
            void rootDeleted(Node* child);                                          //������ ����, ��� �������� ������

        private:                                                                    //�������� ������

            NodeType* type_;                                                        //��� ����

            Node* parent_;                                                          //��������
            Node* left_;                                                            //����� �������
            Node* right_;                                                           //������ �������

            bool del_left_;                                                         //���� �������� ���������� �������
            int  pos_;                                                              //������� ���� � ������
            int  layer_;                                                            //������� ���� � ������
        };
    }
}