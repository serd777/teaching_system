#include "syntax.h"

#include <directory/directory.h>
#include <file_types/syntax_file/syntax_file.h>
#include <container_search.h>

using namespace semantic;
using namespace templates;

//
//  ������������ ������
//

Syntax::Syntax(const QString& name)                                                 //����������� ������
{
    name_ = name;                                                                   //���������� ���

    //
    //  ������� ������ �� ������
    //
    filesystem::filetypes::SyntaxFile file_main(                                    //���� � ��������� �����������
        "syntax/" + name + "/main.syntax"
    );
    filesystem::filetypes::SyntaxFile file_operands(                                //���� � ����������
        "syntax/" + name + "/operands.syntax"
    );
    filesystem::filetypes::SyntaxFile file_undel(                                   //���� � ������������ �����������
        "syntax/" + name + "/undeletable.syntax"
    );

    main_ = file_main.pairs();                                                      //�������� ���� ��������
    operands_ = file_operands.pairs();                                              //�������� ���� ���������
    undeletable_ = file_undel.pairs();                                              //�������� ���� �����������

    loadTemplates();                                                                //��������� ������������� �������
}

//
//  ������ �������������
//

void Syntax::loadTemplates()                                                        //�����, ����������� ������������� �������
{
    filesystem::Directory dir("syntax/" + name_ + "/templates");                    //�������� ������ � ���������� � ���������

    if(dir.isExists())                                                              //���� ���������� ����������
    {
        auto files = dir.getFilesPaths();                                           //�������� ������ ����� ������

        for (auto& path : files)                                                    //���������� ����
            templates_.push_back(SemanticTemplate(path));                           //������ � ��������� ������������� ������
    }
}

//
//  ������ ������� � ������
//

Syntax::ListStr Syntax::mainWords()                                                 //����� ������� � ������ �������� ����
{
    auto count = main_.count();                                                     //���-�� ��������� � ����������
    ListStr result;                                                                 //�������������� ������

    for (decltype(count) i = 0; i < count; i++)                                     //���������� ��������� ���
        result.push_back(main_[i].second);                                          //�������� ������� � ������

    return result;                                                                  //���������� ���������
}
Syntax::ListStr Syntax::operands()                                                  //����� ������� � ������ ���������
{
    auto count = operands_.count();                                                 //���-�� ��������� � ����������
    ListStr result;                                                                 //�������������� ������

    for (decltype(count) i = 0; i < count; i++)                                     //���������� ��������� ���
        result.push_back(operands_[i].second);                                      //�������� ������� � ������

    return result;                                                                  //���������� ���������
}
Syntax::ListStr Syntax::undeletables()                                              //����� ������� � ������ ����������� ����������
{
    auto count = undeletable_.count();                                              //���-�� ��������� � ����������
    ListStr result;                                                                 //�������������� ������

    for (decltype(count) i = 0; i < count; i++)                                     //���������� ��������� ���
        result.push_back(undeletable_[i].second);                                   //�������� ������� � ������

    return result;                                                                  //���������� ���������
}

const QString& Syntax::getKey(const QString& word)                                  //�����, ������������ ���� ���������
{
    auto index = ContainerSearch::findByValue(main_, word);                         //���� ����� �������� ����������
    if (index != ContainerSearch::NOT_FOUND)                                        //����� ������
        return main_[index].first;                                                  //���������� ����

    index = ContainerSearch::findByValue(operands_, word);                          //���� ����� ���������
    if (index != ContainerSearch::NOT_FOUND)                                        //����� ������
        return operands_[index].first;                                              //���������� ����

    index = ContainerSearch::findByValue(undeletable_, word);                       //���� ����� ����������� ����������
    if (index != ContainerSearch::NOT_FOUND)                                        //����� ������
        return undeletable_[index].first;                                           //���������� ����

    return QString();                                                               //�� ����� �������� - ������ ����
}
const QString& Syntax::getWord(const QString& key)                                  //�����, ������������ ��������� ������������� ���������
{
    auto index = ContainerSearch::findByKey(main_, key);                            //���� ����� �������� ����������
    if (index != ContainerSearch::NOT_FOUND)                                        //����� ������
        return main_[index].second;                                                 //���������� ��������

    index = ContainerSearch::findByKey(operands_, key);                             //���� ����� ���������
    if (index != ContainerSearch::NOT_FOUND)                                        //����� ������
        return operands_[index].second;                                             //���������� ��������

    index = ContainerSearch::findByKey(undeletable_, key);                          //���� ����� ����������� ����������
    if (index != ContainerSearch::NOT_FOUND)                                        //����� ������
        return undeletable_[index].second;                                          //���������� ��������

    return QString();                                                               //�� ����� �������� - ������ ��������
}
const SemanticTemplate& Syntax::getTemplate(const QString& key)                     //�����, ������������ ������������� ������
{
    for (auto& tpl : templates_)                                                    //���������� ������������� �������
        if (tpl.name() == key)                                                      //���� ����� ���������� �� �����
            return tpl;                                                             //���������� ���� ������
    return SemanticTemplate();                                                      //����� �� ������
}

//
//  ������ ��������
//

bool Syntax::isInsertableOperator(const QString& word)                              //�����, ����������� �������� �� �������� �����������
{
    return ContainerSearch::findByValue(main_, word) != ContainerSearch::NOT_FOUND  //���� ���� ����� �������� ����
    || ContainerSearch::findByValue(operands_, word) != ContainerSearch::NOT_FOUND; //���� ���� ����� ���������
}
bool Syntax::isOperator(const QString& word)                                        //�����, ����������� �������� �� ����� ����������
{
    return ContainerSearch::findByValue(main_, word) != ContainerSearch::NOT_FOUND  //���� ���� ����� �������� ����
    || ContainerSearch::findByValue(operands_, word) != ContainerSearch::NOT_FOUND  //���� ���� ����� ���������
    || ContainerSearch::findByValue(undeletable_, word) != ContainerSearch::NOT_FOUND; //���� ���� ����� �����������
}

bool Syntax::hasTemplate(const QString& key)                                        //�����, ����������� ����� �� ������ �������� ������. ������
{
    for (auto& tpl : templates_)                                                    //���������� ������������� �������
        if (tpl.name() == key)                                                      //���� ����� ���������� �� �����
            return true;                                                            //���������� ������
    return false;                                                                   //����� ����
}
