#pragma once

//
//  ��������� ����� ����������������
//

#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtCore/QPair>

#include <export.h>
#include <syntax/semtpl/semtpl.h>

namespace semantic
{
    class SEMANTIC_EXPORT Syntax                                                    //��������� ����� ����������������
    {
        typedef QVector<QPair<QString, QString>> VecPair;                           //������ ��� ����� ���� - ��������
        typedef QList<QString> ListStr;                                             //��� ������ - ������ �����
        typedef QVector<SemanticTemplate> VecTmpl;                                  //��� ������ - ������ ������������� ��������

    public:                                                                         //�������� ������

        explicit Syntax(                                                            //����������� ������
            const QString& name                                                     //��� ����������
        );

        ~Syntax() {}                                                                //���������� ������

        ListStr mainWords();                                                        //����� ������� � ������ �������� ����
        ListStr operands();                                                         //����� ������� � ������ ���������
        ListStr undeletables();                                                     //����� ������� � ������ ����������� ����������

        const QString& getKey(const QString& word);                                 //�����, ������������ ���� ���������
        const QString& getWord(const QString& key);                                 //�����, ������������ ��������� ������������� ���������
        const SemanticTemplate& getTemplate(const QString& key);                    //�����, ������������ ������������� ������

        bool isInsertableOperator(const QString& word);                             //�����, ����������� �������� �� �������� �����������
        bool isOperator(const QString& word);                                       //�����, ����������� �������� �� ����� ����������
        
        bool hasTemplate(const QString& key);                                       //�����, ����������� ����� �� ������ �������� ������. ������

    private:                                                                        //�������� ������

        QString name_;                                                              //�������� ����������

        VecPair main_;                                                              //������ �������� ����������
        VecPair operands_;                                                          //������ ���������
        VecPair undeletable_;                                                       //������ ����������� ����������

        VecTmpl templates_;                                                         //������ ������������� ��������

        void loadTemplates();                                                       //�����, ����������� ������������� �������
    };
}