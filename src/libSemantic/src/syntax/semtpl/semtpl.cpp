#include "semtpl.h"

#include <file_types/semtpl_file/semtpl_file.h>

using namespace semantic;

SemanticTemplate::SemanticTemplate(QString path)                                    //����������� ������
{
    loadTemplate(path);                                                             //��������� ������
}

void SemanticTemplate::loadTemplate(const QString& path)                            //�����, ����������� ������ �� �����
{
    filesystem::filetypes::SemTplFile file(path);                                   //���� �������

    name_ = file.name();                                                            //������� ��� �������
    auto keys = file.keys();                                                        //������� ������������� �����
    auto count = keys.count();                                                      //���-�� ������ � �������

    for(decltype(count) i = 0; i < count; i++)                                      //���������� �����
    {
        QString value  = keys[i];                                                   //������ �������� = �����
        bool    is_key = true;                                                      //��������� ���� �����

        if(isSpace(keys[i]))                                                        //���� ���� - ��� ���� �������
        {
            value = " ";                                                            //�� �������� = ������� �������
            is_key = false;                                                         //���� = ������� �����
        }
        if(isEndLine(keys[i]))
        {
            value = "\n";                                                           //�� �������� = ������� �������� ������
            is_key = false;                                                         //���� = ������� �����
        }

        nodes_.push_back(TplNode_t(value, is_key));                                 //������ � ���������
    }
}

//
//  ������ ������� ��������
//

bool SemanticTemplate::isSpace(const QString& key) const                            //�����, ����������� �������� �� ���� ��������
{
    return key == "[space]";                                                        //���������� ��������� ���������
}

bool SemanticTemplate::isEndLine(const QString& key) const                          //�����, ����������� �������� �� ���� ������ �����
{
    return key == "[end_line]";                                                     //���������� ��������� ���������
}
