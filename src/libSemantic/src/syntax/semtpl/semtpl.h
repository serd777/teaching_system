#pragma once

//
//  ����� �������������� �������
//

#include <QtCore/QVector>
#include <QtCore/QString>

namespace semantic
{
    class SemanticTemplate                                                          //����� �������������� �������
    {
        struct TplNode_t                                                            //��������� �������������� ����
        {
            QString value_;                                                         //�������� (���� ����, ���� �����)
            bool    is_key_;                                                        //���� - ���� ������, �� �������� = ����

            TplNode_t(QString val, bool key) : value_(val), is_key_(key) {}         //����������� �������������
            TplNode_t() : value_(), is_key_() {}                                    //������ �����������
            
            TplNode_t(const TplNode_t& node) :                                      //����������� �����������
            value_(node.value_),                                                    //�������� ��������
            is_key_(node.is_key_) {}                                                //�������� ����
        };

        typedef QVector<TplNode_t> VecNodes;                                        //��� ������ - ������ ������������� �����

    public:                                                                         //�������� ������

        explicit SemanticTemplate(                                                  //����������� ������
            QString path                                                            //���� �� �����
        );
        SemanticTemplate(                                                           //����������� �����������
            const SemanticTemplate& old                                             //�� �������� ��������
        ) : name_(old.name_), nodes_(old.nodes_) {}                                 //�������������� ����

        SemanticTemplate()  {}                                                      //������ �����������
        ~SemanticTemplate() {}                                                      //���������� ������

        const auto& name() { return name_; }                                        //�����, ������������ ��� �������

              auto  countNodes()               { return nodes_.count(); }           //�����, ������������ ���-�� ����� � �������
        const auto& getValue(const int& index) { return nodes_[index].value_; }     //�����, ������������ �������� ����

        bool isKey(const int& index)           { return nodes_[index].is_key_; }    //�����, ������������ ���� ����� ���������� ����

    private:                                                                        //�������� ������

        QString name_;                                                              //��� �������
        VecNodes nodes_;                                                            //������ ������������� ����� �������

        void loadTemplate(const QString& path);                                     //�����, ����������� ������ �� �����

        bool isSpace(const QString& key)   const;                                   //�����, ����������� �������� �� ���� ��������
        bool isEndLine(const QString& key) const;                                   //�����, ����������� �������� �� ���� ������ �����
    };
}