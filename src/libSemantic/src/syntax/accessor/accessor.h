#pragma once

//
//  ����������� ����� ������� � ����������
//

#include "../syntax.h"

namespace semantic
{
    class SEMANTIC_EXPORT SyntaxAccessor                                            //����������� ����� ������� � ����������
    {
    public:                                                                         //�������� ������

        static void init(Syntax* syntax);                                           //����� �������������
        static const auto& get() { return syntax_; }                                //����� ������� � ����������

    private: static Syntax* syntax_;                                                //���������
    };
}