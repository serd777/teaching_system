#pragma once

//
//  ���������� ���������� ��������
//

#if defined(_WIN32) || defined(_WIN64)
    //������� ��� libDebug
    #ifdef _LIB_DEBUG_EXPORT
        #define DEBUG_EXPORT __declspec(dllexport)
    #else
        #define DEBUG_EXPORT __declspec(dllimport)
    #endif

    //������� ��� libFileSystem
    #ifdef _LIB_FS_EXPORT
        #define FS_EXPORT __declspec(dllexport)
    #else
        #define FS_EXPORT __declspec(dllimport)
    #endif

    //������� ��� libResources
    #ifdef _LIB_RESOURCES_EXPORT
        #define RESOURCES_EXPORT __declspec(dllexport)
    #else
        #define RESOURCES_EXPORT __declspec(dllimport)
    #endif

    //������� ��� libGui
    #ifdef _LIB_GUI_EXPORT
        #define GUI_EXPORT __declspec(dllexport)
    #else
        #define GUI_EXPORT __declspec(dllimport)
    #endif

    //������� ��� libSemantic
    #ifdef _LIB_SEMANTIC_EXPORT
        #define SEMANTIC_EXPORT __declspec(dllexport)
    #else
        #define SEMANTIC_EXPORT __declspec(dllimport)
    #endif

    //������� ��� libData
    #ifdef _LIB_DATA_EXPORT
        #define DATA_EXPORT __declspec(dllexport)
    #else
        #define DATA_EXPORT __declspec(dllimport)
    #endif
#endif

#if defined(__linux__)
    #define DEBUG_EXPORT __attribute__ ((visibility("default")))
    #define FS_EXPORT __attribute__ ((visibility("default")))
    #define RESOURCES_EXPORT __attribute__ ((visibility("default")))
    #define GUI_EXPORT __attribute__ ((visibility("default")))
    #define SEMANTIC_EXPORT __attribute__ ((visibility("default")))
    #define DATA_EXPORT __attribute__ ((visibility("default")))
#endif