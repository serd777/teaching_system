#pragma once

//
//  ������ ��������� ����������
//

#include <export.h>
#include <QtCore/QObject>

#include "./exceptions/exception/exception.h"
#include "./exceptions/types_list.h"

namespace debug
{
    class DEBUG_EXPORT ExceptionHandler : public QObject                            //������ ��������� ����������
    {
    public:                                                                         //�������� ������

        static ExceptionHandler& Instance()                                         //����� ������� � ������
        {
            static ExceptionHandler handler;                                        //������� ����� ���� ��� �� ��� ������
            return handler;                                                         //���������� ���
        }

        void criticalException(const QString& msg);                                 //�����, ������������� ����������� ����������
        void accessException(const QString& msg);                                   //�����, ������������� ���������� �������
        void projectException(const QString& msg);                                  //�����, ������������� ��������� ����������

    private:                                                                        //�������� ������

        ExceptionHandler() : QObject() {}                                           //����������� ������
        ~ExceptionHandler() {}                                                      //���������� ������

        ExceptionHandler(ExceptionHandler const&) = delete;                         //��������� ����������� �����������
        ExceptionHandler& operator= (ExceptionHandler const&) = delete;             //� �������� ������������

        void throwException(exceptions::ExceptionTypes type, const QString& msg);   //����� ��������� ����������
    };
}