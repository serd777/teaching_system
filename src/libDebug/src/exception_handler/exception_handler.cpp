#include "exception_handler.h"

#include "./exceptions/critical_exception/critical_exception.h"
#include "./exceptions/access_exception/access_exception.h"
#include "./exceptions/project_exception/project_exception.h"

using namespace debug;
using namespace exceptions;

//
//  ������ �������
//

void ExceptionHandler::criticalException(const QString& msg)                        //�����, ������������� ����������� ����������
{
    throwException(ExceptionTypes::CRITICAL, msg);                                  //�������� ����� � ������ �����������
}
void ExceptionHandler::accessException(const QString& msg)                          //�����, ������������� ���������� �������
{
    throwException(ExceptionTypes::ACCESS, msg);                                    //�������� ����� � ������ �����������
}
void ExceptionHandler::projectException(const QString& msg)                         //�����, ������������� ��������� ����������
{
    throwException(ExceptionTypes::PROJECT, msg);                                   //�������� ����� � ������ �����������
}

//
//  ����� ������� ����������
//

void ExceptionHandler::throwException(ExceptionTypes type, const QString& msg)      //����� ��������� ����������
{
    try
    {
        switch(type)                                                                //��������� ���������� ���
        {
            case ExceptionTypes::EXCEPTION: throw Exception(msg);                   //������� ����������

            case ExceptionTypes::CRITICAL:  throw CriticalException(msg);           //����������� ����������
            case ExceptionTypes::ACCESS:    throw AccessException(msg);             //���������� �������
            case ExceptionTypes::PROJECT:   throw ProjectException(msg);            //��������� ����������
        }
    }
    catch (Exception e) { e.show(); }                                               //�������� ����������� ����������
}
