#include "project_exception.h"

using namespace debug::exceptions;

ProjectException::ProjectException(QString message)                                 //����������� ������
                 :Exception()                                                       //�������������� ������������ �����
{
    message_ = message;                                                             //���������� ���������
    loadMsg();                                                                      //��������� ��������� ����������
}
QString ProjectException::generateMsg()                                             //�����, ������������ ���������
{
    return QString("[PROJECT EXCEPTION] ") + message_;                              //��������� ������ � ���������� ����������
}
