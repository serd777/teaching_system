#pragma once

//
//  ���������� � ��������� ������
//

#include <exceptions/exception/exception.h>

namespace debug
{
    namespace exceptions
    {
        class DEBUG_EXPORT ProjectException : public Exception                      //����� ���������� ����������
        {
        public:                                                                     //�������� ������

            explicit ProjectException(                                              //����������� ������
                QString message                                                     //��������� ����������
            );

            ~ProjectException() {}                                                  //���������� ������

        protected: QString generateMsg() override;                                  //�����, ������������ ���������
        private:   QString message_;                                                //��������� ����������
        };
    }
}