#pragma once

//
//  ����� ������������ ����������
//

#include "../exception/exception.h"

namespace debug
{
    namespace exceptions
    {
        class DEBUG_EXPORT CriticalException : public Exception                     //����� ������������ ����������
        {
        public:                                                                     //�������� ������

            explicit CriticalException(                                             //����������� ������
                QString message                                                     //��������� ����������
            );

            ~CriticalException() {}                                                 //���������� ������

        protected: QString generateMsg() override;                                  //�����, ������������ ���������
        private:   QString message_;                                                //��������� ����������
        };
    }
}