#include "critical_exception.h"

using namespace debug::exceptions;

CriticalException::CriticalException(QString message)                               //����������� ������
                  :Exception()                                                      //�������������� ������������ �����
{
    message_ = message;                                                             //���������� ���������
    loadMsg();                                                                      //��������� ��������� ����������
}
QString CriticalException::generateMsg()                                            //�����, ������������ ���������
{
    return QString("[CRITICAL EXCEPTION] ") + message_;                             //��������� ������ � ���������� ����������
}
