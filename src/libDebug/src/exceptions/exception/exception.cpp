#include "exception.h"
#include <QtWidgets/QErrorMessage>

using namespace debug::exceptions;

//
//  ������������ ������
//

Exception::Exception(QString message)                                               //����������� � ���������� ����������
          :QException()                                                             //�������������� ������������ �����
{
    message_ = message;                                                             //��������� ���������
}
Exception::Exception() : QException() {}                                            //������ �����������

//
//  �������� ������
//

void Exception::show() const                                                        //�����, ������������ ��������� ����������
{
    QErrorMessage msg;                                                              //���� � ��������� ����������

    msg.showMessage(message_);                                                      //������ ���� ���������
    msg.exec();                                                                     //������� ���
}

void       Exception::raise() const { throw *this; }                                //�����, ����������� ���� ����������
Exception* Exception::clone() const { return new Exception(*this); }                //�����, ����������� ����������

//
//  ����������� ������
//

QString Exception::generateMsg()                                                    //�����, ������������ ���������
{
    return QString("[EXCEPTION] Unknown exception!");                               //����������� ��������� ��� ����������
}
void Exception::loadMsg() { message_ = generateMsg(); }                             //�����, ����������� ��������������� ���������
