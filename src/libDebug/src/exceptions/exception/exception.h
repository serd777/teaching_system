#pragma once

//
//  �����-�������� ���������� ����������
//

#include <QtCore/QException>
#include <export.h>

namespace debug
{
    namespace exceptions
    {
        class DEBUG_EXPORT Exception : public QException                            //�����-�������� ���������� ����������
        {
        public:                                                                     //�������� ������

            explicit Exception(QString message);                                    //����������� ���������� � ����������

            Exception();                                                            //����������� ������
            ~Exception() {}                                                         //���������� ������

            void       show() const;                                                //�����, ������������ ��������� ����������

            void       raise() const override;                                      //�����, ����������� ���� ����������
            Exception* clone() const override;                                      //�����, ����������� ����������

        protected:                                                                  //����������� ������

            virtual QString generateMsg();                                          //�����, ������������ ���������
            void            loadMsg();                                              //�����, ����������� ��������������� ���������

        private:                                                                    //�������� ������

            QString message_;                                                       //��������� ����������
        };
    }
}