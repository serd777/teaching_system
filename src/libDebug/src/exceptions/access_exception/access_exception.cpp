#include "access_exception.h"

using namespace debug::exceptions;

AccessException::AccessException(QString path)                                      //����������� ������
                :Exception()                                                        //�������������� ������������ �����
{
    path_ = path;                                                                   //���������� ����
    loadMsg();                                                                      //��������� ���������
}
QString AccessException::generateMsg()                                              //�����, ������������ ��������� ����������
{
    return QString("[EXCEPTION] Failed to get access to: ") + path_;                //��������� ��������� � ����
}
