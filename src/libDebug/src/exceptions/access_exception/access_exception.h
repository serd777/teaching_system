#pragma once

//
//  ����� ���������� ������� � �������
//

#include "../exception/exception.h"

namespace debug
{
    namespace exceptions
    {
        class DEBUG_EXPORT AccessException : public Exception                       //����� ���������� ������� � �������
        {
        public:                                                                     //�������� ������

            explicit AccessException(                                               //����������� ������
                QString path                                                        //���� �� �������
            );

            ~AccessException() {}                                                   //���������� ������

        protected: QString generateMsg() override;                                  //�����, ������������ ���������
        private:   QString path_;                                                   //���� �� ������� ����������
        };
    }
}