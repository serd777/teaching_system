#pragma once

//
//  ������ ������ ��������� ����������������� ������ � ����������
//

namespace templates
{
    class ContainerSearch                                                           //��������� ����� ����������������� ������ � �����������
    {
    public:                                                                         //�������� ������

        static const int NOT_FOUND = -1;                                            //��������� ���������� ������

        template<
            typename ContainerType,                                                 //��� ����������
            typename ItemType                                                       //��� ��������, ������� �� ����
        >
            static auto find(                                                       //����� ������ �������� � ����������
                const ContainerType& container,                                     //���������
                ItemType item                                                       //������� ������� ����
            );

        template<
            template<typename> typename ContainerType,                              //��� ����������
            template<typename, typename> typename PairType,                         //��� ������� ������
            typename KeyType,                                                       //��� ����� � ����
            typename ItemType                                                       //��� �������� � ����
        >
            static auto findByKey(                                                  //����� ������ � ������ ���������� �� �����
                const ContainerType<PairType<KeyType, ItemType>>& container,        //���������
                KeyType key                                                         //����
            );

        template<
            template<typename> typename ContainerType,                              //��� ����������
            template<typename, typename> typename PairType,                         //��� ������� ������
            typename KeyType,                                                       //��� ����� � ����
            typename ItemType                                                       //��� �������� � ����
        >
            static auto findByValue(                                                //����� ������ � ������ ���������� �� ��������
                const ContainerType<PairType<KeyType, ItemType>>& container,        //���������
                ItemType item                                                       //��������
            );
    };

    //
    //	�������� ��������� �������
    //

    template<
        typename ContainerType,                                                     //��� ����������
        typename ItemType                                                           //��� ��������, ������� �� ����
    >
        auto ContainerSearch::find(                                                 //����� ������ �������� � ����������
            const ContainerType& container,                                         //���������
            ItemType item                                                           //������� ������� ����
        )
    {
        auto count = container.count();                                             //�������� ���-�� ��������� � ����������

        for (decltype(count) i = 0; i < count; ++i)                                 //���������� ��� ��������
            if (container[i] == item)                                               //���� ����� ������������
                return i;                                                           //���������� ������

        return NOT_FOUND;                                                           //����� ���������� �������� �� ���������� ��������
    }

    template<
        template<typename> typename ContainerType,                                  //��� ����������
        template<typename, typename> typename PairType,                             //��� ������� ������
        typename KeyType,                                                           //��� ����� � ����
        typename ItemType                                                           //��� �������� � ����
    >
        auto ContainerSearch::findByKey(                                            //����� ������ � ������ ���������� �� �����
            const ContainerType<PairType<KeyType, ItemType>>& container,            //���������
            KeyType key                                                             //����
        )
    {
        auto count = container.count();                                             //�������� ���-�� ��������� � ����������

        for (decltype(count) i = 0; i < count; ++i)                                 //���������� ��� ��������
            if (container[i].first == key)                                          //���� ����� ������������
                return i;                                                           //���������� ������

        return NOT_FOUND;                                                           //����� ���������� �������� �� ���������� ��������
    }

    template<
        template<typename> typename ContainerType,                                  //��� ����������
        template<typename, typename> typename PairType,                             //��� ������� ������
        typename KeyType,                                                           //��� ����� � ����
        typename ItemType                                                           //��� �������� � ����
    >
        auto ContainerSearch::findByValue(                                          //����� ������ � ������ ���������� �� ��������
            const ContainerType<PairType<KeyType, ItemType>>& container,            //���������
            ItemType item                                                           //��������
        )
    {
        auto count = container.count();                                             //�������� ���-�� ��������� � ����������

        for (decltype(count) i = 0; i < count; ++i)                                 //���������� ��� ��������
            if (container[i].second == item)                                        //���� ����� ������������
                return i;                                                           //���������� ������

        return NOT_FOUND;                                                           //����� ���������� �������� �� ���������� ��������
    }
}