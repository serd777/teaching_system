#pragma once

//
//  ����� ������� � ����� � ���� ������
//

#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtCore/QScopedPointer>
#include <QtCore/QFile>

#include <export.h>

namespace filesystem
{
    namespace files
    {
        class FS_EXPORT TextFile                                                    //����� ������� � ����� � ���� ������
        {
            typedef QScopedPointer<QFile> FilePtr;                                  //��� ������ - ��������� �� �������� �����������
            typedef QVector<QString>      VecSt;                                    //��� ������ - ������ �����

        public:                                                                     //�������� ������

            explicit TextFile(                                                      //����������� ������
                QString path,                                                       //���� �� �����
                bool empty_lines = false                                            //���� ������ �����
            );

            virtual ~TextFile();                                                    //���������� ������

            void clearFile();                                                       //�����, ��������� ����

            void addString(QString string);                                         //�����, ����������� ������ � ����
            void deleteString(int index);                                           //�����, ��������� ������ �� �������
            void replaceString(QString string, int index);                          //�����, ���������� ������ �� �������

            QString getString(int index)       const;                               //�����, ������������ ������ �� �������
            QString getAllContent()            const;                               //�����, ������������ ��� ������ � �����
            VecSt   getStrings()               const;                               //�����, ������������ ������ �����
            int     findString(QString string) const;                               //�����, ������������ ������ ������

        private:                                                                    //�������� ������

            const char* encoding_ = "UTF-8";                                        //��������� ���� ������

            FilePtr file_;                                                          //�������� �����������
            VecSt   lines_;                                                         //����� �����
            bool    skip_empty_lines_;                                              //���� �������� ������ �����

            void init();                                                            //�����, ���������������� ������ �����

            void loadData();                                                        //�����, ����������� ������ �� �����
            void saveData();                                                        //�����, ����������� ������ � ����
        };
    }
}