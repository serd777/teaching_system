#include "text_file.h"

#include <QtCore/QFileInfo>
#include <QtCore/QTextStream>

#include <directory/directory.h>
#include <container_search.h>

using namespace filesystem::files;

//
//  ����������� � ���������� ������
//

TextFile::TextFile(QString path, bool empty_lines)                                  //����������� ������
{
    file_.reset(new QFile(path));                                                   //�������������� �����������
    skip_empty_lines_ = !empty_lines;                                               //���������� ����

    init();                                                                         //�������������� ����
}
TextFile::~TextFile()                                                               //���������� ������
{
    if (file_->isOpen())                                                            //���� ����������� ����� ������
        file_->close();                                                             //�� ������� ���
}

//
//  �������� �������������� � ������
//

void TextFile::clearFile()                                                          //�����, ��������� ����
{
    file_->open(QIODevice::WriteOnly);                                              //��������� ���� ��� ����, ����� ������� ���
    file_->close();                                                                 //��������� �����

    lines_.clear();                                                                 //������� ���������
}

void TextFile::addString(QString string)                                            //�����, ����������� ������ � ����
{
    lines_.push_back(string);                                                       //��������� ������ � ���������
    saveData();                                                                     //��������� ������
}
void TextFile::deleteString(int index)                                              //�����, ��������� ������ �� �������
{
    lines_.removeAt(index);                                                         //������� �� �������
    saveData();                                                                     //���������� ������ � �����
}
void TextFile::replaceString(QString string, int index)                             //�����, ���������� ������ �� �������
{
    lines_[index] = string;                                                         //�������� ������
    saveData();                                                                     //���������� ������ � �����
}

//
//  �������� ������� � ������ �����
//

QString TextFile::getString(int index) const { return lines_[index]; }              //�����, ������������ ������ �� �������
QString TextFile::getAllContent()      const                                        //�����, ������������ ��� ������ � �����
{
    QString result;                                                                 //�������������� ������

    for (auto& line : lines_)                                                       //���������� ��� ������
        result += line;                                                             //��������� � ������

    return result;                                                                  //���������� ���������
}
TextFile::VecSt TextFile::getStrings() const { return lines_; }                     //�����, ������������ ������ �����
int             TextFile::findString(QString string) const                          //�����, ������������ ������ ������
{
    return templates::ContainerSearch::find(lines_, string);                        //�������� ���������������� �����
}

//
//  ����� ������������� �����
//

void TextFile::init()                                                               //�����, ���������������� ������ �����
{
    QFileInfo f_info(*file_);                                                       //������� ������ � ����������� �����

    if(!file_->exists())                                                            //���� ����� �� ����������
    {
        Directory dir(f_info.dir().path());                                         //�������� ������ � ���������� �����

        if (!dir.isExists())                                                        //���� ���������� �� ����������
            dir.create();                                                           //������� ����������

        clearFile();                                                                //������� ������ ����
    }

    loadData();                                                                     //��������� ������ �� �����
}

//
//  ������ �������� � ���������� ������
//

void TextFile::loadData()                                                           //�����, ����������� ������ �� �����
{
    file_->open(QIODevice::ReadOnly);                                               //��������� ����� ����� ��� ������

    if(file_->isOpen())                                                             //���� ����� ������ �������
    {
        QTextStream stream(file_.data());                                           //������� ��������� �����
        stream.setCodec(encoding_);                                                 //������ ���������

        while (!stream.atEnd())                                                     //���� ���� ��� ���������
        {
            auto string = stream.readLine();                                        //��������� �����

            if (string.isEmpty() && skip_empty_lines_)                              //���� ������ ������ � ��� ����� ���������� ������ ������
                continue;                                                           //�� ��������� � ��������� ������
            
            lines_.push_back(string);                                               //����� ������ ������ � ���������
        }
    }

    file_->close();                                                                 //��������� �����
}
void TextFile::saveData()                                                           //�����, ����������� ������ � ����
{
    file_->open(QIODevice::WriteOnly);                                              //��������� ���� �� ������

    if(file_->isOpen())                                                             //���� ���� ������ �������
    {
        QTextStream stream(file_.data());                                           //������� ��������� �����
        stream.setCodec(encoding_);                                                 //������ ���������

        auto count = lines_.count();                                                //���-�� ����� �����

        for (decltype(count) i = 0; i < count; i++)                                 //���������� ������ ����������
            stream << lines_[i] << endl;                                            //���������� � ����
    }

    file_->close();                                                                 //��������� �������� �����
}
