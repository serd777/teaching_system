#include "xml_file.h"

#include <QtCore/QFile>

using namespace filesystem::files;

XMLFile::XMLFile(QString path)                                                      //����������� ������
{
    path_ = path;                                                                   //���������� ����
}

void XMLFile::loadData()                                                            //�����, ����������� ������ �� �����
{
    auto file = new QFile(path_);                                                   //������ � �����

    if(file->open(QIODevice::ReadOnly | QIODevice::Text))                           //��������� ����� ������
    {
        QXmlStreamReader reader(file);                                              //XML ������
        parseInput(reader);                                                         //�������� ������ �� �����
    }

    delete file;                                                                    //����������� ������
}
