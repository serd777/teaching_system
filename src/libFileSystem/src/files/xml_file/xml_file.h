#pragma once

//
//  ���� � XML ����������
//

#include <export.h>

#include <QtCore/QXmlStreamReader>
#include <QtCore/QString>

namespace filesystem
{
    namespace files
    {
        class FS_EXPORT XMLFile                                                     //���� � XML ����������
        {
        public:                                                                     //�������� ������

            explicit XMLFile(                                                       //����������� ������
                QString path                                                        //���� �� �����
            );

            XMLFile() {}                                                            //������ �����������
            virtual ~XMLFile() {}                                                   //���������� ������

            void loadData();                                                        //�����, ����������� ������ �� �����

        protected:                                                                  //����������� ������

            QString path_;                                                          //���� �� �����

            virtual void parseInput(QXmlStreamReader& reader) = 0;                  //�����, ����������� ������ �� XML �����
        };
    }
}