#pragma once

//
//  �����, �������������� ������ ���������� ����� � ���� ������ ��������
//

#include <files/text_file/text_file.h>

namespace filesystem
{
    namespace files
    {
        class FS_EXPORT PairFile : TextFile                                         //�����, �������������� ������ ���������� ����� � ���� ������ ��������
        {
            typedef QPair<QString, QString> PairType;                               //��� ������� ����������
            typedef QVector<PairType> VecPair;                                      //��� ������ - ������ ������ �����������

        public:                                                                     //�������� ������

            explicit PairFile(                                                      //����������� ������
                QString path                                                        //���� �� �����
            );

            ~PairFile() {};                                                         //���������� ������

            auto& pairs()     { return pairs_;         }                            //����� ������� � ����������
            auto pairsCount() { return pairs_.count(); }                            //�����, ������������ ���-�� ��� � ����������

            QString  getItem(QString key);                                          //�����, ������������ �������� �� �����
            PairType getPair(int index);                                            //�����, ������������ ���� �� �������

        private:                                                                    //�������� ������

            VecPair pairs_;                                                         //������ ������ ��������

            void initPairs();                                                       //�����, ���������������� ���� �� ����� �����
        };
    }
}