#include "pair_file.h"
#include <container_search.h>

using namespace filesystem::files;

PairFile::PairFile(QString path)                                                    //����������� ������
         :TextFile(path)                                                            //������������� ������������� ������
{
    initPairs();                                                                    //�������������� ������ �����
}

//
//  ������ ������
//

QString PairFile::getItem(QString key)                                              //�����, ������������ �������� �� �����
{
    auto item = templates::ContainerSearch::findByKey(pairs_, key);                 //���� �� ����� � ���������� ���

    if (item != templates::ContainerSearch::NOT_FOUND)                              //���� ����� �������
        return pairs_[item].second;                                                 //���������� ��������

    return QString();                                                               //����� ���������� ������ ������
}
PairFile::PairType PairFile::getPair(int index)                                     //�����, ������������ ���� �� �������
{
    return pairs_[index];                                                           //���������� ����
}

void PairFile::initPairs()                                                          //�����, ���������������� ���� �� ����� �����
{
    auto strings = getStrings();                                                    //�������� ������ �����
    auto count = strings.count();                                                   //�������� ���-�� �����

    for (decltype(count) i = 0; i < count; i += 2)                                  //���������� ������
    {
        auto key = getString(i);                                                    //������ ������ - ����
        auto value = getString(i + 1);                                              //������ ������ - ��������

        pairs_.push_back(PairType(key, value));                                     //������ � ���������
    }
}
