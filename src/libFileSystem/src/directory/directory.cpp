#include "directory.h"
#include <QtCore/QDirIterator>

using namespace filesystem;

//
//  ������������ ������
//

Directory::Directory(QString path)                                                  //����������� ������
{
    path_ = path;                                                                   //���������� ����
    dir_ = QDir(path);                                                              //�������������� ����������� �����������
}
Directory::Directory(const Directory& old)                                          //����������� �����������
{
    path_ = old.path_;                                                              //�������� ����
    dir_  = old.dir_;                                                               //�������� �����������
}

//
//  ������ ������
//

void Directory::clear()                                                             //�����, ��������� ���������� �� ������
{
    auto paths = getFilesPaths();                                                   //�������� ���� � ������

    for (auto& path : paths)                                                        //���������� ����
        QFile::remove(path);                                                        //������� ����
}

Directory::VecSt Directory::getFilesNames() const                                   //�����, ������������ ����� ������ � ����������
{
    VecSt result;                                                                   //�������������� ������
    auto files = dir_.entryInfoList();                                              //�������� ���������� � ������

    for (const auto& file : files)                                                  //���������� ����� ����������
        if (file.isFile())                                                          //���� ��� ����
            result.push_back(file.baseName());                                      //��������� ��� �����

    return result;                                                                  //���������� ���������
}
Directory::VecSt Directory::getFilesPaths() const                                   //�����, ������������ ���� ������ � ����������
{
    VecSt result;                                                                   //�������������� ������
    auto files = dir_.entryInfoList();                                              //�������� ���������� � ������

    for (const auto& file : files)                                                  //���������� ����� ����������
        if (file.isFile())                                                          //���� ��� ����
            result.push_back(file.absoluteFilePath());                              //��������� ���� �����

    return result;                                                                  //���������� ���������
}

Directory::VecSt Directory::getSubDirsNames() const                                 //�����, ������������ ����� ���-����������
{
    VecSt result;                                                                   //�������������� ������
    QDirIterator it(path_);                                                         //�������� ���-����������

    while (it.hasNext())                                                            //���� ���� ���-����������
    {
        it.next();                                                                  //��������� ��������

        auto dir_name = it.fileName();                                              //�������� ��� ����������

        if (dir_name != "." && dir_name != "..")                                    //���� ��� �� ������������� ������
            result.push_back(dir_name);                                             //�� ��������� ��� ����������
    }

    return result;                                                                  //���������� ���������
}
Directory::VecSt Directory::getSubDirsPaths() const                                 //�����, ������������ ���� ���-����������
{
    VecSt result;                                                                   //�������������� ������
    QDirIterator it(path_);                                                         //�������� ���-����������

    while (it.hasNext())                                                            //���� ���� ���-����������
    {
        auto dir_path = it.next();                                                  //�������� ���� ���-����������

        if (!dir_path.contains("/.") && !dir_path.contains("/.."))                  //���� ��� �� ������������� ������
            result.push_back(dir_path);                                             //�� ��������� ���� ����������
    }

    return result;                                                                  //���������� ���������
}
