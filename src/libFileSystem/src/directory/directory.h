#pragma once

//
//  ����� ������� � ���������� �������� �������
//

#include <QtCore/QString>
#include <QtCore/QDir>
#include <QtCore/QVector>

#include <export.h>

namespace filesystem
{
    class FS_EXPORT Directory                                                       //����� ������� � ���������� �������� �������
    {
        typedef QVector<QString> VecSt;                                             //��� ������ - ������ �����

    public:                                                                         //�������� ������

        explicit Directory(                                                         //����������� ������
            QString path                                                            //���� �� ����������
        );
        Directory(const Directory& old);                                            //����������� �����������

        Directory()  {}                                                             //������ �����������
        ~Directory() {}                                                             //���������� ������

        const auto& path() const { return path_; }                                  //�����, ������������ ���� ����������
        auto        name() const { return dir_.dirName(); }                         //�����, ������������ ��� ����������

        bool isExists() const { return dir_.exists(); }                             //�����, ����������� �� �������������
        bool create()   const { return dir_.mkpath("."); }                          //�����, ��������� ����������

        void clear();                                                               //�����, ��������� ���������� �� ������

        VecSt getFilesNames() const;                                                //�����, ������������ ����� ������ � ����������
        VecSt getFilesPaths() const;                                                //�����, ������������ ���� ������ � ����������

        VecSt getSubDirsNames() const;                                              //�����, ������������ ����� ���-����������
        VecSt getSubDirsPaths() const;                                              //�����, ������������ ���� ���-����������

    private:                                                                        //�������� ������

        QDir    dir_;                                                               //�����������
        QString path_;                                                              //���� �� ����������
    };
}