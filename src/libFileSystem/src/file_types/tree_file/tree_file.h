#pragma once

//
//  ����� ����� � ������������� �������
//

#include "../../files/text_file/text_file.h"

namespace filesystem
{
    namespace filetypes
    {
        class FS_EXPORT TreeFile : files::TextFile                                  //����� ����� � ������������� �������
        {
        public:                                                                     //�������� ������

            explicit TreeFile(                                                      //����������� ������
                QString path                                                        //���� �� �����
            );

            ~TreeFile() {}                                                          //���������� ������

        };
    }
}