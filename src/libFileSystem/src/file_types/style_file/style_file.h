#pragma once

//
//  ����� ��������� �����
//

//
//  ������ �����: 1�� ������ - ���� �������,
//  ����������� ������ - ����� ����������
//

#include <files/text_file/text_file.h>

namespace filesystem
{
    namespace filetypes
    {
        class FS_EXPORT StyleFile : files::TextFile                                 //����� ��������� �����
        {
        public:                                                                     //�������� ������

            explicit StyleFile(                                                     //����������� ������
                QString path                                                        //���� �� �����
            );

            ~StyleFile() {};                                                        //���������� ������

            const auto& key()     { return key_;   }                                //�����, ������������ ���� �����
            const auto& content() { return style_; }                                //�����, ������������ ���������� �����

        private:                                                                    //�������� ������

            QString key_;                                                           //���� �����
            QString style_;                                                         //���������� CSS �����

            void loadStyle();                                                       //�����, ����������� ����� �� ������ �����
        };
    }
}