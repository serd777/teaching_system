#include "style_file.h"

using namespace filesystem::filetypes;

StyleFile::StyleFile(QString path)                                                  //����������� ������
          :TextFile(path)                                                           //������������� ������������� ������
{
    loadStyle();                                                                    //��������� �����
}

//
//  ������ ������
//

void StyleFile::loadStyle()                                                         //�����, ����������� ����� �� ������ �����
{
    key_ = getString(0);                                                            //�� ������ ������ �������� ���� �������

    auto strings = getStrings();                                                    //�������� ������� �����
    auto count = strings.count();                                                   //���-�� ����� � �����

    for (decltype(count) i = 1; i < count; i++)                                     //���������� ������
        style_ += strings[i];                                                       //�������� ������ � ������� ������
}
