#pragma once

//
//  ����� ������� ����� �������������� �������
//

#include <files/text_file/text_file.h>

namespace filesystem
{
    namespace filetypes
    {
        class FS_EXPORT SemTplFile : public files::TextFile                         //����� ������� ����� �������������� �������
        {
            typedef QVector<QString> VecString;                                     //��� ������ - ������ �����

        public:                                                                     //�������� ������

            explicit SemTplFile(                                                    //����������� ������
                QString path                                                        //���� �� �����
            );

            ~SemTplFile() {}                                                        //���������� ������

            const auto& name() { return name_; }                                    //������ � ����� �������
            const auto& keys() { return keys_; }                                    //������ � ������ �������

        private:                                                                    //�������� ������

            QString   name_;                                                        //��� �������
            VecString keys_;                                                        //������ ������

            void parseTemplate();                                                   //�����, ����������� ������ �� ������

            
        };
    }
}