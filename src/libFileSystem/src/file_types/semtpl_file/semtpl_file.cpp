#include "semtpl_file.h"

using namespace filesystem::filetypes;

SemTplFile::SemTplFile(QString path)                                                //����������� ������
           :TextFile(path)                                                          //�������������� ������������ �����
{
    parseTemplate();                                                                //�������� ������
}

void SemTplFile::parseTemplate()                                                    //�����, ����������� ������ �� ������
{
    auto lines = getStrings();                                                      //�������� ������ �����
    auto count = lines.count();                                                     //���-�� ����� � �������

    name_ = lines[0];                                                               //��� ����� � ������ ������
    
    for (int i = 1; i < count; i++)                                                 //���������� ������ � �������
    {
        QString buf;                                                                //��������� ������
        QString line = lines[i];                                                    //������� ������� ������

        for(auto& ch : line)                                                        //���������� ������� ������
        {
            buf += ch;                                                              //������ ������� ������ �� ��������� ����������
            if (ch == ']')                                                          //���� ���������� �� ������ ��������� �����
            {
                keys_.push_back(buf);                                               //������ ���� � ���������
                buf.clear();                                                        //������� ��������� ����������
            }
        }
    }
}
