#pragma once

//
//  ����� ����� � ����� ����� Pascal
//

#include "../../files/text_file/text_file.h"

namespace filesystem
{
    namespace filetypes
    {
        class FS_EXPORT PasFile : files::TextFile                                   //����� ����� � ����� ����� Pascal
        {
        public:                                                                     //�������� ������

            explicit PasFile(                                                       //����������� ������
                QString path                                                        //���� �� �����
            );

            ~PasFile() {}                                                           //���������� ������

            QString getCode() const;                                                //�����, ������������ ���������� � ���� ������
            void    writeCode(const QString& code);                                 //�����, ������������ ��� � ����
        };
    }
}