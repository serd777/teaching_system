#include "pas_file.h"

using namespace filesystem::filetypes;

PasFile::PasFile(QString path)                                                      //����������� ������
        :TextFile(path, true)                                                       //�������������� ������������ �����
{

}

//
//  ������ �������������� � ������ ����
//

QString PasFile::getCode() const                                                    //�����, ������������ ���������� � ���� ������
{
    QString result;                                                                 //�������������� ������
    auto lines = getStrings();                                                      //������ �����

    for (auto& line : lines)                                                        //���������� ��� ������ �����
        result += line;                                                             //���������� ������ � ��������������

    return result;                                                                  //���������� ���������
}
void PasFile::writeCode(const QString& code)                                        //�����, ������������ ��� � ����
{
    clearFile();                                                                    //������� ����

    QString buf;                                                                    //��������� ������
    for(auto& ch : code)                                                            //���������� ������� ������ � �����
    {
        if(ch == '\n')                                                              //���� ������ �������� ������
        {
            addString(buf);                                                         //�� ���������� ������ � ����
            buf.clear();                                                            //������� ��������� ������
        }
        else buf += ch;                                                             //����� ���������� �� ��������� ������ ������
    }

    if(!buf.isEmpty())                                                              //���� ��� ���-�� ��������
        addString(buf);                                                             //�� ���������� ������ � ����
}
