#pragma once

//
//  ���� ������� �������� � ���� XML
//

#include "../../files/xml_file/xml_file.h"

#include <QtCore/QHash>

namespace filesystem
{
    namespace filetypes
    {
        class FS_EXPORT SettingsFile : public files::XMLFile                        //���� ������� �������� � ���� XML
        {
            typedef QHash<QString, QString> HashType;                               //��� ������ - ��� �������: ���� - ��������

        public:                                                                     //�������� ������

            explicit SettingsFile(                                                  //����������� ������
                QString path                                                        //���� �� �����
            );
            SettingsFile(                                                           //����������� �����������
                const SettingsFile& copy                                            //�����
            );

            SettingsFile()  {}                                                      //������ �����������
            ~SettingsFile() {}                                                      //���������� ������

            QString& name() { return name_; }                                       //����� ������� � ����� ����� ��������
            QString  get(const QString& key);                                       //����� ������� � �������� �� �����

        protected:                                                                  //����������� ������

            void parseInput(QXmlStreamReader& reader) override;                     //�����, ����������� ������ �� XML �����

        private:                                                                    //�������� ������

            QString  name_;                                                         //�������� ����� ��������
            HashType table_;                                                        //��� ������� ��������

            void parseOption(QXmlStreamReader& reader);                             //�����, ����������� ���� option
            void parseName(QXmlStreamReader& reader);                               //�����, ����������� ���� name
        };
    }
}