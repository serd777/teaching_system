#include "settings_file.h"

using namespace filesystem::filetypes;

SettingsFile::SettingsFile(QString path)                                            //����������� ������
             :XMLFile(path)                                                         //�������������� ������������ �����
{}
SettingsFile::SettingsFile(const SettingsFile& copy)                                //����������� �����������
{
    table_ = copy.table_;                                                           //�������� ��� �������
    name_  = copy.name_;                                                            //�������� ���
    path_  = copy.path_;                                                            //�������� ���� �� �����
}

//
//  ������ ��������������
//

QString SettingsFile::get(const QString& key)                                       //����� ������� � �������� �� �����
{
    auto it = table_.find(key);                                                     //���� � ��� �������

    if (it != table_.end())                                                         //���� ����� �������
        return *it;                                                                 //���������� ��������

    return QString();                                                               //����� ���������� ������ ������
}

//
//  ����������� ������
//

void SettingsFile::parseInput(QXmlStreamReader& reader)                             //�����, ����������� ������ �� XML �����
{
    while(!reader.atEnd() && !reader.hasError())                                    //���� �� �� � ����� ����� � �� ���������� �� ������
    {
        auto token = reader.readNext();                                             //��������� �������

        if(token == QXmlStreamReader::StartDocument)                                //���� ��� ������������ ������� XML �����
            continue;                                                               //�� ��������� ���
        if(token == QXmlStreamReader::StartElement)                                 //���� ��� XML ����
        {
            if (reader.name() == "settings")                                        //���� ��� �������� ����
                continue;                                                           //�� ��������� ���
            if (reader.name() == "name")                                            //���� ��� ���� � ������ ����� ��������
                parseName(reader);                                                  //�� �������� ���
            if (reader.name() == "option")                                          //���� ���� = option
                parseOption(reader);                                                //��������� ���
        }
    }
}

void SettingsFile::parseOption(QXmlStreamReader& reader)                            //�����, ����������� ���� option
{
    auto attributes = reader.attributes();                                          //�������� ����

    auto key = attributes.value("key").toString();                                  //����
    auto value = attributes.value("value").toString();                              //��������

    table_.insert(key, value);                                                      //���������� � ���-�������
}
void SettingsFile::parseName(QXmlStreamReader& reader)                              //�����, ����������� ���� name
{
    name_ = reader.text().toString();                                               //������� ��������
}
