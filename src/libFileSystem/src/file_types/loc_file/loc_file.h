#pragma once

//
//  ����� �������������� ������ ���������������� �����
//

#include <files/pair_file/pair_file.h>

namespace filesystem
{
    namespace filetypes
    {
        class LocFile : public files::PairFile                                      //����� �������������� ������ ���������������� �����
        {
        public:                                                                     //�������� ������

            explicit LocFile(                                                       //����������� ������
                QString path                                                        //���� �� �����
            ) : PairFile(path) {}                                                   //�������������� ������������ �����

            ~LocFile() {};                                                          //���������� ������

        private:                                                                    //�������� ������
        };
    }
}