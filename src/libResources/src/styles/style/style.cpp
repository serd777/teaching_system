#include "style.h"

#include <file_types/style_file/style_file.h>
#include <directory/directory.h>

using namespace resources;

//
//  ������������ ������
//

Style::Style(QString name, QString dir_path)                                        //����������� ������
{
    name_ = name;                                                                   //���������� ��� �����
    loadStyle(dir_path);                                                            //�������� ����� �� ������
}
Style::Style(const Style & style)                                                   //����������� �����������
{
    name_ = style.name_;                                                            //�������� ���
    items_ = style.items_;                                                          //�������� ��������
}

//
//  ������ ������
//

const QString& Style::get(const QString& key)                                       //����� ������� � ������� �����
{
    auto it = items_.find(key);                                                     //���� � ��� �������

    if (it == items_.end())                                                         //���� �� �����
        return QString();                                                           //�� ���������� ������ �����

    return *it;                                                                     //����� ������ ������ �����
}
void Style::loadStyle(QString dir)                                                  //�����, ����������� CSS ����� �� ������
{
    filesystem::Directory directory(dir);                                           //�������� ������ � ����������

    if (directory.isExists())                                                       //���� ���������� ����������
    {
        auto files = directory.getFilesPaths();                                     //�������� ��� ���� ������ �� ����������

        for(auto& path : files)                                                     //���������� ��
        {
            filesystem::filetypes::StyleFile file(path);                            //��������� �������� ����
            items_.insert(file.key(), file.content());                              //��������� � ���������
        }
    }
}