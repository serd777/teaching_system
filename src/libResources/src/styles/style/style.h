#pragma once

//
//  ����� ��������� ����������
//

#include <QtCore/QHash>

namespace resources
{
    class Style                                                                     //����� ��������� ����������
    {
        typedef QHash<QString, QString> HashType;                                   //��� ������ - ��� �������

    public:                                                                         //�������� ������

        explicit Style(                                                             //����������� ������
            QString name,                                                           //��� �����
            QString dir_path                                                        //���� �� ���������� � CSS ������� �����
        );
        Style(const Style & style);                                                 //����������� �����������
        Style()  {};                                                                //������ �����������

        ~Style() {};                                                                //���������� ������

        const QString& get(const QString& key);                                     //����� ������� � ������� �����
        const auto&    name() { return name_; }                                     //����� ������� � ����� �����

    private:                                                                        //�������� ������

        QString name_;                                                              //��� �����
        HashType items_;                                                            //CSS �����

        void loadStyle(QString dir);                                                //�����, ����������� CSS ����� �� ������
    };
}