#include "styles.h"
#include <directory/directory.h>

using namespace resources;

//
//  ���� ������
//

StyleAccessor*    Styles::accessor_ = new StyleAccessor();                          //������������� ������ �������
Styles::StyleVec  Styles::styles_   = StyleVec();                                   //������ ������

//
//  ������ ������
//

bool Styles::init()                                                                 //����� �������������
{
    filesystem::Directory dir("resources/styles");                                  //�������� ������ � ���������� �� �������

    if (!dir.isExists())                                                            //���� � �� ����������
        return false;                                                               //�� ������������� �����������

    auto names = dir.getSubDirsNames();                                             //�������� ����� ���-����������

    for(auto& name : names)
    {
        Style style(name, "resources/styles/" + name);                              //��������� �����

        styles_.push_back(style);                                                   //������ ��� � ���������

        if (name == "white")                                                        //���� ����� ��������� �����
            accessor_->setStyle(&styles_.back());                                   //�� �������� ���
    }

    return true;                                                                    //������������� ������ �������
}
