#pragma once

//
//  ����� ������� � �������� �����
//

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include <export.h>
#include <styles/style/style.h>

namespace resources
{
    class RESOURCES_EXPORT StyleAccessor : public QObject                           //����� ������� � �������� �����
    {
        Q_OBJECT                                                                    //MOC ����

        typedef QSharedPointer<Style> StylePtr;                                     //��� ������ - ��������� �� �����

    public:                                                                         //�������� ������

        StyleAccessor() : QObject(), style_(nullptr) {};                            //����������� ������
        StyleAccessor(const StyleAccessor& old) { style_ = old.style_; }            //����������� �����������

        ~StyleAccessor() {}                                                         //���������� ������

        const QString& operator[](const QString& key) const;                        //�������� ������� � �������� �������� �����

        void setStyle(Style* style);                                                //�����, �������� ����� 
        const auto& name() { return style_->name(); }                               //����� ������� � ����� �����

    signals: void styleUpdated();                                                   //������ ���������� �����
    private: StylePtr style_;                                                       //������� ����� ����������
    };
}