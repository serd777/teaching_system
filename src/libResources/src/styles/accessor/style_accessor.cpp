#include "style_accessor.h"

using namespace resources;

//
//  ������������� ���������
//

const QString& StyleAccessor::operator[](const QString& key) const                  //�������� ������� � �������� �������� �����
{
    return style_->get(key);                                                        //���������� ����� �� �����
}

//
//  ������ ������
//

void StyleAccessor::setStyle(Style* style)                                          //�����, �������� ����� 
{
    style_ = StylePtr(style);                                                       //���������� �����
    emit styleUpdated();                                                            //�������� ������
}
