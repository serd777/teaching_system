#pragma once

//
//  ��������� ����� ������� � �������� �����������
//

#include <QtCore/QVector>
#include <export.h>

#include "./accessor/style_accessor.h"
#include "./style/style.h"

namespace resources
{
    class RESOURCES_EXPORT Styles                                                   //��������� ����� ������� � �������� �����������
    {
        typedef QVector<Style> StyleVec;                                            //��� ������ - �������� ������

    public:                                                                         //�������� ������

        static const auto&     accessor() { return accessor_; }                     //����� ������� � ���� ������-�������
        static bool            init();                                              //����� �������������

    private:                                                                        //�������� ������

        static StyleAccessor* accessor_;                                            //�����-������ � �������� �����
        static StyleVec       styles_;                                              //������ � ���������� �������
    };
}