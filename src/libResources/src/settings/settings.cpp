#include "settings.h"

#include <directory/directory.h>

using namespace resources;

void Settings::init()                                                               //�����, ���������������� ���������
{
    filesystem::Directory dir("resources/settings");                                //�������� ������ � ����������

    if(dir.isExists())                                                              //���� ���������� ����������
    {
        auto paths = dir.getFilesPaths();                                           //�������� ���� ���� ������ � �����

        for(auto& path : paths)                                                     //���������� ����
        {
            filesystem::filetypes::SettingsFile file(path);                         //���� � �����������
            file.loadData();                                                        //��������� ������ �� �����

            files_.push_back(file);                                                 //������ ���� � ���������
        }
    }
}

QString Settings::get(const QString& name, const QString& key)                      //�����, ������������ �������� �� ����� (��� = ��� �����)
{
    for (auto& file : files_)                                                       //���������� ��� �����
        if (file.name() == name)                                                    //����� ����������
            return file.get(key);                                                   //���������� ��������

    return QString();                                                               //�������� �� �������
}
