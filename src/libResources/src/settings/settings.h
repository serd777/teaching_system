#pragma once

//
//  ����� ������ �������� ����������
//

#include <export.h>
#include <file_types/settings_file/settings_file.h>

#include <QtCore/QVector>

namespace resources
{
    class RESOURCES_EXPORT Settings                                                 //����� ������ �������� ����������
    {
        typedef QVector<filesystem::filetypes::SettingsFile> VecFiles;              //��� ������ - ������ ������ � �����������

    public:                                                                         //�������� ������

        static Settings& Instance()                                                 //����� �������
        {
            static Settings settings;                                               //������ ������
            return settings;                                                        //���������� ���
        }

        void init();                                                                //�����, ���������������� ���������

        QString get(const QString& name, const QString& key);                       //�����, ������������ �������� �� ����� (��� = ��� �����)

    private:                                                                        //�������� ������

        Settings()  {}                                                              //����������� ������
        ~Settings() {}                                                              //���������� ������

        Settings(Settings const&) = delete;                                         //��������� ����������� �����������
        Settings& operator= (Settings const&) = delete;                             //� �������� ������������

        VecFiles files_;                                                            //������ ������
    };
}