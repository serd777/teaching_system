#pragma once

//
//  ��������� ������� ��� ��������� ����
//

#include <QtCore/QString>

namespace resources
{
    namespace code_helper
    {
        class HelperWord                                                            //��������� ������� ��� ��������� ����
        {
        public:                                                                     //�������� ������

            HelperWord()  {}                                                        //����������� ������
            ~HelperWord() {}                                                        //���������� ������

            HelperWord(const HelperWord& copy) :                                    //����������� �����������
                name_(copy.name_),                                                  //�������� ���
                short_help_(copy.short_help_),                                      //�������� �������� ���������
                long_help_(copy.long_help_)                                         //�������� ������� ���������
            {}

            auto& name()      { return name_;       }                               //������ � ���� � ������
            auto& shortHelp() { return short_help_; }                               //������ � ���� � �������� ����������
            auto& longHelp()  { return long_help_;  }                               //������ � ���� � ������� ����������

        private:                                                                    //�������� ������

            QString name_;                                                          //��� �� �������������� �����
            QString short_help_;                                                    //����� �������� ���������
            QString long_help_;                                                     //����� ������� ���������
        };
    }
}