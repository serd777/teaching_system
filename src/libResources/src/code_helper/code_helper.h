#pragma once

//
//  ������ ������� � ���������� ���������� ����
//

#include <export.h>
#include <QtCore/QObject>
#include <QtCore/QHash>

#include "helper_word.h"

namespace resources
{
    class RESOURCES_EXPORT CodeHelper : public QObject                              //������ ������� � ���������� ���������� ����
    {
        Q_OBJECT                                                                    //MOC ����

        typedef QHash<QString, code_helper::HelperWord> HashTable;                  //��� ������ - ��� ������� �� ������ �������

    public:                                                                         //�������� ������

        static CodeHelper& Instance()                                               //����� ������� � ������
        {
            static CodeHelper code_helper;                                          //������� ��������� ������
            return code_helper;                                                     //���������� ���
        }

        bool init();                                                                //����� �������������

        QString getShort(const QString& key);                                       //�����, ������������ �������� ���������
        QString getLong(const QString& key);                                        //�����, ������������ ������� ���������

    private:                                                                        //�������� ������

        CodeHelper()  {}                                                            //����������� ������
        ~CodeHelper() {}                                                            //���������� ������

        CodeHelper(CodeHelper const&) = delete;                                     //��������� ����������� �����������
        CodeHelper& operator= (CodeHelper const&) = delete;                         //��������� �������� ������������

        HashTable dictionary_;                                                      //������� ���������

        bool loadDictionary(const QString& name);                                   //�����, ����������� ������� ���������
    };
}