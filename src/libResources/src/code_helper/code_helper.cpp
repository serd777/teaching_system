#include "code_helper.h"

#include <localizations/loc.h>
#include <directory/directory.h>
#include <files/text_file/text_file.h>

using namespace resources;

//
//  �������� �������
//

bool CodeHelper::init()                                                             //����� �������������
{
    auto accessor = Loc::accessor();                                                //������ � �����������

    if (!accessor)                                                                  //���� ����������� �� ��������������
        return false;                                                               //������������� ������ �����������
    if (!loadDictionary(accessor->name()))                                          //��������� ����������� �������
        return false;                                                               //�������� �����������

    connect(accessor, &LocaleAccessor::localeUpdated,                               //���������� � ������� ���������� �����������
            this,     [this, accessor]()                                            //������-����������
    {
        loadDictionary(accessor->name());                                           //��������� ����� �������
    });

    return true;                                                                    //���������������� �������
}

bool CodeHelper::loadDictionary(const QString& name)                                //�����, ����������� ������� ���������
{
    filesystem::Directory dir("resources/code_helper/" + name);                     //�������� ������ � ����������

    if (!dir.isExists())                                                            //���� ���������� �� ����������
        return false;                                                               //�������� �����������
    if (!dictionary_.isEmpty())                                                     //���� ������� �� ������
        dictionary_.clear();                                                        //�� ������� ���

    auto sub_dirs = dir.getSubDirsPaths();                                          //�������� ������ ����� ���-����������

    for(auto& path : sub_dirs)                                                      //���������� ����
    {
        filesystem::files::TextFile key_file(path   + "/key.name");                 //���� � ������������� ������
        filesystem::files::TextFile short_help(path + "/short_help.html");          //���� � ������� ���������� �����
        filesystem::files::TextFile long_help(path  + "/long_help.html");           //���� � ������� ���������� �����

        code_helper::HelperWord word;                                               //������� �������

        word.name()      = key_file.getAllContent();                                //������ ���
        word.shortHelp() = short_help.getAllContent();                              //������ �������� ���������
        word.longHelp()  = long_help.getAllContent();                               //������ ������� ���������

        dictionary_.insert(key_file.getAllContent(), word);                         //������ � �������
    }

    return true;                                                                    //�������� �������
}

QString CodeHelper::getShort(const QString& key)                                    //�����, ������������ �������� ���������
{
    auto it = dictionary_.find(key);                                                //���� � ��� �������

    if (it != dictionary_.end())                                                    //���� ����� �������
        return (*it).shortHelp();                                                   //���������� ���������

    return QString();                                                               //����� �� ������
}
QString CodeHelper::getLong(const QString& key)                                     //�����, ������������ ������� ���������
{
    auto it = dictionary_.find(key);                                                //���� � ��� �������

    if (it != dictionary_.end())                                                    //���� ����� �������
        return (*it).longHelp();                                                    //���������� ���������

    return QString();                                                               //����� �� ������
}