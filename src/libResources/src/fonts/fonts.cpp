#include "fonts.h"

#include <directory/directory.h>

using namespace resources;

//
//  ���� ������
//

Fonts::FontDBPtr Fonts::database_ = FontDBPtr(nullptr);                             //���� �� ��������

//
//  ������ ������
//

bool Fonts::init()                                                                  //����� �������������
{
    filesystem::Directory dir("resources/fonts");                                   //�������� ������ � ����������

    if (!dir.isExists())                                                            //���� ���������� �� ����������
        return false;                                                               //������������� �����������

    database_.reset(new QFontDatabase);                                             //�������������� �� �� ��������

    auto files = dir.getFilesPaths();                                               //�������� ����� � �����

    for(auto& path : files)                                                         //���������� ����
        database_->addApplicationFont(path);                                        //��������� ����� � ���������

    return true;                                                                    //�������
}

QFont Fonts::get(QString name, QString style, int size)                             //�����, ������������ ����� �� ��
{
    return database_->font(name, style, size);                                      //���������� ����� �� ��
}
