#pragma once

//
//  ����� ������� � ������� �� ����
//

#include <QtCore/QString>
#include <QtCore/QSharedPointer>

#include <QtGui/QFontDatabase>

#include <export.h>

namespace resources
{
    class RESOURCES_EXPORT Fonts
    {
        typedef QSharedPointer<QFontDatabase> FontDBPtr;                            //��� ������ - ��������� �� �� �� ��������

    public:                                                                         //�������� ������

        static bool init();                                                         //����� ������������� �� �������

        static QFont get(QString name, QString style, int size);                    //�����, ������������ ����� �� ��

    private:                                                                        //�������� ������

        static FontDBPtr database_;                                                 //���� �� ��������
    };
}