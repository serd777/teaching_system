#pragma once

//
//  ����������� ����� ������� � ������������
//

#include <QtCore/QVector>
#include <QtCore/QString>

#include <localizations/locale/locale.h>
#include <localizations/accessor/locale_accessor.h>

namespace resources
{
    class RESOURCES_EXPORT Loc                                                      //����������� ����� ������� � ������������
    {
        typedef QVector<Locale> VecLocale;                                          //��� ������ - ������ �������

    public:                                                                         //�������� ������

        static bool init();                                                         //����� �������������
        static const auto& accessor() { return accessor_; }                         //����� ������� � ��������� �����������

    private:                                                                        //�������� ������

        static LocaleAccessor* accessor_;                                           //������ � ������� �����������
        static VecLocale locales_;                                                  //������ �����������
    };
}