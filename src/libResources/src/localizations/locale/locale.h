#pragma once

//
//  ����� ����������� �����
//

#include <QtCore/QSharedPointer>
#include <file_types/loc_file/loc_file.h>

namespace resources
{
    class Locale                                                                    //����� ����������� �����
    {
        typedef QSharedPointer<filesystem::filetypes::LocFile> LocFilePtr;          //��� ������ - ��������� �� ��������������� ����

    public:                                                                         //�������� ������

        explicit Locale(                                                            //����������� ������
            QString name                                                            //��� �����������
        );

        Locale(const Locale& locale);                                               //����������� �����������
        Locale()  {}                                                                //������ �����������

        ~Locale() {}                                                                //���������� ������

        const auto& name() const { return name_; }                                  //����� ������� � ����� �����������
        const auto& get(const QString& key) { return file_->getItem(key); }         //����� ������� � ���������������� ��������

    private:                                                                        //�������� ������

        LocFilePtr file_;                                                           //��������������� ����
        QString    name_;                                                           //�������� �����������
    };
}