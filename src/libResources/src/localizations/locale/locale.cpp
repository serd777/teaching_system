#include "locale.h"

using namespace resources;

//
//  ������������ ������
//

Locale::Locale(QString name)                                                        //����������� ������
{
    const auto path = "resources/locales/" + name + ".loc";                         //���� �� �����

    name_ = name;                                                                   //���������� ���
    file_.reset(new filesystem::filetypes::LocFile(path));                          //�������� ������ � �����
}
Locale::Locale(const Locale& locale)                                                //����������� �����������
{
    file_ = locale.file_;                                                           //�������� ������ � �����
    name_ = locale.name_;                                                           //�������� ��� �����������
}
