#pragma once

//
//  ����� ������� � ������� �����������
//

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include <export.h>
#include <localizations/locale/locale.h>

namespace resources
{
    class RESOURCES_EXPORT LocaleAccessor : public QObject                          //����� ������� � ������� �����������
    {
        Q_OBJECT                                                                    //MOC ����

        typedef QSharedPointer<Locale> LocalePtr;                                   //��� ������ - ��������� �� �����������

    public:                                                                         //�������� ������

        LocaleAccessor() : QObject(), locale_(nullptr) {};                          //����������� ������
        LocaleAccessor(const LocaleAccessor& old) { locale_ = old.locale_; }        //����������� �����������

        ~LocaleAccessor() {}                                                        //���������� ������

        const QString& operator[](const QString& key) const;                        //�������� ������� � �������� �������� �����

        void setLocale(Locale* locale);                                             //�����, �������� ����������� 
        const auto& name() { return locale_->name(); }                              //�����, ������������ �������� �����������

    signals: void localeUpdated();                                                  //������ ���������� �����������
    private: LocalePtr locale_;                                                     //������� �����������
    };
}