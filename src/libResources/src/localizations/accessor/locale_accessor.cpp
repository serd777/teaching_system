#include "locale_accessor.h"

using namespace resources;

//
//  ������������� ���������
//

const QString& LocaleAccessor::operator[](const QString& key) const                 //�������� ������� � �������� ������� �����������
{
    return locale_->get(key);                                                       //���������� ����������� �� �����
}

//
//  ������ ������
//

void LocaleAccessor::setLocale(Locale* style)                                       //�����, �������� ����������� 
{
    locale_ = LocalePtr(style);                                                     //���������� �����������
    emit localeUpdated();                                                           //�������� ������
}
