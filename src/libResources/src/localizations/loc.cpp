#include "loc.h"
#include <directory/directory.h>

using namespace resources;

//
//  ���� ������
//

LocaleAccessor* Loc::accessor_  = new LocaleAccessor();                             //������������� ������ �������
Loc::VecLocale  Loc::locales_   = VecLocale();                                      //������ �����������

//
//  ������ ������
//

bool Loc::init()                                                                    //����� �������������
{
    filesystem::Directory locales_dir("resources/locales");                         //�������� ������ � ����������

    if (!locales_dir.isExists())                                                    //���� ���������� �� ����������
        return false;                                                               //������������� �����������

    auto file_names = locales_dir.getFilesNames();                                  //�������� ����� ������

    for(auto& name : file_names)                                                    //���������� ��� �����
    {
        Locale locale(name);                                                        //������� �����������
        locales_.push_back(locale);                                                 //������ � � ���������

        if (name == "ru")                                                           //���� ��� ����� ��������� �� ��������� ������������
            accessor_->setLocale(&locales_.back());                                 //���������� ���������
    }

    return true;                                                                    //���������������� �������
}
